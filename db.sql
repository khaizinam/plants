--
-- PostgreSQL database dump
--

-- Dumped from database version 15.1 (Ubuntu 15.1-1.pgdg20.04+1)
-- Dumped by pg_dump version 15.3

-- Started on 2024-05-13 21:47:32

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE postgres;
--
-- TOC entry 3885 (class 1262 OID 28561)
-- Name: postgres; Type: DATABASE; Schema: -; Owner: -
--

CREATE DATABASE postgres WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE_PROVIDER = libc LOCALE = 'C.UTF-8';


\connect postgres

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 18 (class 2615 OID 2200)
-- Name: public; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA public;


--
-- TOC entry 544 (class 1255 OID 29648)
-- Name: update_modified_column(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.update_modified_column() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
NEW.modified_at = now();
RETURN NEW;
END;
$$;


--
-- TOC entry 543 (class 1255 OID 29632)
-- Name: update_timestamp(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.update_timestamp() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    NEW.modified_at = CURRENT_TIMESTAMP;
    RETURN NEW;
END;
$$;


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 278 (class 1259 OID 29606)
-- Name: areas; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.areas (
    id integer NOT NULL,
    no integer,
    name character varying,
    user_id integer,
    pump_code text,
    soil_code text,
    created_at character varying
);


--
-- TOC entry 277 (class 1259 OID 29605)
-- Name: areas_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.areas_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3886 (class 0 OID 0)
-- Dependencies: 277
-- Name: areas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.areas_id_seq OWNED BY public.areas.id;


--
-- TOC entry 284 (class 1259 OID 29653)
-- Name: plant_feed_back; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.plant_feed_back (
    id integer NOT NULL,
    plant_id integer NOT NULL,
    author_id integer NOT NULL,
    content text,
    is_seen boolean,
    time_seen timestamp without time zone,
    created_at timestamp without time zone
);


--
-- TOC entry 283 (class 1259 OID 29652)
-- Name: plant_feed_back_author_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.plant_feed_back_author_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3887 (class 0 OID 0)
-- Dependencies: 283
-- Name: plant_feed_back_author_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.plant_feed_back_author_id_seq OWNED BY public.plant_feed_back.author_id;


--
-- TOC entry 281 (class 1259 OID 29650)
-- Name: plant_feed_back_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.plant_feed_back_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3888 (class 0 OID 0)
-- Dependencies: 281
-- Name: plant_feed_back_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.plant_feed_back_id_seq OWNED BY public.plant_feed_back.id;


--
-- TOC entry 282 (class 1259 OID 29651)
-- Name: plant_feed_back_plant_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.plant_feed_back_plant_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3889 (class 0 OID 0)
-- Dependencies: 282
-- Name: plant_feed_back_plant_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.plant_feed_back_plant_id_seq OWNED BY public.plant_feed_back.plant_id;


--
-- TOC entry 276 (class 1259 OID 29539)
-- Name: plant_posts; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.plant_posts (
    id integer NOT NULL,
    no integer,
    plant_id integer,
    title text,
    content text,
    modified_at character varying,
    created_at character varying
);


--
-- TOC entry 275 (class 1259 OID 29538)
-- Name: plant_posts_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.plant_posts_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3890 (class 0 OID 0)
-- Dependencies: 275
-- Name: plant_posts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.plant_posts_id_seq OWNED BY public.plant_posts.id;


--
-- TOC entry 274 (class 1259 OID 29495)
-- Name: plant_stages; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.plant_stages (
    id integer NOT NULL,
    no integer,
    name character varying,
    description text,
    humidity_above double precision,
    humidity_below double precision,
    range integer,
    plant_id integer NOT NULL,
    grow_types character varying,
    img_url character varying
);


--
-- TOC entry 272 (class 1259 OID 29493)
-- Name: plant_stages_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.plant_stages_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3891 (class 0 OID 0)
-- Dependencies: 272
-- Name: plant_stages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.plant_stages_id_seq OWNED BY public.plant_stages.id;


--
-- TOC entry 273 (class 1259 OID 29494)
-- Name: plant_stages_plant_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.plant_stages_plant_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3892 (class 0 OID 0)
-- Dependencies: 273
-- Name: plant_stages_plant_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.plant_stages_plant_id_seq OWNED BY public.plant_stages.plant_id;


--
-- TOC entry 271 (class 1259 OID 29458)
-- Name: plants; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.plants (
    id integer NOT NULL,
    name character varying,
    avatar character varying,
    category character varying,
    description text
);


--
-- TOC entry 270 (class 1259 OID 29457)
-- Name: plants_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.plants_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3893 (class 0 OID 0)
-- Dependencies: 270
-- Name: plants_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.plants_id_seq OWNED BY public.plants.id;


--
-- TOC entry 292 (class 1259 OID 29748)
-- Name: reminder_tree; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.reminder_tree (
    reminder_id integer NOT NULL,
    tree_id integer NOT NULL
);


--
-- TOC entry 290 (class 1259 OID 29746)
-- Name: reminder_tree_reminder_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.reminder_tree_reminder_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3894 (class 0 OID 0)
-- Dependencies: 290
-- Name: reminder_tree_reminder_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.reminder_tree_reminder_id_seq OWNED BY public.reminder_tree.reminder_id;


--
-- TOC entry 291 (class 1259 OID 29747)
-- Name: reminder_tree_tree_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.reminder_tree_tree_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3895 (class 0 OID 0)
-- Dependencies: 291
-- Name: reminder_tree_tree_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.reminder_tree_tree_id_seq OWNED BY public.reminder_tree.tree_id;


--
-- TOC entry 289 (class 1259 OID 29732)
-- Name: reminders; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.reminders (
    id integer NOT NULL,
    user_id integer,
    loop character varying,
    date_start date,
    date_end date,
    "time" time without time zone,
    note text
);


--
-- TOC entry 288 (class 1259 OID 29730)
-- Name: reminders_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.reminders_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3896 (class 0 OID 0)
-- Dependencies: 288
-- Name: reminders_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.reminders_id_seq OWNED BY public.reminders.id;


--
-- TOC entry 296 (class 1259 OID 29782)
-- Name: tree_diaries; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.tree_diaries (
    id integer NOT NULL,
    tree_id integer NOT NULL,
    stage_no integer,
    plant_id integer NOT NULL,
    note character varying,
    weather character varying,
    image_url character varying,
    date_published date DEFAULT CURRENT_DATE NOT NULL,
    status integer DEFAULT 0 NOT NULL
);


--
-- TOC entry 293 (class 1259 OID 29779)
-- Name: tree_diaries_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.tree_diaries_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3897 (class 0 OID 0)
-- Dependencies: 293
-- Name: tree_diaries_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.tree_diaries_id_seq OWNED BY public.tree_diaries.id;


--
-- TOC entry 295 (class 1259 OID 29781)
-- Name: tree_diaries_plant_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.tree_diaries_plant_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3898 (class 0 OID 0)
-- Dependencies: 295
-- Name: tree_diaries_plant_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.tree_diaries_plant_id_seq OWNED BY public.tree_diaries.plant_id;


--
-- TOC entry 294 (class 1259 OID 29780)
-- Name: tree_diaries_tree_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.tree_diaries_tree_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3899 (class 0 OID 0)
-- Dependencies: 294
-- Name: tree_diaries_tree_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.tree_diaries_tree_id_seq OWNED BY public.tree_diaries.tree_id;


--
-- TOC entry 280 (class 1259 OID 29639)
-- Name: trees; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.trees (
    id integer NOT NULL,
    name character varying,
    user_id integer,
    plant_id integer,
    area_id integer,
    description text,
    modified_at timestamp without time zone DEFAULT now(),
    created_at date DEFAULT now()
);


--
-- TOC entry 279 (class 1259 OID 29638)
-- Name: trees_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.trees_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3900 (class 0 OID 0)
-- Dependencies: 279
-- Name: trees_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.trees_id_seq OWNED BY public.trees.id;


--
-- TOC entry 269 (class 1259 OID 29428)
-- Name: users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.users (
    id integer NOT NULL,
    email character varying,
    password character varying,
    full_name character varying,
    phone_number character varying,
    mail_contact character varying,
    location character varying,
    role character varying DEFAULT 'USER'::character varying,
    create_at date
);


--
-- TOC entry 268 (class 1259 OID 29427)
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3901 (class 0 OID 0)
-- Dependencies: 268
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- TOC entry 287 (class 1259 OID 29716)
-- Name: water_schedules; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.water_schedules (
    id integer NOT NULL,
    mode character varying,
    time_start timestamp without time zone,
    time_end timestamp without time zone,
    soil_up integer,
    soil_down integer,
    pump_power boolean,
    area_id integer NOT NULL
);


--
-- TOC entry 286 (class 1259 OID 29715)
-- Name: water_schedules_area_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.water_schedules_area_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3902 (class 0 OID 0)
-- Dependencies: 286
-- Name: water_schedules_area_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.water_schedules_area_id_seq OWNED BY public.water_schedules.area_id;


--
-- TOC entry 285 (class 1259 OID 29714)
-- Name: water_schedules_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.water_schedules_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3903 (class 0 OID 0)
-- Dependencies: 285
-- Name: water_schedules_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.water_schedules_id_seq OWNED BY public.water_schedules.id;


--
-- TOC entry 3654 (class 2604 OID 29609)
-- Name: areas id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.areas ALTER COLUMN id SET DEFAULT nextval('public.areas_id_seq'::regclass);


--
-- TOC entry 3658 (class 2604 OID 29656)
-- Name: plant_feed_back id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.plant_feed_back ALTER COLUMN id SET DEFAULT nextval('public.plant_feed_back_id_seq'::regclass);


--
-- TOC entry 3659 (class 2604 OID 29657)
-- Name: plant_feed_back plant_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.plant_feed_back ALTER COLUMN plant_id SET DEFAULT nextval('public.plant_feed_back_plant_id_seq'::regclass);


--
-- TOC entry 3660 (class 2604 OID 29658)
-- Name: plant_feed_back author_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.plant_feed_back ALTER COLUMN author_id SET DEFAULT nextval('public.plant_feed_back_author_id_seq'::regclass);


--
-- TOC entry 3653 (class 2604 OID 29542)
-- Name: plant_posts id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.plant_posts ALTER COLUMN id SET DEFAULT nextval('public.plant_posts_id_seq'::regclass);


--
-- TOC entry 3652 (class 2604 OID 29498)
-- Name: plant_stages id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.plant_stages ALTER COLUMN id SET DEFAULT nextval('public.plant_stages_id_seq'::regclass);


--
-- TOC entry 3651 (class 2604 OID 29461)
-- Name: plants id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.plants ALTER COLUMN id SET DEFAULT nextval('public.plants_id_seq'::regclass);


--
-- TOC entry 3664 (class 2604 OID 29865)
-- Name: reminder_tree reminder_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.reminder_tree ALTER COLUMN reminder_id SET DEFAULT nextval('public.reminder_tree_reminder_id_seq'::regclass);


--
-- TOC entry 3665 (class 2604 OID 29871)
-- Name: reminder_tree tree_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.reminder_tree ALTER COLUMN tree_id SET DEFAULT nextval('public.reminder_tree_tree_id_seq'::regclass);


--
-- TOC entry 3663 (class 2604 OID 29735)
-- Name: reminders id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.reminders ALTER COLUMN id SET DEFAULT nextval('public.reminders_id_seq'::regclass);


--
-- TOC entry 3666 (class 2604 OID 29785)
-- Name: tree_diaries id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tree_diaries ALTER COLUMN id SET DEFAULT nextval('public.tree_diaries_id_seq'::regclass);


--
-- TOC entry 3667 (class 2604 OID 29786)
-- Name: tree_diaries tree_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tree_diaries ALTER COLUMN tree_id SET DEFAULT nextval('public.tree_diaries_tree_id_seq'::regclass);


--
-- TOC entry 3668 (class 2604 OID 29787)
-- Name: tree_diaries plant_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tree_diaries ALTER COLUMN plant_id SET DEFAULT nextval('public.tree_diaries_plant_id_seq'::regclass);


--
-- TOC entry 3655 (class 2604 OID 29642)
-- Name: trees id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.trees ALTER COLUMN id SET DEFAULT nextval('public.trees_id_seq'::regclass);


--
-- TOC entry 3649 (class 2604 OID 29431)
-- Name: users id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- TOC entry 3661 (class 2604 OID 29719)
-- Name: water_schedules id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.water_schedules ALTER COLUMN id SET DEFAULT nextval('public.water_schedules_id_seq'::regclass);


--
-- TOC entry 3662 (class 2604 OID 29720)
-- Name: water_schedules area_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.water_schedules ALTER COLUMN area_id SET DEFAULT nextval('public.water_schedules_area_id_seq'::regclass);


--
-- TOC entry 3861 (class 0 OID 29606)
-- Dependencies: 278
-- Data for Name: areas; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.areas VALUES (72, 1, 'ABC', 10, NULL, NULL, NULL);
INSERT INTO public.areas VALUES (73, 2, 'ZZZZ', 10, NULL, NULL, NULL);
INSERT INTO public.areas VALUES (75, 3, 'KHU VỰC 3', 3, NULL, NULL, NULL);
INSERT INTO public.areas VALUES (76, 4, 'KHU VỰC 0', 3, NULL, NULL, NULL);
INSERT INTO public.areas VALUES (74, 2, 'KHU VUC 2', 3, NULL, NULL, NULL);


--
-- TOC entry 3867 (class 0 OID 29653)
-- Dependencies: 284
-- Data for Name: plant_feed_back; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.plant_feed_back VALUES (9, 3, 3, 'Theo các bạn có những cách tích luỹ kinh nghiệm và profile để nhà tuyển dụng khó có thể từ chối cho xin việc hoặc xin thực tập?
Mình trước nhé: tự làm project và tham gia hackathon 😉', false, NULL, '2024-05-13 15:16:59');
INSERT INTO public.plant_feed_back VALUES (8, 1, 3, 'áddadsadadsada', true, '2024-03-10 11:01:37', '2024-03-10 11:00:01');
INSERT INTO public.plant_feed_back VALUES (5, 1, 3, '.', true, '2024-03-10 11:02:03', '2024-03-10 10:59:52');
INSERT INTO public.plant_feed_back VALUES (6, 1, 3, 'ádaddasdadsdsad', true, '2024-03-10 11:13:31', '2024-03-10 10:59:55');
INSERT INTO public.plant_feed_back VALUES (7, 1, 3, 'ádadsdadsadadadasdsad', true, '2024-03-10 14:12:43', '2024-03-10 10:59:58');
INSERT INTO public.plant_feed_back VALUES (10, 3, 3, 'Theo các bạn có những cách tích luỹ kinh nghiệm và profile để nhà tuyển dụng khó có thể từ chối cho xin việc hoặc xin thực tập?
Mình trước nhé: tự làm project và tham gia hackathon 😉', false, NULL, '2024-03-10 15:17:11');
INSERT INTO public.plant_feed_back VALUES (11, 8, 3, 'Theo các bạn có những cách tích luỹ kinh nghiệm và profile để nhà tuyển dụng khó có thể từ chối cho xin việc hoặc xin thực tập?
Mình trước nhé: tự làm project và tham gia hackathon 😉', false, NULL, '2024-03-10 15:20:25');
INSERT INTO public.plant_feed_back VALUES (12, 4, 3, 'Có bạn nào kỳ này đang học môn giải tích hệ thống điện của thầy Nguyễn Phúc Khải ko ạ. Các bạn có thể chấm bài mình có ít việc cần giúp đỡ ạ. Cảm ơn mn ạ.', false, NULL, '2024-03-10 15:23:03');
INSERT INTO public.plant_feed_back VALUES (13, 8, 3, 'Táo tây, còn gọi là bôm (phiên âm từ tiếng Pháp: pomme), là một loại quả ăn được từ cây táo tây (Malus domestica). Táo tây được trồng trên khắp thế giới và là loài cây được trồng phổ biến nhất trong chi Malus. Cây táo tây có nguồn gốc từ Trung Á, nơi tổ tiên của nó là táo dại Tân Cương sinh sống, hiện vẫn còn tồn tại cho đến ngày nay. Chúng đã được trồng từ hàng ngàn năm qua ở châu Á và châu u và đã được thực dân châu u đưa đến Bắc Mỹ. Táo tây có ý nghĩa tôn giáo và thần thoại trong nhiều nền văn hóa, bao gồm Bắc u, Hy Lạp và Cơ đốc giáo châu u.', false, NULL, '2024-03-10 15:26:02');
INSERT INTO public.plant_feed_back VALUES (14, 8, 3, 'Táo tây, còn gọi là bôm (phiên âm từ tiếng Pháp: pomme), là một loại quả ăn được từ cây táo tây (Malus domestica). Táo tây được trồng trên khắp thế giới và là loài cây được trồng phổ biến nhất trong chi Malus. Cây táo tây có nguồn gốc từ Trung Á, nơi tổ tiên của nó là táo dại Tân Cương sinh sống, hiện vẫn còn tồn tại cho đến ngày nay. Chúng đã được trồng từ hàng ngàn năm qua ở châu Á và châu u và đã được thực dân châu u đưa đến Bắc Mỹ. Táo tây có ý nghĩa tôn giáo và thần thoại trong nhiều nền văn hóa, bao gồm Bắc u, Hy Lạp và Cơ đốc giáo châu u.', false, NULL, '2024-03-10 16:47:32');
INSERT INTO public.plant_feed_back VALUES (15, 8, 3, 'Táo tây, còn gọi là bôm (phiên âm từ tiếng Pháp: pomme), là một loại quả ăn được từ cây táo tây (Malus domestica). Táo tây được trồng trên khắp thế giới và là loài cây được trồng phổ biến nhất trong chi Malus. Cây táo tây có nguồn gốc từ Trung Á, nơi tổ tiên của nó là táo dại Tân Cương sinh sống, hiện vẫn còn tồn tại cho đến ngày nay. Chúng đã được trồng từ hàng ngàn năm qua ở châu Á và châu u và đã được thực dân châu u đưa đến Bắc Mỹ. Táo tây có ý nghĩa tôn giáo và thần thoại trong nhiều nền văn hóa, bao gồm Bắc u, Hy Lạp và Cơ đốc giáo châu u.', false, NULL, '2024-03-10 16:47:35');
INSERT INTO public.plant_feed_back VALUES (16, 8, 3, 'Táo tây, còn gọi là bôm (phiên âm từ tiếng Pháp: pomme), là một loại quả ăn được từ cây táo tây (Malus domestica). Táo tây được trồng trên khắp thế giới và là loài cây được trồng phổ biến nhất trong chi Malus. Cây táo tây có nguồn gốc từ Trung Á, nơi tổ tiên của nó là táo dại Tân Cương sinh sống, hiện vẫn còn tồn tại cho đến ngày nay. Chúng đã được trồng từ hàng ngàn năm qua ở châu Á và châu u và đã được thực dân châu u đưa đến Bắc Mỹ. Táo tây có ý nghĩa tôn giáo và thần thoại trong nhiều nền văn hóa, bao gồm Bắc u, Hy Lạp và Cơ đốc giáo châu u.', false, NULL, '2024-03-10 16:47:38');
INSERT INTO public.plant_feed_back VALUES (17, 8, 3, 'Táo tây, còn gọi là bôm (phiên âm từ tiếng Pháp: pomme), là một loại quả ăn được từ cây táo tây (Malus domestica). Táo tây được trồng trên khắp thế giới và là loài cây được trồng phổ biến nhất trong chi Malus. Cây táo tây có nguồn gốc từ Trung Á, nơi tổ tiên của nó là táo dại Tân Cương sinh sống, hiện vẫn còn tồn tại cho đến ngày nay. Chúng đã được trồng từ hàng ngàn năm qua ở châu Á và châu u và đã được thực dân châu u đưa đến Bắc Mỹ. Táo tây có ý nghĩa tôn giáo và thần thoại trong nhiều nền văn hóa, bao gồm Bắc u, Hy Lạp và Cơ đốc giáo châu u.', false, NULL, '2024-03-10 16:47:40');
INSERT INTO public.plant_feed_back VALUES (18, 8, 3, 'Táo tây, còn gọi là bôm (phiên âm từ tiếng Pháp: pomme), là một loại quả ăn được từ cây táo tây (Malus domestica). Táo tây được trồng trên khắp thế giới và là loài cây được trồng phổ biến nhất trong chi Malus. Cây táo tây có nguồn gốc từ Trung Á, nơi tổ tiên của nó là táo dại Tân Cương sinh sống, hiện vẫn còn tồn tại cho đến ngày nay. Chúng đã được trồng từ hàng ngàn năm qua ở châu Á và châu u và đã được thực dân châu u đưa đến Bắc Mỹ. Táo tây có ý nghĩa tôn giáo và thần thoại trong nhiều nền văn hóa, bao gồm Bắc u, Hy Lạp và Cơ đốc giáo châu u.', false, NULL, '2024-03-10 16:47:42');
INSERT INTO public.plant_feed_back VALUES (19, 8, 3, 'Táo tây, còn gọi là bôm (phiên âm từ tiếng Pháp: pomme), là một loại quả ăn được từ cây táo tây (Malus domestica). Táo tây được trồng trên khắp thế giới và là loài cây được trồng phổ biến nhất trong chi Malus. Cây táo tây có nguồn gốc từ Trung Á, nơi tổ tiên của nó là táo dại Tân Cương sinh sống, hiện vẫn còn tồn tại cho đến ngày nay. Chúng đã được trồng từ hàng ngàn năm qua ở châu Á và châu u và đã được thực dân châu u đưa đến Bắc Mỹ. Táo tây có ý nghĩa tôn giáo và thần thoại trong nhiều nền văn hóa, bao gồm Bắc u, Hy Lạp và Cơ đốc giáo châu u.', false, NULL, '2024-03-10 16:47:45');
INSERT INTO public.plant_feed_back VALUES (20, 3, 3, 'Táo tây, còn gọi là bôm (phiên âm từ tiếng Pháp: pomme), là một loại quả ăn được từ cây táo tây (Malus domestica). Táo tây được trồng trên khắp thế giới và là loài cây được trồng phổ biến nhất trong chi Malus. Cây táo tây có nguồn gốc từ Trung Á, nơi tổ tiên của nó là táo dại Tân Cương sinh sống, hiện vẫn còn tồn tại cho đến ngày nay. Chúng đã được trồng từ hàng ngàn năm qua ở châu Á và châu u và đã được thực dân châu u đưa đến Bắc Mỹ. Táo tây có ý nghĩa tôn giáo và thần thoại trong nhiều nền văn hóa, bao gồm Bắc u, Hy Lạp và Cơ đốc giáo châu u.', false, NULL, '2024-03-10 16:47:50');
INSERT INTO public.plant_feed_back VALUES (21, 14, 3, 'Táo tây, còn gọi là bôm (phiên âm từ tiếng Pháp: pomme), là một loại quả ăn được từ cây táo tây (Malus domestica). Táo tây được trồng trên khắp thế giới và là loài cây được trồng phổ biến nhất trong chi Malus. Cây táo tây có nguồn gốc từ Trung Á, nơi tổ tiên của nó là táo dại Tân Cương sinh sống, hiện vẫn còn tồn tại cho đến ngày nay. Chúng đã được trồng từ hàng ngàn năm qua ở châu Á và châu u và đã được thực dân châu u đưa đến Bắc Mỹ. Táo tây có ý nghĩa tôn giáo và thần thoại trong nhiều nền văn hóa, bao gồm Bắc u, Hy Lạp và Cơ đốc giáo châu u.', false, NULL, '2024-03-10 16:47:55');
INSERT INTO public.plant_feed_back VALUES (22, 14, 3, 'Táo tây, còn gọi là bôm (phiên âm từ tiếng Pháp: pomme), là một loại quả ăn được từ cây táo tây (Malus domestica). Táo tây được trồng trên khắp thế giới và là loài cây được trồng phổ biến nhất trong chi Malus. Cây táo tây có nguồn gốc từ Trung Á, nơi tổ tiên của nó là táo dại Tân Cương sinh sống, hiện vẫn còn tồn tại cho đến ngày nay. Chúng đã được trồng từ hàng ngàn năm qua ở châu Á và châu u và đã được thực dân châu u đưa đến Bắc Mỹ. Táo tây có ý nghĩa tôn giáo và thần thoại trong nhiều nền văn hóa, bao gồm Bắc u, Hy Lạp và Cơ đốc giáo châu u.', false, NULL, '2024-03-10 16:47:59');
INSERT INTO public.plant_feed_back VALUES (23, 14, 3, 'Táo tây, còn gọi là bôm (phiên âm từ tiếng Pháp: pomme), là một loại quả ăn được từ cây táo tây (Malus domestica). Táo tây được trồng trên khắp thế giới và là loài cây được trồng phổ biến nhất trong chi Malus. Cây táo tây có nguồn gốc từ Trung Á, nơi tổ tiên của nó là táo dại Tân Cương sinh sống, hiện vẫn còn tồn tại cho đến ngày nay. Chúng đã được trồng từ hàng ngàn năm qua ở châu Á và châu u và đã được thực dân châu u đưa đến Bắc Mỹ. Táo tây có ý nghĩa tôn giáo và thần thoại trong nhiều nền văn hóa, bao gồm Bắc u, Hy Lạp và Cơ đốc giáo châu u.', false, NULL, '2024-03-10 16:48:02');
INSERT INTO public.plant_feed_back VALUES (24, 14, 3, 'Táo tây, còn gọi là bôm (phiên âm từ tiếng Pháp: pomme), là một loại quả ăn được từ cây táo tây (Malus domestica). Táo tây được trồng trên khắp thế giới và là loài cây được trồng phổ biến nhất trong chi Malus. Cây táo tây có nguồn gốc từ Trung Á, nơi tổ tiên của nó là táo dại Tân Cương sinh sống, hiện vẫn còn tồn tại cho đến ngày nay. Chúng đã được trồng từ hàng ngàn năm qua ở châu Á và châu u và đã được thực dân châu u đưa đến Bắc Mỹ. Táo tây có ý nghĩa tôn giáo và thần thoại trong nhiều nền văn hóa, bao gồm Bắc u, Hy Lạp và Cơ đốc giáo châu u.', false, NULL, '2024-03-10 16:48:04');
INSERT INTO public.plant_feed_back VALUES (25, 14, 3, 'Táo tây, còn gọi là bôm (phiên âm từ tiếng Pháp: pomme), là một loại quả ăn được từ cây táo tây (Malus domestica). Táo tây được trồng trên khắp thế giới và là loài cây được trồng phổ biến nhất trong chi Malus. Cây táo tây có nguồn gốc từ Trung Á, nơi tổ tiên của nó là táo dại Tân Cương sinh sống, hiện vẫn còn tồn tại cho đến ngày nay. Chúng đã được trồng từ hàng ngàn năm qua ở châu Á và châu u và đã được thực dân châu u đưa đến Bắc Mỹ. Táo tây có ý nghĩa tôn giáo và thần thoại trong nhiều nền văn hóa, bao gồm Bắc u, Hy Lạp và Cơ đốc giáo châu u.', false, NULL, '2024-03-10 16:48:07');
INSERT INTO public.plant_feed_back VALUES (26, 14, 3, 'Táo tây, còn gọi là bôm (phiên âm từ tiếng Pháp: pomme), là một loại quả ăn được từ cây táo tây (Malus domestica). Táo tây được trồng trên khắp thế giới và là loài cây được trồng phổ biến nhất trong chi Malus. Cây táo tây có nguồn gốc từ Trung Á, nơi tổ tiên của nó là táo dại Tân Cương sinh sống, hiện vẫn còn tồn tại cho đến ngày nay. Chúng đã được trồng từ hàng ngàn năm qua ở châu Á và châu u và đã được thực dân châu u đưa đến Bắc Mỹ. Táo tây có ý nghĩa tôn giáo và thần thoại trong nhiều nền văn hóa, bao gồm Bắc u, Hy Lạp và Cơ đốc giáo châu u.', false, NULL, '2024-03-10 16:48:09');
INSERT INTO public.plant_feed_back VALUES (27, 14, 3, 'Táo tây, còn gọi là bôm (phiên âm từ tiếng Pháp: pomme), là một loại quả ăn được từ cây táo tây (Malus domestica). Táo tây được trồng trên khắp thế giới và là loài cây được trồng phổ biến nhất trong chi Malus. Cây táo tây có nguồn gốc từ Trung Á, nơi tổ tiên của nó là táo dại Tân Cương sinh sống, hiện vẫn còn tồn tại cho đến ngày nay. Chúng đã được trồng từ hàng ngàn năm qua ở châu Á và châu u và đã được thực dân châu u đưa đến Bắc Mỹ. Táo tây có ý nghĩa tôn giáo và thần thoại trong nhiều nền văn hóa, bao gồm Bắc u, Hy Lạp và Cơ đốc giáo châu u.', false, NULL, '2024-03-10 16:48:11');
INSERT INTO public.plant_feed_back VALUES (28, 14, 3, 'Táo tây, còn gọi là bôm (phiên âm từ tiếng Pháp: pomme), là một loại quả ăn được từ cây táo tây (Malus domestica). Táo tây được trồng trên khắp thế giới và là loài cây được trồng phổ biến nhất trong chi Malus. Cây táo tây có nguồn gốc từ Trung Á, nơi tổ tiên của nó là táo dại Tân Cương sinh sống, hiện vẫn còn tồn tại cho đến ngày nay. Chúng đã được trồng từ hàng ngàn năm qua ở châu Á và châu u và đã được thực dân châu u đưa đến Bắc Mỹ. Táo tây có ý nghĩa tôn giáo và thần thoại trong nhiều nền văn hóa, bao gồm Bắc u, Hy Lạp và Cơ đốc giáo châu u.', false, NULL, '2024-03-10 16:48:13');
INSERT INTO public.plant_feed_back VALUES (30, 1, 3, '.', false, NULL, '2024-03-17 13:32:23');
INSERT INTO public.plant_feed_back VALUES (29, 1, 3, '.', true, '2024-03-20 01:07:29', '2024-03-17 13:02:35');
INSERT INTO public.plant_feed_back VALUES (31, 1, 3, 'fdgdgfytfyt', false, NULL, '2024-04-10 01:44:27');
INSERT INTO public.plant_feed_back VALUES (32, 1, 3, 'vcvcxffzdawrsetsetest', false, NULL, '2024-04-10 01:44:35');
INSERT INTO public.plant_feed_back VALUES (33, 6, 3, 'lalala', false, NULL, '2024-04-21 03:00:43');
INSERT INTO public.plant_feed_back VALUES (34, 6, 4, 'nhu cuc cuk', false, NULL, '2024-04-21 03:17:24');
INSERT INTO public.plant_feed_back VALUES (35, 1, 3, '127357eyibaydby', false, NULL, '2024-04-21 03:21:55');
INSERT INTO public.plant_feed_back VALUES (36, 1, 4, 'khahkahkah', false, NULL, '2024-04-21 03:22:28.217');


--
-- TOC entry 3859 (class 0 OID 29539)
-- Dependencies: 276
-- Data for Name: plant_posts; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.plant_posts VALUES (47, 1, 1, 'Tổ tiên hoang dại', '<p>Bài viết chi tiết: <a href="https://vi.wikipedia.org/wiki/Malus_sieversii">Malus sieversii</a>&nbsp;<br><br>Tổ tiên hoang dại của Malus domestica là Malus sieversii được tìm thấy mọc hoang ở vùng núi Trung Á tại miền nam Kazakhstan, Kyrgyzstan, Tajikistan và Tân Cương (Trung Quốc).[5][10] Việc trồng loài này rất có thể đã bắt đầu từ các khu rừng trên sườn núi của dãy Thiên Sơn, tiến triển trong một thời gian dài và cho phép lai nhập gen thứ cấp các gen từ các loài khác vào các hạt giống thụ phấn mở. Việc trao đổi di truyền đáng kể với Malus sylvestris, một loài táo dại châu Âu, dẫn đến kết quả là các quần thể táo tây hiện nay có liên hệ nhiều đến táo dại châu Âu hơn là tổ tiên giống Malus sieversii, dù về hình thái học thì nó gần với tổ tiên hơn. Trong các dòng táo gần đây thì không có xáo trộn di truyền, sự đóng góp của Malus sieversii vẫn chiếm ưu thế.</p>', '1712323529220', '1711268383179');
INSERT INTO public.plant_posts VALUES (48, 1, 1, 'Bộ gen', '<figure class="image image-style-side"><img style="aspect-ratio:220/165;" src="https://upload.wikimedia.org/wikipedia/commons/thumb/f/f3/Malus_domestica._Pumar%2C_mazanal.jpg/220px-Malus_domestica._Pumar%2C_mazanal.jpg" alt="img" width="220" height="165"></figure><p>Hoa táo tây</p><p>Táo tây là loài cây lưỡng bội (mặc dù các giống cây tam bội không phải là hiếm), có 2n = 34 nhiễm sắc thể[14] và kích thước bộ gen ước tính xấp xỉ 650 Mb. Một số trình tự gen đã được giải sẵn, lần đầu tiên là vào năm 2010 dựa trên giống cây lưỡng bội ''Golden Delicious''. Tuy nhiên, trình tự bộ gen đầu tiên này hóa ra có chứa một số lỗi,[16] một phần là do mức độ mất tính dị hợp tử cao ở táo lưỡng bội, kết hợp với một đoạn lặp của bộ gen cổ xưa, dẫn đến việc giải trình tự gen phức tạp hơn. Gần đây, các giống lưỡng bội và tam bội đã được xác định trình tự, do đó tạo ra trình tự toàn bộ bộ gen có chất lượng cao hơn.&nbsp;<br><br>Toàn bộ bộ gen đầu tiên được ước tính chứa khoảng 57.000 gen, mặc dù các trình tự gen gần đây đã cung cấp một ước tính vừa phải hơn từ 42.000 đến 44.700 gen mã hóa protein. Bên cạnh đó, toàn bộ trình tự bộ gen sẵn có đã cung cấp bằng chứng cho thấy tổ tiên hoang dại của táo trồng rất có thể là Malus sieversii. Việc tái giải trình tự qua nhiều lần đã củng cố cho nhận định này, đồng thời cũng cho thấy sự lai nhập gen sâu rộng từ Malus sylvestris sau khi M. sieversii được thuần hóa.</p>', '1712325405872', '1711268715261');
INSERT INTO public.plant_posts VALUES (46, 1, 1, 'Mô tả', '<p>&nbsp;</p><p>&nbsp;</p><p><strong>Táo</strong> là một loài cây rụng lá, cây thường có chiều cao từ 2 đến 4,5 m (6 đến 15 ft) trong môi trường canh tác và có chiều cao lên đến 9 m (30 ft) trong điều kiện tự nhiên.[4] Khi trồng, kích thước, hình dạng và mật độ cành được kiểm soát bằng phương pháp chọn lựa và chiết ghép cành. Các lá được sắp xếp xen kẽ, chúng có hình bầu dục màu xanh đậm với mép lá có hình răng cưa và mặt dưới có lông tơ nhỏ li ti.</p><figure class="image image_resized image-style-side" style="width:49.71%;"><img style="aspect-ratio:3840/2160;" src="https://res.cloudinary.com/dnvpdjbx0/image/upload/v1712242609/dacn_admin/fkqhmn2rvmgbapu9ltjq.jpg" width="3840" height="2160"></figure><p><br>Vào mùa xuân, hoa nở cùng lúc với sự phát triển mạnh mẽ của cây như đâm chồi của lá, chúng nở trên một số chồi dài và trên các ngọn của cành. Những bông hoa dài 3 đến 4 cm (1 đến 1+1⁄2 in) màu trắng pha chút màu hồng nhạt dần, có năm cánh, với một cụm hoa lớn trên cành hoặc thân, một cụm hoa gồm có 4–6 hoa. Hoa nằm ở trung tâm của cụm hoa được gọi là "hoa vương"; nó là hoa mở ra đầu tiên và có thể phát triển thành một quả lớn.&nbsp;<br><br>Tiết diện ngang quả táo cho thấy từ ngoài vào là vỏ mỏng (màu đỏ), thịt (trắng xanh), và ruột 5 múi với hột màu nâu.Quả táo tây là một loại quả pome, thường chín vào cuối mùa hè hoặc mùa thu, tháng 9 hoặc tháng 10, quả của các giống táo trồng hiện nay có nhiều kích cỡ khác nhau. Những người trồng táo tây thương mại đặt mục tiêu tạo ra quả táo có chiều dài từ 7 đến 8,5 cm (2+3⁄4 đến 3+1⁄4 in) nhằm đáp ứng thị hiếu của thị trường. Đối với một số người tiêu dùng, đặc biệt là người tiêu dùng Nhật Bản thì thích quả kích cỡ lớn, còn những quả táo dưới 5,5 cm (2+1⁄4 in) ít có giá trị trên thị trường thường được sử dụng để làm nước ép trái cây. Vỏ của quả táo tây khi chín thường có màu đỏ, vàng, lục, hồng, hoặc màu rám nắng, mặc dù nhiều giống có hai hoặc ba màu.[7] Vỏ quả cũng có thể bị rám toàn bộ hoặc một phần, tức là thô ráp và có màu nâu. Vỏ được bao phủ bởi một lớp sáp biểu bì.[8] Phần vỏ quả ngoài (dính cùi quả) thường có màu trắng vàng nhạt[7] đến màu hồng, cũng có loại táo tây có vỏ quả ngoài màu vàng hoặc xanh lá cây.</p><figure class="image"><img style="aspect-ratio:700/700;" src="https://res.cloudinary.com/dnvpdjbx0/image/upload/v1712997175/dacn_admin/rhvwiawj5d31lcy0r2bt.jpg" width="700" height="700"></figure><figure class="image"><img style="aspect-ratio:1024/451;" src="https://res.cloudinary.com/dnvpdjbx0/image/upload/v1712998705/dacn_admin/brlvrom764ytvmpbiqr4.jpg" width="1024" height="451"></figure><figure class="image"><img src="https://res.cloudinary.com/dnvpdjbx0/image/upload/v1712998790/dacn_admin/rqadxvxo0unar9zhqzr5.jpg"></figure><figure class="image"><img></figure>', '1712998753213', '1711268330391');


--
-- TOC entry 3857 (class 0 OID 29495)
-- Dependencies: 274
-- Data for Name: plant_stages; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.plant_stages VALUES (24, 2, 'Phát triển', 'Giai đoạn cây tiếp tục phát triển.', 35, 20, 10, 1, NULL, NULL);
INSERT INTO public.plant_stages VALUES (25, 5, 'Trưởng thành', 'Khi quả táo chín và sẵn sàng, chúng có thể được thu hoạch để sử dụng hoặc bán. Thu hoạch thường diễn ra vào mùa thu, khi quả đã đạt độ chín lý tưởng.', 35, 20, 999, 1, NULL, NULL);
INSERT INTO public.plant_stages VALUES (5, 3, 'Thu hoạch', 'Khi quả táo chín và sẵn sàng, chúng có thể được thu hoạch để sử dụng hoặc bán. Thu hoạch thường diễn ra vào mùa thu, khi quả đã đạt độ chín lý tưởng.
', 0, 0, 50, 1, '', NULL);
INSERT INTO public.plant_stages VALUES (6, 4, 'Nghỉ ngơi', 'Sau khi thu hoạch, cây táo tây có thể vào giai đoạn nghỉ đông trong mùa đông. Trong thời gian này, cây táo tây không hoạt động sản xuất và có thể mất lá. Giai đoạn này là quan trọng để cây có thể phục hồi và chuẩn bị cho mùa mọc mới.', 0, 0, 50, 1, '', NULL);
INSERT INTO public.plant_stages VALUES (4, 1, 'Nảy mầm', 'Giai đoạn bắt đầu khi hạt giống được gieo vào đất. Trong giai đoạn này, hạt giống sẽ nảy mầm và phát triển cành lá đầu tiên.', 80, 60, 10, 1, NULL, NULL);
INSERT INTO public.plant_stages VALUES (26, 1, 'Nảy mầm', '', 0, 0, 10, 4, NULL, NULL);


--
-- TOC entry 3854 (class 0 OID 29458)
-- Dependencies: 271
-- Data for Name: plants; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.plants VALUES (8, 'Sung', 'https://res.cloudinary.com/dnvpdjbx0/image/upload/v1712678547/dacn/mioy5n5kklmmjp149zoi.jpg', 'Cây cảnh', 'Cây sung hay ưu đàm thụ hoặc tụ quả dong (danh pháp hai phần: Ficus racemosa, đồng nghĩa Ficus glomerata Roxb., 1802) là loại thân cây gỗ lớn, mọc nhanh, thuộc họ Dâu tằm (Moraceae). Cây mọc hoang dại ở các vùng nhiệt đới và cận nhiệt đới tại những nơi đất ẩm bìa rừng, nhiều nhất là ven các bờ nước ao, hồ, sông, suối.');
INSERT INTO public.plants VALUES (3, 'Cam Sành', 'https://res.cloudinary.com/dnvpdjbx0/image/upload/v1711285920/dacn/hbelmpqlpjlr7p39r3aj.jpg', 'Cam', 'Cam sành là một giống cây ăn quả thuộc chi Cam chanh có quả gần như quả cam (Citrus reticulata x maxima), có nguồn gốc từ Việt Nam. Quả cam sành rất dễ nhận ra nhờ lớp vỏ dày, sần sùi giống bề mặt mảnh sành và thường có màu lục nhạt (khi chín có sắc cam), các múi thịt có màu cam.');
INSERT INTO public.plants VALUES (14, 'Tùng la hán lá dài', 'https://res.cloudinary.com/dnvpdjbx0/image/upload/v1712678720/dacn/gjdkv9jbsxxrdw0on88s.jpg', 'Cây cảnh', 'Tùng La hán lá dài hay còn gọi la hán tùng là một loài cây cảnh thuộc họ Thông tre có nguồn gốc từ Nhật Bản và Trung Quốc thường được trồng làm cảnh trong công viên, đình chùa, vườn nhà. Cây thuộc loại cây gỗ lớn, cành nhánh nhiều, dài, mọc ngang hay rủ xuống.');
INSERT INTO public.plants VALUES (27, 'Măng cụt Lái Thiêu', 'https://res.cloudinary.com/dnvpdjbx0/image/upload/v1712678977/dacn/k848eaqeralwyen4dxnq.jpg', 'Măng cụt', 'Măng Cụt Lái Thiêu là một trong những món đặc sản Bình Dương mà ai cũng muốn được thử một lần trong đời. Với hương vị ngọt ngào như chính lòng hiếu khách của người Bình Dương – măng cụt Lái Thiêu sẽ khiến du khách nhớ mãi không quên về mảnh đất hữu tình này.');
INSERT INTO public.plants VALUES (30, 'Táo Gala', 'https://res.cloudinary.com/dnvpdjbx0/image/upload/v1712679130/dacn/oxgfsxm02gew9ewsbqiz.jpg', 'Táo', 'Táo Gala là dòng trái cây nhập khẩu đang được đông đảo người dùng ưa chuộng tại Việt Nam. Với độ giòn, ngọt và giá thành phải chăng, hiện dòng táo này được bày bán rất nhiều tại các siêu thị, cửa hàng hoa quả nhập khẩu. Trong bài viết này, Citi Fruit sẽ chia sẻ đến bạn đầy đủ những thông tin hữu ích về dòng quả cao cấp.');
INSERT INTO public.plants VALUES (39, 'Mai vàng', 'https://res.cloudinary.com/dnvpdjbx0/image/upload/v1712679335/dacn/okahpohkkzf3a2o73j64.jpg', 'Hoa', 'Mai vàng (còn gọi là hoàng mai, huỳnh mai hay lão mai, danh pháp khoa học: Ochna integerrima) là loài thực vật có hoa thuộc chi Mai (Ochna), họ Mai (Ochnaceae). Cây này được trồng làm cảnh phổ biến ở miền Nam Việt Nam vào dịp Tết Nguyên Đán.[1]');
INSERT INTO public.plants VALUES (4, 'Lộc vừng', 'https://res.cloudinary.com/dnvpdjbx0/image/upload/v1712677769/dacn/sej7is9bktrvkhs52fjq.jpg', 'Cây cảnh', 'Lộc vừng, còn gọi là chiếc hay lộc mưng (danh pháp khoa học: Barringtonia acutangula) là một loài thuộc chi Lộc vừng, là loài cây bản địa của các vùng đất ẩm ven biển Nam Á và Bắc Úc, từ Afghanistan đến Philippin và Queensland.[1]');
INSERT INTO public.plants VALUES (6, 'Táo Táo Gia Lộc', 'https://res.cloudinary.com/dnvpdjbx0/image/upload/v1712678061/dacn/x889m1if3dtx43rhaoxu.jpg', 'Táo', 'Táo Gia Lộc có quả hình trái xoan, khi chín có màu vàng tươi, vị hơi chua. Táo cho sai quả, năng suất cao và ổn định, ở nhiều vùng trồng 1 năm cho 2 lần quả.');
INSERT INTO public.plants VALUES (7, 'Cam Cao Phong', 'https://res.cloudinary.com/dnvpdjbx0/image/upload/v1712678170/dacn/dszvp93gbgssygkqa8fz.jpg', 'Cam', 'Nhắc đến cam Cao Phong là người ta sẽ nhớ ngay đến mảnh đất Hòa Bình trù phú. Mảnh đất này không biết từ bao giờ đã trở thành nơi sở hữu loại đặc sản ngon ngọt, mọng nước nức tiếng.');
INSERT INTO public.plants VALUES (1, 'Táo Tây', 'https://afamilycdn.com/k:thumb_w/600/BbnKCSyXiBaccccccccccccFlVeI2z/Image/2014/01/Corbis-42-51949669-4a8cc/6-goi-y-thu-vi-voi-mon-tao-tay-cho-be.jpg', 'Táo', 'Táo tây, còn gọi là bôm (phiên âm từ tiếng Pháp: pomme), là một loại quả ăn được từ cây táo tây (Malus domestica). Táo tây được trồng trên khắp thế giới và là loài cây được trồng phổ biến nhất trong chi Malus. Cây táo tây có nguồn gốc từ Trung Á, nơi tổ tiên của nó là táo dại Tân Cương sinh sống, hiện vẫn còn tồn tại cho đến ngày nay. Chúng đã được trồng từ hàng ngàn năm qua ở châu Á và châu Âu và đã được thực dân châu Âu đưa đến Bắc Mỹ. Táo tây có ý nghĩa tôn giáo và thần thoại trong nhiều nền văn hóa, bao gồm Bắc Âu, Hy Lạp và Cơ đốc giáo châu Âu.');


--
-- TOC entry 3875 (class 0 OID 29748)
-- Dependencies: 292
-- Data for Name: reminder_tree; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.reminder_tree VALUES (108, 51);
INSERT INTO public.reminder_tree VALUES (107, 57);
INSERT INTO public.reminder_tree VALUES (107, 35);
INSERT INTO public.reminder_tree VALUES (109, 58);


--
-- TOC entry 3872 (class 0 OID 29732)
-- Dependencies: 289
-- Data for Name: reminders; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.reminders VALUES (108, 3, 'Tuần', '2024-04-24', '2024-05-01', '06:00:00', 'abc');
INSERT INTO public.reminders VALUES (107, 3, 'Ngày', '2024-04-22', NULL, '02:00:00', 'Tưới cây');
INSERT INTO public.reminders VALUES (110, 3, 'Ngày', '2024-04-30', '2024-05-01', '00:00:00', 'Nhổ cỏ');
INSERT INTO public.reminders VALUES (109, 3, NULL, '2024-04-26', '2024-04-27', '20:00:00', 'Bón phân');


--
-- TOC entry 3879 (class 0 OID 29782)
-- Dependencies: 296
-- Data for Name: tree_diaries; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.tree_diaries VALUES (72, 51, 1, 1, 'Hello', 'Nắng', NULL, '2024-04-05', 0);
INSERT INTO public.tree_diaries VALUES (64, 51, 2, 1, '', 'Nắng', 'null', '2024-04-27', 0);
INSERT INTO public.tree_diaries VALUES (2, 35, 1, 1, 'note', 'Nắng', '../image/diary_img_bg.png', '2024-03-01', 0);
INSERT INTO public.tree_diaries VALUES (59, 57, 1, 1, 'Cây được trồng', 'Nắng', NULL, '2024-03-15', 0);
INSERT INTO public.tree_diaries VALUES (12, 38, 1, 1, '', 'Có gió', NULL, '2024-03-20', 0);
INSERT INTO public.tree_diaries VALUES (13, 38, 1, 1, '', 'Nắng', NULL, '2024-03-19', 0);
INSERT INTO public.tree_diaries VALUES (14, 38, 2, 1, '', 'Nắng', NULL, '2024-03-24', 0);
INSERT INTO public.tree_diaries VALUES (15, 38, 2, 1, '', 'Nắng', NULL, '2024-03-25', 0);
INSERT INTO public.tree_diaries VALUES (16, 38, 1, 1, '', 'Nắng', NULL, '2024-03-26', 0);
INSERT INTO public.tree_diaries VALUES (17, 38, 2, 1, 'Cây có dấu hiệu bị bệnh', 'Nắng', 'https://res.cloudinary.com/dnvpdjbx0/image/upload/v1710899908/dacn/dmufksutdqyskudnu6ps.jpg', '2024-03-26', 0);
INSERT INTO public.tree_diaries VALUES (3, 35, 2, 1, 'ghi chú 2', 'Nắng', '../image/diary_img_bg.png', '2024-03-26', 0);
INSERT INTO public.tree_diaries VALUES (6, 35, 2, 1, '123', 'Nắng', 'https://res.cloudinary.com/dnvpdjbx0/image/upload/v1710857319/dacn/tnwisw4wf6avvqbdc1co.jpg', '2024-03-27', 0);
INSERT INTO public.tree_diaries VALUES (9, 35, 1, 1, 'aaaaa', 'Có gió', NULL, '2024-03-03', 1);
INSERT INTO public.tree_diaries VALUES (53, 35, 2, 1, '', 'Nắng', NULL, '2024-04-13', 0);
INSERT INTO public.tree_diaries VALUES (1, 35, 1, 1, 'note 1', 'Nắng', '../image/diary_img_bg.png', '2024-03-18', 1);
INSERT INTO public.tree_diaries VALUES (74, 58, 1, 1, 'Tets last', 'Mưa', 'null', '2023-12-31', 0);
INSERT INTO public.tree_diaries VALUES (78, 58, 2, 1, '', 'Nắng', NULL, '2024-05-01', 0);
INSERT INTO public.tree_diaries VALUES (60, 58, 1, 1, 'Cây được trồng...', 'Nắng', 'null', '2024-04-01', 0);
INSERT INTO public.tree_diaries VALUES (80, 58, 4, 1, '', 'Nắng', NULL, '2024-05-07', 3);
INSERT INTO public.tree_diaries VALUES (73, 57, 3, 1, '', 'Nắng', 'https://res.cloudinary.com/dnvpdjbx0/image/upload/v1714207447/dacn/fn1hvbfxvbdxxim7dagv.jpg', '2024-04-24', 0);
INSERT INTO public.tree_diaries VALUES (83, 51, 5, 1, '', 'Nắng', NULL, '2024-05-08', 2);
INSERT INTO public.tree_diaries VALUES (8, 35, 1, 1, 'lorem ispum... haha', 'Mưa', NULL, '2024-03-02', 0);
INSERT INTO public.tree_diaries VALUES (66, 51, 2, 1, '', 'Nắng', NULL, '2024-04-27', 0);
INSERT INTO public.tree_diaries VALUES (44, 51, 1, 1, 'Cây được mua về ngày đầu tiên.', 'Nắng', 'https://res.cloudinary.com/dnvpdjbx0/image/upload/v1712984077/dacn/zdduahgjvnpltub6np77.png', '2024-04-13', 0);
INSERT INTO public.tree_diaries VALUES (70, 51, 2, 1, '', 'Nắng', NULL, '2024-04-27', 0);


--
-- TOC entry 3863 (class 0 OID 29639)
-- Dependencies: 280
-- Data for Name: trees; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.trees VALUES (51, 'Cần sa', 3, 1, 75, 'cây thử nghiệm, không cho cs thấy', '2024-05-13 13:54:48.605937', '2024-02-13');
INSERT INTO public.trees VALUES (62, 'Mai', 3, 39, 74, 'Cây đẹp', '2024-05-13 13:54:48.633894', '2024-02-13');
INSERT INTO public.trees VALUES (57, 'Táo 123', 3, 1, 74, 'Người ơi người ở đừng về', '2024-05-13 13:54:48.661579', '2024-02-13');
INSERT INTO public.trees VALUES (38, 'Táo', 8, 1, NULL, 'abc', '2024-05-13 13:54:48.690473', '2024-02-13');
INSERT INTO public.trees VALUES (39, 'táo', 8, 1, NULL, '', '2024-05-13 13:54:48.719573', '2024-02-13');
INSERT INTO public.trees VALUES (40, 'Cây táo tây', 10, 1, 72, 'abcasdas', '2024-05-13 13:54:48.748891', '2024-02-13');
INSERT INTO public.trees VALUES (42, 'Cam', 10, 3, NULL, '', '2024-05-13 13:54:48.779083', '2024-02-13');
INSERT INTO public.trees VALUES (43, 'Cam1', 10, 3, 73, '', '2024-05-13 13:54:48.808243', '2024-02-13');
INSERT INTO public.trees VALUES (35, 'Táo tây', 3, 1, 75, '', '2024-05-13 13:54:48.836909', '2024-02-13');
INSERT INTO public.trees VALUES (58, 'Táo deff', 3, 1, NULL, '', '2024-05-13 14:10:48.850185', '2024-02-12');


--
-- TOC entry 3852 (class 0 OID 29428)
-- Dependencies: 269
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.users VALUES (3, 'user@gmail.com', '123', 'user', '0846141788', 'user123@gmail.com', 'VN', 'USER', '2024-05-11');
INSERT INTO public.users VALUES (4, 'admin@gmail.com', '123', 'admin', '0846141788', 'admin123@gmail.com', 'VN', 'ADMIN', '2024-05-11');
INSERT INTO public.users VALUES (5, 'user@gmail.com', '123', 'user', NULL, NULL, NULL, NULL, '2024-05-11');
INSERT INTO public.users VALUES (6, 'khaizinam@gmail.com', '123', 'khai', NULL, NULL, NULL, NULL, '2024-05-11');
INSERT INTO public.users VALUES (7, 'test@gmail.com', '123456', 'tan', NULL, NULL, NULL, 'ADMIN', '2024-05-11');
INSERT INTO public.users VALUES (14, 'khai12@gmail.com', '123', 'khai nguyen huu', NULL, NULL, NULL, 'USER', '2024-03-01');
INSERT INTO public.users VALUES (15, 'khaidz@gmail.com', '123', 'khai dep zai', NULL, NULL, NULL, 'USER', '2024-04-30');
INSERT INTO public.users VALUES (10, 'abc@gmail.com', '123', 'abc', NULL, NULL, NULL, 'USER', '2024-01-01');
INSERT INTO public.users VALUES (11, 'khai@gmail.com', '123', 'Khai', NULL, NULL, NULL, 'USER', '2024-05-13');
INSERT INTO public.users VALUES (8, 'khanh@gmail.com', '123', 'Khanh', NULL, NULL, NULL, 'USER', '2023-05-11');


--
-- TOC entry 3870 (class 0 OID 29716)
-- Dependencies: 287
-- Data for Name: water_schedules; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3904 (class 0 OID 0)
-- Dependencies: 277
-- Name: areas_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.areas_id_seq', 76, true);


--
-- TOC entry 3905 (class 0 OID 0)
-- Dependencies: 283
-- Name: plant_feed_back_author_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.plant_feed_back_author_id_seq', 1, false);


--
-- TOC entry 3906 (class 0 OID 0)
-- Dependencies: 281
-- Name: plant_feed_back_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.plant_feed_back_id_seq', 36, true);


--
-- TOC entry 3907 (class 0 OID 0)
-- Dependencies: 282
-- Name: plant_feed_back_plant_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.plant_feed_back_plant_id_seq', 1, false);


--
-- TOC entry 3908 (class 0 OID 0)
-- Dependencies: 275
-- Name: plant_posts_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.plant_posts_id_seq', 50, true);


--
-- TOC entry 3909 (class 0 OID 0)
-- Dependencies: 272
-- Name: plant_stages_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.plant_stages_id_seq', 27, true);


--
-- TOC entry 3910 (class 0 OID 0)
-- Dependencies: 273
-- Name: plant_stages_plant_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.plant_stages_plant_id_seq', 2, true);


--
-- TOC entry 3911 (class 0 OID 0)
-- Dependencies: 270
-- Name: plants_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.plants_id_seq', 41, true);


--
-- TOC entry 3912 (class 0 OID 0)
-- Dependencies: 290
-- Name: reminder_tree_reminder_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.reminder_tree_reminder_id_seq', 1, false);


--
-- TOC entry 3913 (class 0 OID 0)
-- Dependencies: 291
-- Name: reminder_tree_tree_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.reminder_tree_tree_id_seq', 1, false);


--
-- TOC entry 3914 (class 0 OID 0)
-- Dependencies: 288
-- Name: reminders_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.reminders_id_seq', 110, true);


--
-- TOC entry 3915 (class 0 OID 0)
-- Dependencies: 293
-- Name: tree_diaries_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.tree_diaries_id_seq', 83, true);


--
-- TOC entry 3916 (class 0 OID 0)
-- Dependencies: 295
-- Name: tree_diaries_plant_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.tree_diaries_plant_id_seq', 1, false);


--
-- TOC entry 3917 (class 0 OID 0)
-- Dependencies: 294
-- Name: tree_diaries_tree_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.tree_diaries_tree_id_seq', 1, true);


--
-- TOC entry 3918 (class 0 OID 0)
-- Dependencies: 279
-- Name: trees_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.trees_id_seq', 62, true);


--
-- TOC entry 3919 (class 0 OID 0)
-- Dependencies: 268
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.users_id_seq', 15, true);


--
-- TOC entry 3920 (class 0 OID 0)
-- Dependencies: 286
-- Name: water_schedules_area_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.water_schedules_area_id_seq', 1, false);


--
-- TOC entry 3921 (class 0 OID 0)
-- Dependencies: 285
-- Name: water_schedules_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.water_schedules_id_seq', 1, false);


--
-- TOC entry 3680 (class 2606 OID 29613)
-- Name: areas areas_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.areas
    ADD CONSTRAINT areas_pk PRIMARY KEY (id);


--
-- TOC entry 3684 (class 2606 OID 29662)
-- Name: plant_feed_back plant_feed_back_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.plant_feed_back
    ADD CONSTRAINT plant_feed_back_pkey PRIMARY KEY (id);


--
-- TOC entry 3678 (class 2606 OID 29546)
-- Name: plant_posts plant_posts_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.plant_posts
    ADD CONSTRAINT plant_posts_pkey PRIMARY KEY (id);


--
-- TOC entry 3676 (class 2606 OID 29503)
-- Name: plant_stages plant_stages_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.plant_stages
    ADD CONSTRAINT plant_stages_pkey PRIMARY KEY (id);


--
-- TOC entry 3674 (class 2606 OID 29465)
-- Name: plants plants_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.plants
    ADD CONSTRAINT plants_pkey PRIMARY KEY (id);


--
-- TOC entry 3688 (class 2606 OID 29740)
-- Name: reminders reminders_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.reminders
    ADD CONSTRAINT reminders_pkey PRIMARY KEY (id);


--
-- TOC entry 3690 (class 2606 OID 29791)
-- Name: tree_diaries tree_diaries_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tree_diaries
    ADD CONSTRAINT tree_diaries_pkey PRIMARY KEY (id);


--
-- TOC entry 3682 (class 2606 OID 29646)
-- Name: trees trees_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.trees
    ADD CONSTRAINT trees_pkey PRIMARY KEY (id);


--
-- TOC entry 3672 (class 2606 OID 29435)
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- TOC entry 3686 (class 2606 OID 29724)
-- Name: water_schedules water_schedules_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.water_schedules
    ADD CONSTRAINT water_schedules_pkey PRIMARY KEY (id);


--
-- TOC entry 3700 (class 2620 OID 29649)
-- Name: trees update_modified_time; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER update_modified_time BEFORE UPDATE ON public.trees FOR EACH ROW EXECUTE FUNCTION public.update_modified_column();


--
-- TOC entry 3695 (class 2606 OID 29725)
-- Name: water_schedules fk_area_water_schedule; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.water_schedules
    ADD CONSTRAINT fk_area_water_schedule FOREIGN KEY (area_id) REFERENCES public.areas(id);


--
-- TOC entry 3693 (class 2606 OID 29668)
-- Name: plant_feed_back fk_author_feed_back; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.plant_feed_back
    ADD CONSTRAINT fk_author_feed_back FOREIGN KEY (author_id) REFERENCES public.users(id);


--
-- TOC entry 3692 (class 2606 OID 29564)
-- Name: plant_posts fk_plant; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.plant_posts
    ADD CONSTRAINT fk_plant FOREIGN KEY (plant_id) REFERENCES public.plants(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3694 (class 2606 OID 29663)
-- Name: plant_feed_back fk_plant_feed_back; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.plant_feed_back
    ADD CONSTRAINT fk_plant_feed_back FOREIGN KEY (plant_id) REFERENCES public.plants(id);


--
-- TOC entry 3691 (class 2606 OID 29569)
-- Name: plant_stages fk_plant_plant_stages; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.plant_stages
    ADD CONSTRAINT fk_plant_plant_stages FOREIGN KEY (plant_id) REFERENCES public.plants(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3696 (class 2606 OID 29877)
-- Name: reminder_tree fk_reminder_tree_reminer; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.reminder_tree
    ADD CONSTRAINT fk_reminder_tree_reminer FOREIGN KEY (reminder_id) REFERENCES public.reminders(id) ON DELETE CASCADE;


--
-- TOC entry 3697 (class 2606 OID 29882)
-- Name: reminder_tree fk_reminder_tree_tree; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.reminder_tree
    ADD CONSTRAINT fk_reminder_tree_tree FOREIGN KEY (tree_id) REFERENCES public.trees(id) ON DELETE CASCADE;


--
-- TOC entry 3698 (class 2606 OID 29797)
-- Name: tree_diaries fk_tree_diary_plant; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tree_diaries
    ADD CONSTRAINT fk_tree_diary_plant FOREIGN KEY (plant_id) REFERENCES public.plants(id);


--
-- TOC entry 3699 (class 2606 OID 29823)
-- Name: tree_diaries fk_tree_diary_tree; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tree_diaries
    ADD CONSTRAINT fk_tree_diary_tree FOREIGN KEY (tree_id) REFERENCES public.trees(id) ON DELETE CASCADE;


-- Completed on 2024-05-13 21:47:39

--
-- PostgreSQL database dump complete
--

