/* eslint-disable no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
import "./Statistics.scss"

import { useEffect, useState } from 'react';
import { toast } from "react-toastify";

import GeneralStatusStatistic from "../../components/GeneralStatusStatistic/GeneralStatusStatistic";
import StatisticsNewSubscriber from "./child/StatisticsNewSubscriber/StatisticsNewSubscriber";
import TreeStatistic from "./child/TreeStatistic/TreeStatistic";
import StatusStatistic from "./child/StatusStatistic/StatusStatistic"
import GeneralStatistic from "./child/GeneralStatistic/GeneralStatistic";
import StatisticService from '../../services/statisticService';

export default function Statistics() {

  const [dataGeneralStatus, setDataGeneralStatus] = useState([]);
    const getNumberStatuses = async () => {
    try {
      const { data } = await StatisticService.getNumberStatuses();
      setDataGeneralStatus(data);
    } catch (error) {
      if (!error.response) {
        toast.error("Không tải được dữ liệu");
      } else {
        toast.error("Hệ thống bị lỗi");
      }
    }
  }

  useEffect(function () {
    getNumberStatuses();
  }, []);

  return (
    <div className="statistic-container">
      <GeneralStatistic/>
      <StatisticsNewSubscriber />
      <div style={{height: '8px'}}></div>
      <TreeStatistic/>
      <GeneralStatusStatistic
        dataStatus={dataGeneralStatus}
      />
      <StatusStatistic/>
    </div>
  );
}



