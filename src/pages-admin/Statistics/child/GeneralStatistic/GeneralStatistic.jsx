import "./GeneralStatistic.scss";

import { useEffect, useState } from 'react';
import { toast } from "react-toastify";
import ChartService from '../../../../services/chartService';
import { ERROR_NAME } from "../../../../utils/utils";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUser, faCommentDots, faTree} from "@fortawesome/free-solid-svg-icons";

const fontSize = 20
const iconColor = "white"
function styleBackGround(colorHex) {
  return {
    backgroundColor: `${colorHex}`
  }
}

const iconArr = [faUser, faCommentDots, faTree]
/*
generalData = {
  bgColor:  string
  name: string,
  count: integer,
  increase_in_day: integer
}
*/
const GeneralStatistic = () => {
  const [generalData, setGeneralData] = useState([]);
  useEffect(() => {
    getNumberStatuses();
  },[])
  const getNumberStatuses = async () => {
    try {
      const res = await ChartService.getGeneralStatisticData();
      setGeneralData(res?.data);
    } catch (error) {
      if (!error.response) {
        toast.error(ERROR_NAME.DATA_IS_NOT_LOADED)
      }
      else {
        toast.error(ERROR_NAME.SYSTEM_ERROR)
      }
    }
  }

  return (
    <div className="general-statistic-container">
      {generalData?.length > 0 ? 
        generalData.map((ele, index) => {
          return (
            <div className="statistic-tag" key={index}>
              <div className="left-content">
                <div className="icon-content">
                  <div className="icon-wrapper" style={styleBackGround(ele?.bgColor)}>
                    <FontAwesomeIcon icon={ iconArr[index % 3] } color={iconColor} fontSize={fontSize}/>
                  </div>
                </div>
                <div className="sum-data-content">
                  <div className="title">
                    Tổng {ele?.name} 
                  </div>
                  <div className="value">
                    {ele?.count}
                  </div>
                </div>
              </div>
              <div className="right-content">
                <div className="number-increase">
                  +{ele?.increase_in_day}
                </div>
                <div className="title">
                  Trong ngày
                </div>
              </div>
            </div>
                );
              })
              : <></>}
    </div>
  );
};

export default GeneralStatistic;
