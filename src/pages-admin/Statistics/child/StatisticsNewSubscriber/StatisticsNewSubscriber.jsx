/* eslint-disable no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
import "./StatisticsNewSubscriber.scss"

import { useEffect, useState } from 'react';
import { toast } from "react-toastify";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend,
} from 'chart.js';
import { Bar } from 'react-chartjs-2';
import { Form, Select, Col, Row, Spin} from "antd";

import {
  SpanLabel,
  CustomStyleFormItem,
  CustomSpin
} from "../../Custom.style";

import StatisticService from '../../../../services/statisticService';
import { ERROR_NAME } from "../../../../utils/utils";

ChartJS.register(
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend
);

// const barThickness = 10;

export default function StatisticsNewSubscriber() {
  const [form] = Form.useForm();

  const [dataGeneralStatus, setDataGeneralStatus] = useState([]);

  const [barThickness, setBarThickness] = useState(10);
  const [data, setData] = useState(null);
  const [themes, setThemes] = useState([]);
  const [dataCSV, setDataCSV] = useState([]);
  const [firstTime, setFirstTime] = useState(new Date());

  const [year, setYear] = useState((new Date()).getFullYear());
  const [isLoadingChartData, setIsLoadingChartData] = useState(false);

  useEffect(() => {
    const handleResize = () => {
      if (window.innerWidth > 1024) {
        // if (barThickness != 10)
          setBarThickness(10)
      } else {
        // if (barThickness != 5)
          setBarThickness(4)
      }
    };

    window.addEventListener('resize', handleResize);

    // Cleanup function
    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  const getDataAPI = async () => {
    try {
      setIsLoadingChartData(true);
      const res = await StatisticService.getNewSubscriberStatisticalData(year);
      setThemes(res?.data['theme']);
      setDataCSV(res?.data['data']);
      setIsLoadingChartData(false);
    } catch (error) {
      if (!error.response) {
        toast.error(ERROR_NAME.DATA_IS_NOT_LOADED)
      }
      else {
        toast.error(ERROR_NAME.SYSTEM_ERROR)
      }
    }
  }

  const getFirstUserRegisterTime = async () => {
    try {
      const { data } = await StatisticService.getFirstUserRegisterTime();
      setFirstTime(new Date(data));
    } catch (error) {
      if (data ?? false) {
        toast.error(ERROR_NAME.SYSTEM_ERROR)
      }
      else {
        toast.error(ERROR_NAME.DATA_IS_NOT_LOADED)
      }
    }
  }

  useEffect(function () {
    getFirstUserRegisterTime();
  }, []);

  useEffect(function () {
    if (year ?? false) {
      getDataAPI();
    }
    else {
      setDataCSV(null);
      setThemes([]);
    }
  }, [year]);

  useEffect(function () {
    setData(getChartData(themes, dataCSV))
    // if (themes && dataCSV) setData(getChartData(themes, dataCSV))

  }, [themes, dataCSV]);

  function getDataColum(col) {
    const dataCol = [];
    dataCSV.forEach((e, index) => {
      if (index !== 0) {
        dataCol.push(e[col]);
      }
    })
    // return dataCol;

    let missingPart = []
    missingPart = Array.from({ length: 12 - dataCol.length + 1 }, () => 0);
    return [...dataCol, ...missingPart];
  }
  function getFieldNM(row) {
    const field = [];
    row.forEach((element, index) => {
      if (index !== 0) {
        field.push(element);
      }
    });
    return field;
  }
  function getDataCombine(themes, dataCSV) {
    const FieldNM = getFieldNM(dataCSV[0]);
    return FieldNM.map((ele, index) => {
      return ({
        label: ele,
        data: getDataColum(index + 1),
        backgroundColor: themes[index],
        barThickness: barThickness
      })
    })
  }

  function getLabels(dataCSV) {
    const label = []
    dataCSV.forEach((element, index) => {
      if (index !== 0) {
        label.push(element[0]);
      }
    });
    let missingPart = []
    missingPart = Array.from({ length: 12 - label.length }, (_, index) => index + label.length + 1);
    return [...label, ...missingPart];
    // return label;
  }

  function getChartData(themes, dataCSV) {
    if (!themes || !dataCSV || themes.length === 0 || dataCSV.length === 0) {
      return null;
    }
    return ({
      labels: getLabels(dataCSV),
      datasets: getDataCombine(themes, dataCSV)
    });
  };

  // const getSumData = () => {
  //   return dataCSV?.slice(1).reduce((acc, innerArray) => {
  //       innerArray.forEach((num, index) => {
  //           acc[index] = (acc[index] || 0) + num;
  //       });
  //       return acc;
  //   }, [])?.slice(1);
  // }

  const yearOptions = () => {
    let start = firstTime.getFullYear();
    let end = (new Date()).getFullYear();
    return Array.from({length: end - start + 1 }, (_, index) => index + start)
      .map((ele) => {
        return {
          value: ele,
          label: ele,
          }
      })
  }

  function findValueDataMax() {
    let arrSum = dataCSV?.slice(1).map((ele, index) => {
      let sum = ele?.slice(1).reduce((accumulator, currentValue) => {
          return accumulator + currentValue
      }, 0);
      return sum;
    })
    if (arrSum?.length >= 0) return Math.max(...arrSum);
    else return 0;
  }

  const handleChangeYear = (value) => {
    setYear(value);
  }

  const options = {
    plugins: {
      title: {
        display: true,
        // text: 'Bảng thống kê số người dùng đăng ký mới',
        text: `Bảng thống kê số ${dataCSV.length > 0 ? dataCSV[0][1] : ""}`,
      },
      // datalabels: {
      //   display: true,
      //   align: 'end', // Điều chỉnh vị trí hiển thị của đơn vị
      //   // anchor: 'end', // Điều chỉnh vị trí neo của đơn vị
      //   formatter: function(value, context) {
      //     return value; // Format lại nếu cần
      //   },
      // },
    },
    responsive: true,
    maintainAspectRatio: false, // Cho phép phóng to/kéo nhỏ
    layout: {
      padding: {
        top: 20, // Phần đệm phía trên để tránh chạm vào tiêu đề hoặc chú thích
      },
    },
    scales: {
      x: {
        stacked: true,
        beginAtZero: true, // Bắt đầu từ 0
        title: {
          display: true,
          text: "Tháng",
          font: {
            weight: 'bold', // Đặt font in đậm cho tiêu đề
          },
        },
      },
      y: {
        stacked: true,
        beginAtZero: true, // Bắt đầu từ 0
        suggestedMax: findValueDataMax() * 1.2, // Giá trị tối đa của trục y
        ticks: {
          stepSize: 1, // Đây là cấu hình để đặt độ chia nhỏ nhất là 1
        },
        title: {
          display: true,
          text: "Người",
          font: {
            weight: 'bold', // Đặt font in đậm cho tiêu đề
          },
        },
      },
    },
  };
  return (
    <>
    <div className="statistics-new-subscriber-container">
      
        <div className="filter-chart">
          <Form
            size="middle"
            className="chart-mode"
            form={form}
            colon={false}
            layout="vertical"
          >
            <Row gutter={[20, 0]} style={{ margin: "0 15px" }}>
              <Col xs={6} sm={6} md={6} lg={6} xl={6} style={{ paddingLeft: "0" }}>
                <CustomStyleFormItem
                  name="year"
                  label={<SpanLabel>Năm:</SpanLabel>}
                  initialValue={year}
                  rules={[
                    {
                      required: true,
                      pattern: /\S/,
                      message: "This field is required",
                    },
                  ]}
                >
                  <Select
                    style={{ width: "100%" }}
                    onChange={handleChangeYear}
                    options={
                      yearOptions()
                    }
                  />
                </CustomStyleFormItem>
              </Col>
            </Row>
          </Form>
        </div>
          <CustomSpin spinning={isLoadingChartData} style={{ zIndex: '5000' }}>
          <div className="main-chart">
              {data ? <Bar options={options} data={data} /> : <span>Không có dữ liệu để thống kê</span>}
          </div>
          </CustomSpin>
    </div>
    </>
  );
}



