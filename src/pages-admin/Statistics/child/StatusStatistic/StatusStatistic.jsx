/* eslint-disable no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
import "./StatusStatistic.scss"

import { useEffect, useState } from 'react';
import { toast } from "react-toastify";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend,
} from 'chart.js';
import { Bar } from 'react-chartjs-2';
import { Form, Select, Col, Row, Spin} from "antd";

import {
  SpanLabel,
  CustomStyleFormItem,
  CustomSpin
} from "../../Custom.style";

import StatisticService from '../../../../services/statisticService';
import { ERROR_NAME } from "../../../../utils/utils";

ChartJS.register(
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend
);

// const barThickness = 10;

export default function StatusStatistic() {
  const [form] = Form.useForm();

  const [dataGeneralStatus, setDataGeneralStatus] = useState([]);

  const [barThickness, setBarThickness] = useState(10);
  const [data, setData] = useState(null);
  const [themes, setThemes] = useState([]);
  const [dataCSV, setDataCSV] = useState([]);
  const [firstTime, setFirstTime] = useState(new Date());

  const [year, setYear] = useState((new Date()).getFullYear());
  const [month, setMonth] = useState('');
  const [isLoadingData, setIsLoadingData] = useState(false);

  useEffect(() => {
    const handleResize = () => {
      if (window.innerWidth > 1024) {
        // if (barThickness != 10)
          setBarThickness(10)
      } else {
        // if (barThickness != 5)
          setBarThickness(4)
      }
    };

    window.addEventListener('resize', handleResize);

    // Cleanup function
    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  // const getNumberStatuses = async () => {
  //   try {
  //     const res = await StatisticService.getNumberStatuses();
  //     setDataGeneralStatus(res?.data);
  //   } catch (error) {
  //     console.log(error)
  //   }
  // }
  const getNumberStatuses = async () => {
    try {
      const { data } = await StatisticService.getNumberStatuses();
      setDataGeneralStatus(data);
    } catch (error) {
      if (!error.response) {
        toast.error(ERROR_NAME.DATA_IS_NOT_LOADED)
      }
      else {
        toast.error(ERROR_NAME.SYSTEM_ERROR)
      }
    }
  }

  const getDataAPI = async () => {
    try {
      setIsLoadingData(true);
      const res = await StatisticService.getTreeStatusData({ year, month : (month !== '' ? month : null) });
      setThemes(res?.data['theme']);
      setDataCSV(res?.data['data']);
      setIsLoadingData(false);
    } catch (error) {
      if (!error.response) {
        toast.error(ERROR_NAME.DATA_IS_NOT_LOADED)
      }
      else {
        toast.error(ERROR_NAME.SYSTEM_ERROR)
      }
    }
  }

  const getFistDiaryTime = async () => {
    setIsLoadingData(true);
    const res = await StatisticService.getFirstDiaryTime();
    setFirstTime(new Date(res?.data?.date_published));
    setIsLoadingData(false);
  }

  useEffect(function () {
    getNumberStatuses();
    getFistDiaryTime();
  }, []);

  useEffect(function () {
    if (year ?? false) {
      getDataAPI();
    }
    else {
      setDataCSV(null);
      setThemes([]);
    }
  }, [year, month]);

  useEffect(function () {
    setData(getChartData(themes, dataCSV))
    // if (themes && dataCSV) setData(getChartData(themes, dataCSV))

  }, [themes, dataCSV]);
  
  useEffect(function () {
    if (year) {
      if (month ?? false) {
        const currDate = new Date();
        let start = 1;
        let end = 12;
        if (currDate.getFullYear() === year) {
          end = currDate.getMonth() + 1;
        }
        if (year === firstTime.getFullYear()) {
          start = firstTime.getMonth() + 1;
        }
        if (month < start) {
          setMonth(start)
          form.setFieldValue('month', start)
        }
        if (month > end) {
          setMonth(end)
          form.setFieldValue('month', end)
        }
      }
    }
  }, [year]);

  function getDataColum(col) {
    const dataCol = [];
    dataCSV.forEach((e, index) => {
      if (index !== 0) {
        dataCol.push(e[col]);
      }
    })
    // return dataCol;

    let missingPart = []
    if (month ?? false) {
      missingPart = Array.from({ length: getTotalDaysInMonth(year, month) - dataCol.length }, () => 0);
    }
    else {
      missingPart = Array.from({ length: 12 - dataCol.length + 1 }, () => 0);
    }
    return [...dataCol, ...missingPart];
  }
  function getFieldNM(row) {
    const field = [];
    row.forEach((element, index) => {
      if (index !== 0) {
        field.push(element);
      }
    });
    return field;
  }
  function getDataCombine(themes, dataCSV) {
    const FieldNM = getFieldNM(dataCSV[0]);
    return FieldNM.map((ele, index) => {
      return ({
        label: ele,
        data: getDataColum(index + 1),
        backgroundColor: themes[index],
        barThickness: barThickness
      })
    })
  }

  const getTotalDaysInMonth = (year, month) => {
    // Tạo một đối tượng Date đại diện cho ngày cuối cùng của tháng
    let lastDayOfMonth = new Date(year, month, 0);
    // Trả về ngày của tháng cuối cùng
    return lastDayOfMonth.getDate();
  }

  function getLabels(dataCSV) {
    const label = []
    dataCSV.forEach((element, index) => {
      if (index !== 0) {
        label.push(element[0]);
      }
    });
    let missingPart = []
    if (month ?? false) {
      missingPart = Array.from({ length: getTotalDaysInMonth(year, month) - label.length }, (_, index) => index + label.length + 1);
    }
    else {
      missingPart = Array.from({ length: 12 - label.length }, (_, index) => index + label.length + 1);
    }
    return [...label, ...missingPart];
    // return label;
  }

  function getChartData(themes, dataCSV) {
    if (!themes || !dataCSV || themes.length === 0 || dataCSV.length === 0) {
      return null;
    }
    return ({
      labels: getLabels(dataCSV),
      datasets: getDataCombine(themes, dataCSV)
    });
  };

  const getSumData = () => {
    return dataCSV?.slice(1).reduce((acc, innerArray) => {
        innerArray.forEach((num, index) => {
            acc[index] = (acc[index] || 0) + num;
        });
        return acc;
    }, [])?.slice(1);
  }

  const yearOptions = () => {
    let start = firstTime.getFullYear();
    let end = (new Date()).getFullYear();
    return Array.from({length: end - start + 1 }, (_, index) => index + start)
      .map((ele) => {
        return {
          value: ele,
          label: ele,
          }
      })
  }
 
  const monthOptions = () => {
    const currDate = new Date();
    let start = 1;
    let end = 12;
    if (currDate.getFullYear() === year) {
      end = currDate.getMonth() + 1;
    }
    if (year === firstTime.getFullYear()) {
      start = firstTime.getMonth() + 1;
    }
    let result =  Array.from({length: end - start + 1 }, (_, index) => index + start)
      .map((ele) => {
        return {
          value: ele,
          label: ele,
          }
      })
    result.unshift({
      value: '',
      label: "Tất cả",
    })
    return result;
  }

  function findValueDataMax() {
    let arrSum = dataCSV?.slice(1).map((ele, index) => {
      let sum = ele?.slice(1).reduce((accumulator, currentValue) => {
          return accumulator + currentValue
      }, 0);
      return sum;
    })
    if (arrSum?.length >= 0) return Math.max(...arrSum);
    else return 0;
  }

  const handleChangeYear = (value) => {
    setYear(value);
  }
  const handleChangeMonth = (value) => {
    setMonth(value);
  }

  const options = {
    plugins: {
      title: {
        display: true,
        text: 'Bảng thống kê tình trạng cây trồng',
      },
      // datalabels: {
      //   display: true,
      //   align: 'end', // Điều chỉnh vị trí hiển thị của đơn vị
      //   // anchor: 'end', // Điều chỉnh vị trí neo của đơn vị
      //   formatter: function(value, context) {
      //     return value; // Format lại nếu cần
      //   },
      // },
    },
    responsive: true,
    maintainAspectRatio: false, // Cho phép phóng to/kéo nhỏ
    layout: {
      padding: {
        top: 20, // Phần đệm phía trên để tránh chạm vào tiêu đề hoặc chú thích
      },
    },
    scales: {
      x: {
        stacked: true,
        beginAtZero: true, // Bắt đầu từ 0
        title: {
          display: true,
          text: (month ?? false) ? "Ngày" : "Tháng",
          font: {
            weight: 'bold', // Đặt font in đậm cho tiêu đề
          },
        },
      },
      y: {
        stacked: true,
        beginAtZero: true, // Bắt đầu từ 0
        suggestedMax: findValueDataMax() * 1.2, // Giá trị tối đa của trục y
        ticks: {
          stepSize: 1, // Đây là cấu hình để đặt độ chia nhỏ nhất là 1
        },
        title: {
          display: true,
          text: "Cây",
          font: {
            weight: 'bold', // Đặt font in đậm cho tiêu đề
          },
        },
      },
    },
  };
  return (
    <>
    <div className="status-statistic-container">
        <div className="filter-chart">
          <Form
          size="middle"
          className="chart-mode"
          form={form}
          colon={false}
          layout="vertical"
        >
          <Row gutter={[20, 0]} style={{ margin: "0 15px" }}>
            <Col xs={6} sm={6} md={6} lg={6} xl={6} style={{ paddingLeft: "0" }}>
              <CustomStyleFormItem
                name="year"
                label={<SpanLabel>Năm:</SpanLabel>}
                initialValue={year}
                rules={[
                  {
                    required: true,
                    pattern: /\S/,
                    message: "This field is required",
                  },
                ]}
              >
                <Select
                style={{ width: "100%" }}
                onChange={handleChangeYear}
                  //defaultValue={[]}
                  options={
                    yearOptions()
                  }
                />
              </CustomStyleFormItem>
            </Col>
            <Col xs={6} sm={6} md={6} lg={6} xl={6} style={{ paddingLeft: "0" }}>
              <CustomStyleFormItem
                name="month"
                label={<SpanLabel>Tháng:</SpanLabel>}
                initialValue={month}
              >
                <Select
                  style={{ width: "100%" }}
                  onChange={handleChangeMonth}
                  //defaultValue={[]}
                  options={
                    monthOptions()
                  }
                />
              </CustomStyleFormItem>
            </Col>
          </Row>
        </Form>
        </div>
        <CustomSpin spinning={isLoadingData} style={{ zIndex: '5000' }}>
          <div className="main-chart">
              {data ? <Bar options={options} data={data} /> : <span>Không có dữ liệu để thống kê</span>}
          </div>
        </CustomSpin>
      </div>
    </>
  );
}



