import React from "react";
import "./adminStyle.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faEye,
  faPenToSquare,
  faTrash,
} from "@fortawesome/free-solid-svg-icons";

export default function TableDasboard(props) {
  function renderRow(rows, colNames) {
    if (rows && colNames) {
      return rows.map((row, i) => {
        return (
          <tr key={`header-data-${i}`} >
            {colNames.map((col, i) => {
              return <td>{row[col]}</td>;
            })}
            <td>
              <button type="button" className="btn btn-info">
                <FontAwesomeIcon icon={faEye} />
              </button>
              <button type="button" className="btn btn-primary">
                <FontAwesomeIcon icon={faPenToSquare} />
              </button>
              <button type="button" className="btn btn-danger">
                <FontAwesomeIcon icon={faTrash} />
              </button>
            </td>
          </tr>
        );
      });
    } else return <>no data</>;
  }

  function renderHeaderRow(colNames) {
    if (colNames) {
      return colNames.map((col, index) => {
        return (
          <th key={`row-data-${index}`} scope="col">
            {col}
          </th>
        );
      });
    } else return <>no colums</>;
  }

  return (
    <div>
      <table className="table table-striped table-hover">
        <thead>
          <tr>
            {renderHeaderRow(props.cols)}
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>{renderRow(props.data, props.cols)}</tbody>
      </table>
    </div>
  );
}
