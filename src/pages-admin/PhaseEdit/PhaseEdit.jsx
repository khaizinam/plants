import "./PhaseEdit.scss";
import { useRef, useState, useCallback } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowUpFromBracket, faMinus, faXmark } from "@fortawesome/free-solid-svg-icons";
import plantService from "../../services/plantService";
import { useDispatch } from "react-redux";
import { UpdateStages } from "../../redux/plant";
import { toast } from "react-toastify";
import { Button, FlexboxGrid, Input, InputGroup, InputNumber, Modal, Tooltip, Whisper } from "rsuite";
import InputGroupAddon from "rsuite/esm/InputGroup/InputGroupAddon";

const PhaseEdit = ({ phase, open, onClose }) => {
  const [isChanged, setIsChanged] = useState(false);
  const [stage, setStage] = useState({ ...phase });
  const [loading, setLoading] = useState(false);
  const [errors, setErrors] = useState({});
  const dispatch = useDispatch();
  const imgRef = useRef();

  const imageSelect = useCallback((e) => {
    const image = e.target.files && e.target.files[0] ? e.target.files[0] : null;
    if (image) handleChange("new_img", image);
  }, []);

  const handleChange = useCallback((tag, value) => {
    setIsChanged(true);
    setStage((prev) => ({ ...prev, [tag]: value }));
  }, []);

  const validate = useCallback(() => {
    const newErrors = {};
    if (!stage.name) newErrors.name = "Tên không được để trống";
    if (!stage.description) newErrors.description = "Mô tả không được để trống";
    setErrors(newErrors);
    return Object.keys(newErrors).length === 0;
  }, [stage]);

  const handleSubmit = useCallback(async (e) => {
    e.preventDefault();
    if (loading || !validate()) return;
    setLoading(true);
    try {
      const formRequest = new FormData();
      Object.keys(stage).forEach((key) => {
        if (stage[key] != null) formRequest.append(key, stage[key]);
      });

      const { data = [] } = await plantService.updateStage(phase.id, formRequest);
      dispatch(UpdateStages(data));
      toast.info("Đã sửa");
      onClose();
    } catch (error) {
      if (error?.response?.status !== 409) {
        toast.error("Lỗi hệ thống!", { theme: "colored" });
      } else {
        setErrors((prev) => ({ ...prev, name: "Tên giai đoạn đã tồn tại" }));
      }
    } finally {
      setLoading(false);
    }
  }, [dispatch, loading, onClose, phase.id, stage, validate]);

  return (
    <Modal open={open} onClose={onClose} className="phase-edit">
      <div className="phase-modal-box">
        <input type="file" hidden ref={imgRef} onChange={imageSelect} accept="image/*" />
        <div className={`phase-image-container ${stage.img_url ? "b" : ""}`}>
          {stage.img_url || stage.new_img ? (
            <div className="phase-image">
              <img
                onClick={() => imgRef.current.click()}
                src={stage.new_img ? URL.createObjectURL(stage.new_img) : stage.img_url}
                alt="Hình ảnh"
              />
              <div className="remove-btn" onClick={() => { handleChange("img_url", null); handleChange("new_img", null); }}>
                <FontAwesomeIcon icon={faMinus} />
              </div>
            </div>
          ) : (
            <div className="upload-container" onClick={() => imgRef.current.click()}>
              <FontAwesomeIcon icon={faArrowUpFromBracket} size="3x" className="upload-icon" />
              <span>Tải ảnh lên</span>
            </div>
          )}
        </div>
        <div className="phase-info-container">
          <div className="phase-modal-header">
            <div className="phase-modal-title">CHỈNH SỬA</div>
            <div className="close-btn" onClick={onClose}>
              <FontAwesomeIcon icon={faXmark} />
            </div>
          </div>
          <div className="phase-modal-body">
            <FlexboxGrid className="phase-item" align="middle">
              <FlexboxGrid.Item colspan={8} className="title">Tên:</FlexboxGrid.Item>
              <FlexboxGrid.Item colspan={16}>
                <Whisper placement="auto" trigger="focus" speaker={<Tooltip>Vui lòng nhập tên giai đoạn</Tooltip>}>
                  <Input
                    placeholder="..."
                    value={stage.name}
                    onChange={(value) => handleChange("name", value)}
                    className={errors.name ? "input-error" : ""}
                  />
                </Whisper>
                {errors.name && <div className="error-message">{errors.name}</div>}
              </FlexboxGrid.Item>
            </FlexboxGrid>
            <FlexboxGrid className="phase-item" align="middle">
              <FlexboxGrid.Item colspan={8} className="title">Thời gian:</FlexboxGrid.Item>
              <FlexboxGrid.Item colspan={16}>
                <Whisper trigger="focus" speaker={<Tooltip>Kéo dài bao nhiêu ngày?</Tooltip>}>
                  <InputNumber
                    min={0}
                    value={stage.range}
                    onChange={(value) => handleChange("range", value)}
                    formatter={(value) => (value ? `${value} ngày` : "...")}
                  />
                </Whisper>
              </FlexboxGrid.Item>
            </FlexboxGrid>
            <FlexboxGrid className="phase-item" align="middle">
              <FlexboxGrid.Item colspan={8} className="title">Độ ẩm đất:</FlexboxGrid.Item>
              <FlexboxGrid.Item colspan={16}>
                <Whisper trigger="focus" speaker={<Tooltip>Bắt buộc</Tooltip>}>
                  <InputGroup>
                    <InputGroupAddon>Từ</InputGroupAddon>
                    <InputNumber
                      formatter={(value) => `${value} %`}
                      min={0}
                      max={100}
                      value={stage.humidity_below}
                      onChange={(value) => {
                        if (value > stage.humidity_above) {
                          setStage((prev) => ({ ...prev, humidity_below: value, humidity_above: value }));
                        } else {
                          handleChange("humidity_below", value);
                        }
                      }}
                    />
                    <InputGroupAddon>tới</InputGroupAddon>
                    <InputNumber
                      formatter={(value) => `${value} %`}
                      min={0}
                      max={100}
                      value={stage.humidity_above}
                      onChange={(value) => {
                        if (value < stage.humidity_below) {
                          setStage((prev) => ({ ...prev, humidity_below: value, humidity_above: value }));
                        } else {
                          handleChange("humidity_above", value);
                        }
                      }}
                    />
                  </InputGroup>
                </Whisper>
              </FlexboxGrid.Item>
            </FlexboxGrid>
            <FlexboxGrid className="phase-item" align="middle">
              <FlexboxGrid.Item colspan={24} className="title" style={{ marginBottom: 10 }}>Mô tả:</FlexboxGrid.Item>
              <FlexboxGrid.Item colspan={24}>
                <Whisper trigger="focus" speaker={<Tooltip>Mô tả của giai đoạn</Tooltip>}>
                  <Input
                    as="textarea"
                    placeholder="Chưa có mô tả"
                    rows={3}
                    value={stage.description}
                    onChange={(value) => handleChange("description", value)}
                    className={errors.description ? "input-error" : ""}
                  />
                </Whisper>
                {errors.description && <div className="error-message">{errors.description}</div>}
              </FlexboxGrid.Item>
            </FlexboxGrid>
          </div>
          <div className="phase-modal-footer">
            <FlexboxGrid justify="end">
              <FlexboxGrid.Item>
                <Button loading={loading} className="submit-btn" onClick={handleSubmit} disabled={!isChanged}>
                  Cập nhật
                </Button>
              </FlexboxGrid.Item>
            </FlexboxGrid>
          </div>
        </div>
      </div>
    </Modal>
  );
};

export default PhaseEdit;