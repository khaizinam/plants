import React, { useEffect } from 'react';
import "./adminStyle.scss";
import { Route, Routes, useLocation } from "react-router-dom";

import PlantDetail from "./PlantDetail"
import PlantList from "../components/PlantList/PlantList"
import { useSelector } from 'react-redux';

export default function LibraryDashboard() {
    const location = useLocation();
    const plant = useSelector((state) => state.Plant);
    useEffect(() => {
        document.title = "Thư viện cây";
        const el = document.getElementById("bread-2");
        if (el) {
            el.innerHTML = plant.name;
        }
    }, [location, plant.name]);
    return (
        <div className="library">
            <Routes>
                <Route path="/:id/*" element={<PlantDetail />} />
                <Route path="/" element={<PlantList />} />
            </Routes>
        </div>
    )
}

