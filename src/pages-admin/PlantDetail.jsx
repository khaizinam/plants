/* eslint-disable react-hooks/exhaustive-deps */
import React from "react";
import { useEffect, useState } from "react";

import { NavLink, Route, Routes, useParams } from "react-router-dom";
import { Spin } from "antd";

import PostPlant from "../components/PostPlant/PostPlant";
import plantService from "../services/plantService";
import { useDispatch, useSelector } from "react-redux";
import { GetPlant, UpdateStages } from "../redux/plant";
import PlantStageAdmin from "./PlantStageAdmin";
import CancelIcon from "../icons/CancelIcon";
import { toast } from "react-toastify";
import ButtonLoading from "../components/ButtonLoadind/ButtonLoading";
import { List } from "rsuite";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronCircleLeft } from "@fortawesome/free-solid-svg-icons";

const PhaseReference = ({ stages }) => {
  const dispatch = useDispatch();
  const { id } = useParams();
  const handleSortEnd = async ({ oldIndex, newIndex }) => {
    try {
      if (oldIndex !== newIndex) {
        const { data } = await plantService.changeOrderStage(
          id,
          oldIndex,
          newIndex
        );
        dispatch(UpdateStages(data));
      }
    } catch (error) {
      console.log(error);
    }
  };
  return (
    <div className="reference">
      <div className="reference-title">Các giai đoạn</div>
      <List
        sortable
        className="reference-list"
        style={{ boxShadow: "unset" }}
        onSort={handleSortEnd}
      >
        {stages.map((e) => {
          return (
            <List.Item
              key={`item-${e.id}`}
              index={e.no}
              style={{
                boxShadow: "0 1px 0 var(--rs-list-border)",
                padding: "0 0 10",
              }}
            >
              <a href={`#title-${e.id}`}>
                {e.no}. {e.name}
              </a>
            </List.Item>
          );
        })}
      </List>
    </div>
  );
};

export default function PlantDetail() {
  const { user } = useSelector((state) => state.Auth);
  const isAdmin = user?.role === "ADMIN";
  const { id } = useParams();
  const dispatch = useDispatch();
  const [isLoading, setIsLoading] = useState(false);
  const [modalFeedback, setModalFeedback] = useState(false);
  const [feedBackVal, setFeedBackVal] = useState("");
  const plant = useSelector((state) => state.Plant);

  const [showReference, setShowReference] = useState(false);

  useEffect(() => {
    fetchData();
  }, [id]);

  const fetchData = async () => {
    setIsLoading(true);
    try {
      const { data } = await plantService.getPlant(id);
      data && dispatch(GetPlant(data));
      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    window.document.title = plant.name;
  }, [plant]);

  const { stages = [], posts = [] } = plant;

  function closeModal() {
    setFeedBackVal();
    setIsLoading(false);
    setModalFeedback(false);
  }

  async function sendFeedBack() {
    try {
      setIsLoading(true);
      await plantService.sendFeedBack({ plant_id: id, content: feedBackVal });
      // const feedback_return = { ...res.data, author: { id: user.id, full_name: user.full_name } }
      // await dispatch(UpdateFeedBack(feedback_return));
      closeModal();
      toast.success("You have send feed back!", { theme: "colored" });
    } catch (error) {
      toast.error("Send feed back fail!", { theme: "colored" });
      setIsLoading(false);
    }
  }
  return (
    <>
      <Spin
        spinning={isLoading}
        fullscreen={isLoading}
        style={{
          zIndex: "5000",
          height: "100%",
          flexGrow: "1",
          overflowY: "scroll",
        }}
      ></Spin>
      <div className="plant-page">
        <div className="plant-info">
          <div className="plant-info-header">
            <div className="plant-header-group">
              <div className="group-left">
                <div className="plant-navbar">
                  <NavLink to={"."} end className="plant-nav">
                    {({ isActive }) => {
                      return (
                        <div
                          className={`plant-nav-item ${
                            isActive ? `active` : ``
                          }`}
                        >
                          <div>Thông tin</div>
                        </div>
                      );
                    }}
                  </NavLink>
                  <NavLink to={"./phase"} className="plant-nav">
                    {({ isActive }) => {
                      return (
                        <div
                          className={`plant-nav-item ${
                            isActive ? `active` : ``
                          }`}
                        >
                          <div>Giai đoạn</div>
                        </div>
                      );
                    }}
                  </NavLink>
                </div>
              </div>
              {!isAdmin ? <div className="group-right">
                <button
                  className="btn-feedback"
                  onClick={() => setModalFeedback(true)}
                >
                  Phản hồi
                </button>
              </div> : <></>}
            </div>
          </div>
          <div className="plant-info-content">
            <Routes>
              <Route
                path="/"
                element={<PostPlant data={plant} fetchData={fetchData} />}
              />
              <Route path="/phase" element={<PlantStageAdmin />} />
            </Routes>
          </div>
        </div>

        <div className={`plant-reference ${showReference ? "show" : ""}`}>
          <Routes>
            <Route path="/phase" element={<PhaseReference stages={stages} />} />
            <Route
              path="/"
              element={
                <div className="reference">
                  <div className="reference-title">Mục lục</div>
                  <ul className="reference-list">
                    <li className="reference-item">
                      <a href={`#gioi_thieu`}>Giới thiệu</a>
                    </li>
                    {posts.map((e) => {
                      return (
                        <li className="reference-item" key={`item-${e.id}`}>
                          <a href={`#title-${e.id}`}>{e.title}</a>
                        </li>
                      );
                    })}
                  </ul>
                </div>
              }
            />
          </Routes>
          <div
            className={`show-reference-btn ${showReference ? "show" : ""}`}
            onClick={() => setShowReference((p) => !p)}
          >
            <FontAwesomeIcon
              icon={faChevronCircleLeft}
              size="xl"
              className="icon"
            />
          </div>
        </div>

        <div
          className={`bg-prevent-click ${modalFeedback ? "" : "d-hidden"}`}
          onClick={() => closeModal()}
        ></div>
        <div
          className={`modal-index modal-feed-back ${
            modalFeedback ? "" : "d-hidden"
          }`}
        >
          <div className="modal-header">
            <div className="flex-10"></div>
            <div>Phản hồi</div>
            <div>
              <button className="btn-cancel" onClick={() => closeModal()}>
                <CancelIcon />
              </button>
            </div>
          </div>
          <div className="modal-title">
            <div className="row-plant-info">
              <div className="title">Loài</div>
              <div className="plant-info-row-feed-back">
                <div className="plant-avatar">
                  <img
                    src={plant.avatar ? plant.avatar : "/image/post_2.png"}
                    alt="img-plant"
                  />
                </div>
                <div className="plant-name">{plant.name}</div>
              </div>
            </div>
            <div className="row-content">
              <div className="title">Mô tả</div>
              <div className="plant-input-row-feed-back">
                <textarea
                  spellCheck="false"
                  name="feed-back"
                  id="feed-back"
                  value={feedBackVal}
                  onChange={(event) => {
                    setFeedBackVal(event.target.value);
                  }}
                ></textarea>
              </div>
            </div>
          </div>
          <div className="modal-custom-footer">
            <ButtonLoading
              isLoading={isLoading}
              click={sendFeedBack}
              title={"Gửi"}
            />
          </div>
        </div>
      </div>
    </>
  );
}
