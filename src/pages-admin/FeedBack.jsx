/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import React, { useEffect, useState } from "react";
import { NavLink } from "react-router-dom";
import "./adminStyle.scss";
import { toast } from "react-toastify";
import plantService from "../services/plantService";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faEye,
  faPenToSquare,
  faSort,
  faSortDown,
  faSortUp,
  faTrash,
  faStar,
} from "@fortawesome/free-solid-svg-icons";
import { Table } from "rsuite";
import { faStar as re_faStar } from "@fortawesome/free-regular-svg-icons";
import ModalConfirm from "../components/ModalConfirm/ModalConfirm";
import Loading from "../components/Loading/Loading";
import Pagination from "../components/pagination/Pagination";
import moment from "moment";

export default function FeedBack() {
  const cols = [
    { slug: "id", title: "Mã số", sort: "id" },
    { slug: "plant_name", title: "Loài", sort: "plant_id" },
    { slug: "author_name", title: "Người gửi", sort: "author_id" },
    { slug: "is_seen", title: "Tình trạng", sort: "is_seen" },
    { slug: "created_at", title: "Ngày gửi", sort: "created_at" },
  ];

  const [loading, setIsLoading] = useState(false);
  const [modalHide, setModalHide] = useState(true);
  const [rowSelect, setRowSelect] = useState({});
  const [rowView, setRowView] = useState(-1);
  const [data, setData] = useState([]);
  const [searchValue, setSearchValue] = useState("");
  const [modalSearch, setModalSearch] = useState({
    queries: {},
    q: "",
    page: 1,
    order: [["created_at", "DESC"]],
  });
  const [pagination, setPagination] = useState({ page: 1, total_page: 1 });
  let isLoading = false;
  const updatePlant = async () => {
    try {
      isLoading = true;
      const res = await plantService.getFeedBack(modalSearch);
      setData(res.data.items);
      isLoading = false;
      setPagination({ page: res.data.page, total_page: res.data.total_page });
      setRowSelect(-1);
    } catch (error) {
      toast.error("Hệ thống bị lỗi!");
      isLoading = false;
    }
  };

  function getSortStatus(key) {
    let icon = faSort;
    modalSearch.order.forEach((e) => {
      if (e[0] === key) icon = e[1] === "DESC" ? faSortDown : faSortUp;
    });
    return icon;
  }

  useEffect(() => {
    updatePlant();
  }, [modalSearch]);

  function clickRow(row_index) {
    if (rowView !== row_index) setRowView(row_index);
    else {
      setRowView(-1);
    }
  }

  function renderColTitle(row, colNames) {
    const cols = colNames.map((col, col_index) => {
      let column;
      const val = row[col["slug"]];
      if (col["slug"] === "is_seen") {
        column = (
          <td key={`col-data-${col_index}`}>
            <div
              className={`is-seen ${val === "Đang chờ" ? "waitting" : "seen"}`}
            >
              {val}
            </div>
          </td>
        );
      } else if (col["slug"] === "plant_name") {
        column = (
          <td key={`col-data-${col_index}`}>
            <NavLink
              to={`../library/${row["plant_id"]}`}
              key={`col-data-${col_index}`}
            >
              <span style={{ color: "green" }}>{val}</span>
            </NavLink>
          </td>
        );
      } else if (col["slug"] === "created_at") {
        column = (
          <td key={`col-data-${col_index}`}>
            {moment(val).add(7, "hours").format("DD/MM/YYYY , HH:mm")}
          </td>
        );
      } else {
        column = <td key={`col-data-${col_index}`}>{val}</td>;
      }
      return column;
    });
    return cols;
  }

  async function checkIsSeen(feedback) {
    setRowSelect(feedback);
    setModalHide(false);
  }

  const changeSort = async (col) => {
    let oderCurrent = modalSearch.order[0];
    setRowSelect(-1);
    if (oderCurrent[0] === col["sort"]) {
      if (oderCurrent[1] || oderCurrent[1] === "DESC") {
        setModalSearch((prev) => ({ ...prev, order: [[oderCurrent[0]]] }));
      } else {
        setModalSearch((prev) => ({
          ...prev,
          order: [[oderCurrent[0], "DESC"]],
        }));
      }
    } else {
      setModalSearch((prev) => ({ ...prev, order: [[col["sort"]]] }));
    }
    //await updatePlant();
  };

  function renderContentRow(row, row_index) {
    return (
      <tr
        key={`row-data-content-${row_index}`}
        className={`row-data-detail ${rowView === row_index ? "" : "d-hidden"}`}
      >
        <td colSpan="6" style={{ background: "white" }}>
          <div className="border-bottom d-flex">
            {row.is_seen === "Đang chờ" ? (
              <button onClick={() => checkIsSeen(row)} className="btn">
                <FontAwesomeIcon icon={re_faStar} />
              </button>
            ) : (
              <button className="btn">
                <FontAwesomeIcon icon={faStar} />
              </button>
            )}
            <div className="text-pd-10 time-seen">
              {row.time_seen === null
                ? "Đánh dấu đã đọc!"
                : `Đã xem lúc: ${row.time_seen}`}
            </div>
          </div>
          <div className="text-pd-10">
            Nội dung: <br /> {row.content}
          </div>
        </td>
      </tr>
    );
  }

  function renderRow(rows, colNames) {
    if (rows && colNames) {
      return rows.map((row, row_index) => {
        const el = [
          <tr
            className={`cursor-pointer ${
              rowView === row_index ? "active-row" : ""
            }`}
            key={`row-view-${row_index}`}
            onClick={() => clickRow(row_index)}
          >
            {renderColTitle(row, colNames)}
          </tr>,
        ];
        el.push(renderContentRow(row, row_index));
        return el;
      });
    } else
      return (
        <tr>
          <Loading />
        </tr>
      );
  }

  function renderHeaderRow(colNames) {
    if (colNames) {
      return colNames.map((col, index) => {
        return (
          <th key={`header-title-${index}`} scope="col">
            <div className="table-head-title">
              {col.title}
              <button
                className="table-head-sort-btn"
                onClick={() => {
                  changeSort(col);
                }}
              >
                <FontAwesomeIcon icon={getSortStatus(col["sort"])} />
              </button>
            </div>
          </th>
        );
      });
    } else return <th>no columns</th>;
  }
  async function onConfirm() {
    try {
      const feedback = rowSelect;
      setIsLoading(true);
      const res = await plantService.seenFeedback(feedback.id);
      setData((prev) => {
        return prev.map((item) => {
          if (item.id === feedback.id) {
            return {
              ...item,
              is_seen: "Đã xem",
              time_seen: res.data.time_seen,
            };
          }
          return item;
        });
      });
      setIsLoading(false);
      setModalHide(true);
      toast.info("Cập nhật thành công!", { theme: "colored" });
    } catch (error) {
      toast.error("Server error!", { theme: "colored" });
    }
  }

  //   const onConfirm = async () => {
  //     setIsLoading(true);
  //     try {
  //       const feedback = rowSelect;

  //       const { data } = await plantService.seenFeedback(feedback.id);
  //       console.log(data);
  //     } catch (error) {
  //       console.log(error);
  //       toast.error("Server error!", { theme: "colored" });
  //     }
  //     setIsLoading(false);
  //   };
  function onCancel() {
    setRowSelect({});
    setIsLoading(false);
    setModalHide(true);
  }

  function filterByStatus(event) {
    if (event.target.value === "1") {
      setModalSearch((prev) => ({
        ...prev,
        queries: { ...prev.queries, is_seen: true },
      }));
    } else if (event.target.value === "0") {
      setModalSearch((prev) => ({
        ...prev,
        queries: { ...prev.queries, is_seen: false },
      }));
    } else {
      const query = modalSearch.queries;
      delete query["is_seen"];
      setModalSearch((prev) => ({ ...prev, queries: query }));
    }
  }

  function onInputKeySearch(value) {
    setSearchValue(value);
  }

  function sendSearch() {
    setModalSearch((prev) => ({ ...prev, q: searchValue, page: 1 }));
  }
  function changePage(index) {
    setModalSearch((prev) => ({ ...prev, page: index }));
  }

  return (
    <div className="container feed-back-page">
      <div className="row-jc-btw" style={{ marginTop: 20 }}>
        <div className="filter-group d-flex">
          <div className="filter-title">Trạng thái :</div>
          <div className="filter-form">
            <select
              className="form-select select-filter-is-seen"
              aria-label="Default select example"
              onChange={(event) => filterByStatus(event)}
            >
              <option value="">Tât cả</option>
              <option value="1">Đã xem</option>
              <option value="0">Đang chờ</option>
            </select>
          </div>
        </div>
        <div className="input-group w-250px">
          <input
            style={{ border: "1px solid" }}
            type="text"
            className="form-control"
            placeholder="Nhập mã phản hồi"
            aria-label="Username"
            value={searchValue}
            onChange={(event) => {
              onInputKeySearch(event.target.value);
            }}
          />
          <button
            className="btn btn-outline-secondary"
            onClick={() => sendSearch()}
          >
            Tìm
          </button>
        </div>
      </div>

      <br />
      <Pagination pagination={pagination} onChangePage={changePage} />
      <table className="table table-hover">
        <thead>
          <tr>{renderHeaderRow(cols)}</tr>
        </thead>
        <tbody>{renderRow(data, cols)}</tbody>
      </table>
      <div className={`bg-prevent-click ${!modalHide ? "" : "d-hidden"}`}></div>
      <ModalConfirm
        isLoading={loading}
        message={"Xác nhận hành động!"}
        hidden={modalHide}
        onConfirm={onConfirm}
        onCancel={onCancel}
      />
    </div>
  );
}
