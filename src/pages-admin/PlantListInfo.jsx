/* eslint-disable react-hooks/exhaustive-deps */
import React from "react";
// import { useSelector } from "react-redux";
import { NavLink, useLocation } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMagnifyingGlass } from "@fortawesome/free-solid-svg-icons";
import { Pagination } from "antd";
import { toast } from "react-toastify";

import AllPlantsIcon from "../icons/AllPlantsIcon";
import plantService from "../services/plantService";
import DeleteIcon from "../icons/DeletePostIcon";
import PlusIcon from "../icons/PlusIcon";

import ModalConfirm from "../components/ModalConfirm/ModalConfirm";
import ModalCreatePlant from "../components/PlantList/child/ModalCreatePlant";
import parse from "html-react-parser";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";

const limit = 10;

export default function PlantListInfo() {
  const location = useLocation();

  const searchParams = new URLSearchParams(location.search);
  const categoryQuery = searchParams.get("type") ?? "";
  const searchTermQuery = searchParams.get("searchTerm") ?? "";

  const { user } = useSelector((state) => state.Auth);
  const isEditable = user?.role === "ADMIN";

  const [categories, setCategories] = useState([]);
  const [plants, setPlants] = useState([]);
  const [category, setCategory] = useState("");
  const [searchTerm, setSearchTerm] = useState(searchTermQuery);
  const [totalPlants, setTotalPlants] = useState(0);
  const [currentPage, setCurrentPage] = useState(1);
  const [currPlantId, setCurrPlantId] = useState(0);
  const [isLoadingCreate, setIsLoadingCreate] = useState(false);
  const [isLoadingDelete, setIsLoadingDelete] = useState(false);
  const [isOpenCreateModal, setIsOpenCreateModal] = useState(false);
  const [isOpenDeleteModal, setIsOpenDeleteModal] = useState(false);

  useEffect(() => {
    getListCategory();
    setCategory(categoryQuery);
    setSearchTerm(searchTermQuery);
  }, []);

  useEffect(() => {
    getListPlants(currentPage, category, searchTermQuery);
  }, [category, searchTermQuery, currentPage]);

  useEffect(() => {
    const currCategory = categories.includes(categoryQuery)
      ? categoryQuery
      : "";
    setCategory(currCategory);
    setSearchTerm(searchTermQuery);
    setCurrentPage(1);
  }, [categoryQuery, searchTermQuery]);

  useEffect(() => {
    const currCategory = categories.includes(categoryQuery)
      ? categoryQuery
      : "";
    setCategory(currCategory);
  }, [categories]);

  useEffect(() => {
    if (currentPage > Math.ceil(totalPlants / limit))
      setCurrentPage(Math.ceil(totalPlants / limit));
  }, [totalPlants]);

  const getListCategory = async () => {
    try {
      const { data } = await plantService.getCategoryList();
      setCategories(
        data.categoryList.map((ele) => {
          return ele.category;
        })
      );
    } catch (error) {
      setCategories([]);
    }
  };

  const getListPlants = async (currPage = 1, currCategory, currSearchTerm) => {
    const queries = {
      type: currCategory,
      offset: currPage === 0 ? 0 : (currPage - 1) * limit,
      limit: limit,
      searchTerm: currSearchTerm,
    };
    try {
      const { data } = await plantService.getPlants(queries);
      setPlants(data?.plants);
      setTotalPlants(data?.total);
    } catch (error) {
      setPlants([]);
      setTotalPlants(0);
    }
  };

  const onChange = (pageNumber) => {
    setCurrentPage(pageNumber);
  };

  const handleDelete = async () => {
    try {
      setIsLoadingDelete(true);
      await plantService.deletePlant(currPlantId);
      setIsLoadingDelete(false);
      setIsOpenDeleteModal(false);
      getListPlants(currentPage, category, searchTermQuery);
      getListCategory();
      toast.success("Đã xóa", { autoClose: 1500 });
    } catch (error) {
      toast.error("Có lỗi xảy ra!", { autoClose: 1500 });
    }
  };

  const closeConfirm = () => {
    setIsOpenDeleteModal(false);
  };

  const OpenModalDelete = (id) => {
    setIsOpenDeleteModal(true);
    setCurrPlantId(id);
  };

  const handleCreatePlant = async (data) => {
    try {
      setIsLoadingCreate(true);
      await plantService.createPlant({
        name: data.name,
        category: data.category,
        avatar: data.avatar,
        description: data.description,
      });
      setIsLoadingCreate(false);
      toast.success("update success!", { theme: "colored" });
      setIsOpenCreateModal(false);
      getListPlants(currentPage, category, searchTermQuery);
      getListCategory();
      return true;
    } catch (error) {
      setIsLoadingCreate(false);
      toast.error("update fail!", { theme: "colored" });
      return false;
    }
  };

  return (
    <>
      <div className="plant-list">
        <div className="plant-info">
          {
            <div className="row-info">
              {
                <div className="header-title">
                  <h1>{category ? category : "Tất cả"}</h1>
                </div>
              }
              <div className="post-list">
                {plants?.length > 0 ? (
                  plants.map((post, subIndex) => {
                    return (
                      <NavLink to={`./${post.id}`} key={subIndex}>
                        <div className="post-card">
                          <div className="thumpnail">
                            <img
                              src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR5RP1-zKM_bN91w84Eui8TkbydjnjVUHLdEQ&usqp=CAU"
                              alt=""
                            />
                          </div>
                          <div className="content">
                            <div className="content-head">
                              <div className="name-plant">{post?.name}</div>
                              <span
                                hidden={!isEditable}
                                onClick={(event) => {
                                  event.preventDefault();
                                  OpenModalDelete(post?.id);
                                }}
                              >
                                <></>
                                <DeleteIcon />
                              </span>
                            </div>
                            <div className="description">
                              {parse(post?.description)}
                            </div>
                          </div>
                        </div>
                      </NavLink>
                    );
                  })
                ) : (
                  <></>
                )}
              </div>
            </div>
          }
          {parseFloat(totalPlants / limit) > 1 && (
            <Pagination
              defaultCurrent={1}
              //current={currentPage}
              total={totalPlants}
              pageSize={limit}
              showSizeChanger={false}
              onChange={onChange}
            />
          )}
          <button
            className="btn-add-plant"
            hidden={!isEditable}
            onClick={() => {
              setIsOpenCreateModal(true);
            }}
          >
            <PlusIcon />
          </button>
          <ModalConfirm
            isLoading={isLoadingDelete}
            message={"Bạn chắc chắn muốn xoá!"}
            hidden={!isOpenDeleteModal}
            onConfirm={handleDelete}
            onCancel={closeConfirm}
          />
        </div>
        <div className="plant-reference">
          <div className="search-bar">
            <div className="icon-and-input">
              <span>
                <FontAwesomeIcon icon={faMagnifyingGlass} />
              </span>
              <input
                type="text"
                onChange={(e) => setSearchTerm(e?.target?.value)}
                value={searchTerm}
                onFocus={(event) => {
                  event.target.select();
                }}
              />
            </div>
            <div className="search-button">
              <NavLink
                to={{
                  pathname: "./",
                  search: `?type=${category}&searchTerm=${searchTerm}`,
                }}
              >
                Tìm
              </NavLink>
            </div>
          </div>
          <div className="search-result-title">Kết quả tìm kiếm</div>
          <div className="filter-title">Bộ lọc</div>
          <div className="filter-container">
            <NavLink
              to={{
                pathname: "./",
                search: searchTermQuery ? `?searchTerm=${searchTermQuery}` : "",
              }}
            >
              <div className="filter-all">
                <div className="icon-container">
                  <AllPlantsIcon />
                </div>
                <span className="title">Tất cả</span>
              </div>
            </NavLink>
            <div className="filter-type">
              <ul className="reference-list">
                {categories?.map((category, index) => {
                  return (
                    <li key={index} className="reference-item">
                      <NavLink
                        to={{
                          pathname: "./",
                          search: searchTermQuery
                            ? `?type=${category}&searchTerm=${searchTermQuery}`
                            : `?type=${category}`,
                        }}
                      >
                        {category}
                      </NavLink>
                    </li>
                  );
                })}
              </ul>
            </div>
          </div>
        </div>
      </div>
      <ModalCreatePlant
        isOpenCreateModal={isOpenCreateModal}
        setIsOpenCreateModal={setIsOpenCreateModal}
        isLoading={isLoadingCreate}
        categories={categories}
        handleCreatePlant={handleCreatePlant}
        toast={toast}
      />
    </>
  );
}
