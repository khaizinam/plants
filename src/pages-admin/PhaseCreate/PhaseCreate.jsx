import "./PhaseCreate.scss";
import { useRef, useState, useCallback, useMemo } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowUpFromBracket, faMinus, faXmark } from "@fortawesome/free-solid-svg-icons";
import plantService from "../../services/plantService";
import { useDispatch, useSelector } from "react-redux";
import { UpdateStages } from "../../redux/plant";
import { toast } from "react-toastify";
import { Button, FlexboxGrid, Input, InputGroup, InputNumber, Modal, Tooltip, Whisper } from "rsuite";
import InputGroupAddon from "rsuite/esm/InputGroup/InputGroupAddon";

const defaultForm = {
  name: "",
  range: 10,
  humidity_above: 100,
  humidity_below: 0,
  description: "",
  img_url: null,
};

const PhaseCreate = ({ open, onClose }) => {
  const imgRef = useRef();
  const dispatch = useDispatch();
  const { id } = useSelector((state) => state.Plant);
  const [stage, setStage] = useState(defaultForm);
  const [loading, setLoading] = useState(false);
  const [errors, setErrors] = useState({ name: false, description: false, nameExist: false });

  const handleChange = useCallback((tag, value) => {
    setStage((prev) => ({ ...prev, [tag]: value }));
    setErrors((prev) => ({ ...prev, [tag]: false, nameExist: false })); // Clear errors on input change
  }, []);

  const imageSelect = useCallback((e) => {
    const image = e.target.files && e.target.files[0] ? e.target.files[0] : null;
    if (image) {
      handleChange("img_url", image);
    }
  }, [handleChange]);

  const handleSubmit = useCallback(async (e) => {
    e.preventDefault();
    if (loading) return;
    setLoading(true);

    const newErrors = {
      name: !stage.name,
      description: !stage.description,
      nameExist: false,
    };
    setErrors(newErrors);

    if (newErrors.name || newErrors.description) {
      setLoading(false);
      return;
    }

    try {
      const formRequest = new FormData();
      Object.keys(stage).forEach((key) => formRequest.append(key, stage[key]));

      const { data = [] } = await plantService.createStage(id, formRequest);

      dispatch(UpdateStages(data));
      toast.info("Đã thêm giai đoạn");
      onClose && onClose();
      setStage(defaultForm);
    } catch (error) {
      if (error?.response?.status !== 409) {
        toast.error("Lỗi hệ thống!", { theme: "colored" });
      } else {
        setErrors((prev) => ({ ...prev, nameExist: true }));
      }
    } finally {
      setLoading(false);
    }
  }, [dispatch, id, loading, onClose, stage]);

  const { name, range, humidity_above, humidity_below, description, img_url } = stage;

  const PhaseImage = useMemo(() => (
    <div className={`phase-image-container ${img_url ? "b" : ""}`}>
      {img_url ? (
        <div className="phase-image">
          <img
            onClick={() => imgRef.current.click()}
            src={URL.createObjectURL(img_url)}
            alt=""
          />
          <div className="remove-btn" onClick={() => handleChange("img_url", null)}>
            <FontAwesomeIcon icon={faMinus} />
          </div>
        </div>
      ) : (
        <div className="upload-container">
          <div className="upload-image" onClick={() => imgRef.current.click()}>
            <FontAwesomeIcon icon={faArrowUpFromBracket} size="3x" className="upload-icon" />
            <span>Tải ảnh lên</span>
          </div>
        </div>
      )}
    </div>
  ), [img_url, handleChange]);

  return (
    <Modal open={open} onClose={onClose} className="phase-create">
      <div className="phase-modal-box">
        <input type="file" hidden ref={imgRef} onChange={imageSelect} accept="image/*" />
        {PhaseImage}
        <div className="phase-info-container">
          <div className="phase-modal-header">
            <div className="phase-modal-title">THÊM GIAI ĐOẠN</div>
            <div className="close-btn" onClick={onClose}>
              <FontAwesomeIcon icon={faXmark} />
            </div>
          </div>
          <div className="phase-modal-body">
            <FlexboxGrid className="phase-item" align="middle">
              <FlexboxGrid.Item colspan={8} className="title">Tên:</FlexboxGrid.Item>
              <FlexboxGrid.Item colspan={16}>
                <Whisper trigger="focus" speaker={<Tooltip>Vui lòng nhập tên giai đoạn</Tooltip>}>
                  <Input
                    placeholder="..."
                    value={name}
                    onChange={(v) => handleChange("name", v)}
                    className={errors.name || errors.nameExist ? "input-error" : ""}
                  />
                </Whisper>
                {errors.name && <div className="error-message">Tên không được để trống</div>}
                {errors.nameExist && <div className="error-message">Tên giai đoạn đã tồn tại</div>}
              </FlexboxGrid.Item>
            </FlexboxGrid>
            <FlexboxGrid className="phase-item" align="middle">
              <FlexboxGrid.Item colspan={8} className="title">Thời gian:</FlexboxGrid.Item>
              <FlexboxGrid.Item colspan={16}>
                <Whisper trigger="focus" speaker={<Tooltip>Kéo dài bao nhiêu ngày?</Tooltip>}>
                  <InputNumber
                    min={0}
                    value={range}
                    onChange={(v) => handleChange("range", v)}
                    formatter={(value) => (value ? `${value} ngày` : "...")}
                  />
                </Whisper>
              </FlexboxGrid.Item>
            </FlexboxGrid>
            <FlexboxGrid className="phase-item" align="middle">
              <FlexboxGrid.Item colspan={8} className="title">Độ ẩm đất:</FlexboxGrid.Item>
              <FlexboxGrid.Item colspan={16}>
                <Whisper trigger="focus" speaker={<Tooltip>Bắt buộc</Tooltip>}>
                  <InputGroup>
                    <InputGroupAddon>Từ</InputGroupAddon>
                    <InputNumber
                      formatter={(value) => `${value} %`}
                      min={0}
                      max={100}
                      value={humidity_below}
                      onChange={(v) => handleChange("humidity_below", v)}
                    />
                    <InputGroupAddon>tới</InputGroupAddon>
                    <InputNumber
                      formatter={(value) => `${value} %`}
                      min={0}
                      max={100}
                      value={humidity_above}
                      onChange={(v) => handleChange("humidity_above", v)}
                    />
                  </InputGroup>
                </Whisper>
              </FlexboxGrid.Item>
            </FlexboxGrid>
            <FlexboxGrid className="phase-item" align="middle">
              <FlexboxGrid.Item colspan={24} className="title" style={{ marginBottom: 10 }}>
                Mô tả:
              </FlexboxGrid.Item>
              <FlexboxGrid.Item colspan={24}>
                <Whisper trigger="focus" speaker={<Tooltip>Mô tả của giai đoạn</Tooltip>} placement="auto">
                  <Input
                    as="textarea"
                    placeholder="Chưa có mô tả"
                    rows={3}
                    value={description}
                    onChange={(v) => handleChange("description", v)}
                    className={errors.description ? "input-error" : ""}
                  />
                </Whisper>
                {errors.description && <div className="error-message">Mô tả không được để trống</div>}
              </FlexboxGrid.Item>
            </FlexboxGrid>
          </div>
          <div className="phase-modal-footer">
            <FlexboxGrid justify="end">
              <FlexboxGrid.Item>
                <Button loading={loading} className="submit-btn" onClick={handleSubmit}>
                  Thêm
                </Button>
              </FlexboxGrid.Item>
            </FlexboxGrid>
          </div>
        </div>
      </div>
    </Modal>
  );
};

export default PhaseCreate;


// import "./PhaseCreate.scss";

// import { useRef, useState } from "react";
// import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
// import {
//   faArrowUpFromBracket,
//   faMinus,
//   faXmark,
// } from "@fortawesome/free-solid-svg-icons";

// import plantService from "../../services/plantService";
// import { useDispatch, useSelector } from "react-redux";
// import { UpdateStages } from "../../redux/plant";

// import { toast } from "react-toastify";
// import {
//   Button,
//   FlexboxGrid,
//   Input,
//   InputGroup,
//   InputNumber,
//   Modal,
//   Tooltip,
//   Whisper,
// } from "rsuite";
// import InputGroupAddon from "rsuite/esm/InputGroup/InputGroupAddon";

// const defaultForm = {
//   name: "",
//   range: 10,
//   humidity_above: 100,
//   humidity_below: 0,
//   description: "",
//   img_url: null,
// };

// const PhaseCreate = ({ open, onClose }) => {
//   const imgRef = useRef();
//   const dispatch = useDispatch();

//   const { id } = useSelector((state) => state.Plant);
//   const [stage, setStage] = useState(defaultForm);

//   const [loading, setLoading] = useState(false);

//   const handleChange = (tag, v) => {
//     setStage((prev) => ({
//       ...prev,
//       [tag]: v,
//     }));
//   };

//   const imageSelect = (e) => {
//     const image =
//       e.target.files && e.target.files[0] ? e.target.files[0] : null;
//     if (image) {
//       handleChange("img_url", image);
//     }
//   };

//   const handleSubmit = async (e) => {
//     e.preventDefault();
//     setLoading(true);
//     if (loading) return;
//     try {
//       const formRequest = new FormData();
//       for (let key in stage) {
//         formRequest.append(key, stage[key]);
//       }

//       const { data = [] } = await plantService.createStage(id, formRequest);
//       dispatch(UpdateStages(data));
//       toast.info("Đã thêm giai đoạn");
//       onClose && onClose();
//       setStage(defaultForm);
//     } catch (error) {
//       console.log(error);
//     }
//     setLoading(false);
//   };

//   const { name, range, humidity_above, humidity_below, description, img_url } =
//     stage;

//   return (
//     <Modal open={open} onClose={onClose} className="phase-create">
//       <div className="phase-modal-box">
//         <input
//           type="file"
//           hidden
//           ref={imgRef}
//           onChange={imageSelect}
//           accept="image/*"
//         />
//         <div className={`phase-image-container ${img_url ? "b" : ""}`}>
//           {img_url ? (
//             <div className="phase-image">
//               <img
//                 onClick={() => imgRef.current.click()}
//                 src={
//                   img_url
//                     ? URL.createObjectURL(img_url)
//                     : "https://images.unsplash.com/reserve/bOvf94dPRxWu0u3QsPjF_tree.jpg?ixid=M3wxMjA3fDB8MXxzZWFyY2h8NHx8bmF0dXJhbHxlbnwwfHx8fDE3MDgzODI4NDZ8MA&ixlib=rb-4.0.3"
//                 }
//                 alt=""
//               />
//               <div
//                 className="remove-btn"
//                 onClick={() => handleChange("img_url", null)}
//               >
//                 <FontAwesomeIcon icon={faMinus} />
//               </div>
//             </div>
//           ) : (
//             <div className="upload-container">
//               <div
//                 className="upload-image"
//                 onClick={() => imgRef.current.click()}
//               >
//                 <FontAwesomeIcon
//                   icon={faArrowUpFromBracket}
//                   size="3x"
//                   className="upload-icon"
//                 />
//                 <span>Tải ảnh lên</span>
//               </div>
//             </div>
//           )}
//         </div>
//         <div className="phase-info-container">
//           <div className="phase-modal-header">
//             <div className="phase-modal-title">THÊM GIAI ĐOẠN</div>
//             <div className="close-btn" onClick={onClose}>
//               <FontAwesomeIcon icon={faXmark} />
//             </div>
//           </div>
//           <div className="phase-modal-body">
//             <FlexboxGrid className="phase-item" align="middle">
//               <FlexboxGrid.Item colspan={8} className="title">
//                 Tên:
//               </FlexboxGrid.Item>
//               <FlexboxGrid.Item colspan={16}>
//                 <Whisper
//                   trigger="focus"
//                   speaker={<Tooltip>Vui lòng nhập tên giai đoạn</Tooltip>}
//                 >
//                   <Input
//                     placeholder="..."
//                     value={name}
//                     onChange={(v) => handleChange("name", v)}
//                   />
//                 </Whisper>
//               </FlexboxGrid.Item>
//             </FlexboxGrid>
//             <FlexboxGrid className="phase-item" align="middle">
//               <FlexboxGrid.Item colspan={8} className="title">
//                 Thời gian:
//               </FlexboxGrid.Item>
//               <FlexboxGrid.Item colspan={16}>
//                 <Whisper
//                   trigger="focus"
//                   speaker={<Tooltip>Kéo dài bao nhiêu ngày?</Tooltip>}
//                 >
//                   <InputNumber
//                     min={0}
//                     value={range}
//                     onChange={(v) => handleChange("range", v)}
//                     formatter={(value) => (value ? `${value} ngày` : "...")}
//                   />
//                 </Whisper>
//               </FlexboxGrid.Item>
//             </FlexboxGrid>
//             <FlexboxGrid className="phase-item" align="middle">
//               <FlexboxGrid.Item colspan={8} className="title">
//                 Độ ẩm đất:
//               </FlexboxGrid.Item>
//               <FlexboxGrid.Item colspan={16}>
//                 <Whisper trigger="focus" speaker={<Tooltip>Bắt buộc</Tooltip>}>
//                   <InputGroup>
//                     <InputGroupAddon>Từ</InputGroupAddon>
//                     <InputNumber
//                       formatter={(value) => `${value} %`}
//                       min={0}
//                       max={100}
//                       value={humidity_below}
//                       onChange={(v) => handleChange("humidity_below", v)}
//                     />
//                     <InputGroupAddon>tới</InputGroupAddon>
//                     <InputNumber
//                       formatter={(value) => `${value} %`}
//                       min={0}
//                       max={100}
//                       value={humidity_above}
//                       onChange={(v) => handleChange("humidity_above", v)}
//                     />
//                   </InputGroup>
//                 </Whisper>
//               </FlexboxGrid.Item>
//             </FlexboxGrid>

//             <FlexboxGrid className="phase-item" align="middle">
//               <FlexboxGrid.Item
//                 colspan={24}
//                 className="title"
//                 style={{ marginBottom: 10 }}
//               >
//                 Mô tả:
//               </FlexboxGrid.Item>
//               <FlexboxGrid.Item colspan={24}>
//                 <Whisper
//                   trigger="focus"
//                   speaker={<Tooltip>Mô tả của giai đoạn</Tooltip>}
//                   placement="auto"
//                 >
//                   <Input
//                     as="textarea"
//                     placeholder="Chưa có mô tả"
//                     rows={3}
//                     value={description}
//                     onChange={(v) => handleChange("description", v)}
//                   />
//                 </Whisper>
//               </FlexboxGrid.Item>
//             </FlexboxGrid>
//           </div>

//           <div className="phase-modal-footer">
//             <FlexboxGrid justify="end">
//               <FlexboxGrid.Item>
//                 <Button
//                   loading={loading}
//                   className="submit-btn"
//                   onClick={handleSubmit}
//                 >
//                   Thêm
//                 </Button>
//               </FlexboxGrid.Item>
//             </FlexboxGrid>
//           </div>
//         </div>
//       </div>
//     </Modal>
//   );
// };

// export default PhaseCreate;
