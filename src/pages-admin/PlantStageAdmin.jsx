import { useDispatch, useSelector } from "react-redux";
import { useState } from "react";

import { toast } from "react-toastify";
import DeletePhaseIcon from "../icons/DeletePhaseIcon";
import PlusIcon from "../icons/PlusIcon";
import PhaseModal from "./PhaseEdit/PhaseEdit";
import PhaseCreate from "./PhaseCreate/PhaseCreate";
import ModalConfirm from "../components/ModalConfirm/ModalConfirm";
import plantService from "../services/plantService";
import { UpdateStages } from "../redux/plant";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPencil } from "@fortawesome/free-solid-svg-icons";
import parse from "html-react-parser";

const PhaseItem = ({
  id,
  name,
  no,
  humidity_below,
  humidity_above,
  range,
  description,
  plant_id,
  img_url,
}) => {
  const [edit, setEdit] = useState(false);
  const openEdit = () => setEdit(true);
  const closeEdit = () => setEdit(false);

  const [remove, setRemove] = useState(false);
  const openRemove = () => setRemove(true);
  const closeRemove = () => setRemove(false);

  const dispatch = useDispatch();

  const handleDelete = async () => {
    try {
      const { data = [] } = await plantService.deleteStage(id);
      await dispatch(UpdateStages(data));
      toast.info("Đã xóa");
    } catch (error) {
      console.log(error);
    }
  };
  return (
    <div id={`title-${id}`} className={`phase-card ${id ?? "hidden"}`}>
      <div className="img-container">
        <img
          src={
            img_url ??
            "https://aehinnovativehydrogel.com/wp-content/uploads/2022/07/04June22_what-are-the-requirements-for-plant-growth_iStock-956366756-1024x730.jpg"
          }
          alt=""
        />
        <div className="phase-no">{no}</div>
      </div>
      <div className="info-container">
        <div className="phase-header">
          <div className="phase-name">{name}</div>
          <div className="actions">
            <div className="update action" onClick={openEdit}>
              <FontAwesomeIcon icon={faPencil} />
            </div>
            <div className="remove action" onClick={openRemove}>
              <DeletePhaseIcon />
            </div>
          </div>
        </div>
        <div className="phase-range">
          <div className="title">Thời gian:</div>
          <div className="value">{range} ngày</div>
        </div>
        <div className="phase-humid">
          <div className="title">Độ ẩm đất:</div>
          <div className="value">
            {humidity_below}% - {humidity_above}%
          </div>
        </div>
        <hr />
        <div className="phase-description">{parse(description)}</div>
      </div>

      {edit && (
        <PhaseModal
          phase={{
            id,
            name,
            range,
            description,
            humidity_above,
            humidity_below,
            img_url,
            plant_id,
          }}
          open={edit}
          onClose={closeEdit}
        />
      )}

      <ModalConfirm
        onCancel={closeRemove}
        hidden={!remove}
        onConfirm={handleDelete}
        message={
          <>
            Bạn chắc chắn xóa giai đoạn <b>{name}</b>
          </>
        }
      />
    </div>
  );
};

const PlantStageAdmin = () => {
  const { stages = [] } = useSelector((state) => state.Plant);

  const [modal, setModal] = useState(false);

  return (
    <div className="phase-plant">
      <div className="phase-list">
        {stages.map((stage) => (
          <PhaseItem key={stage.id} {...stage} />
        ))}
      </div>

      <div
        className="add-stage-btn"
        title="Thêm giai đoạn"
        onClick={() => setModal(true)}
      >
        <PlusIcon />
      </div>

      <PhaseCreate open={modal} onClose={() => setModal(false)} />
    </div>
  );
};

export default PlantStageAdmin;
