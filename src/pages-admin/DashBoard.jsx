import React from "react";
import { Navigate } from "react-router-dom";

export default function DashBoard() {
  return <Navigate to={"/admin/statistic"} />;
}
