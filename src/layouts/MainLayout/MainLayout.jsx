/* eslint-disable react-hooks/exhaustive-deps */
import "./MainLayout.scss";

import { NavLink, Navigate, Route, Routes } from "react-router-dom";

import Sidebar from "../../components/Sidebar/Sidebar";
import Library from "../../pages/Library/Library";
import GardenPage from "../../pages/GardenPage/GardenPage";
import ReminderPage from "../../pages/ReminderPage/ReminderPage";
import Logo from "../../components/Logo/Logo";
import Breadcrumb from "../../components/Breadcrumb/Breadcrumb";

import { useEffect, useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBell } from "@fortawesome/free-regular-svg-icons";
import { faBars, faChevronCircleLeft } from "@fortawesome/free-solid-svg-icons";
import { useSelector } from "react-redux";
import Chart from "../../pages/Chart/Chart";
import ProfilePage from "../../pages/ProfilePage/ProfilePage";

const MainLayout = () => {
  const [show, setShow] = useState(false);
  const { token, user } = useSelector((state) => state.Auth);

  useEffect(() => {}, []);

  if (!token) return <Navigate to="/auth/login" />;
  if (user.role !== "USER") return <Navigate to="/admin" />;
  return (
    <div className="main-layout">
      <div className={`sidebar-container ${show ? "show" : ""}`}>
        <FontAwesomeIcon
          icon={faChevronCircleLeft}
          className="close-button"
          onClick={() => setShow(false)}
        />
        <Sidebar />
      </div>
      <div className="main-body">
        <div className="app-header">
          <div className="menu-button" onClick={() => setShow((prev) => !prev)}>
            <FontAwesomeIcon icon={faBars} />
          </div>
          <div className="breadcrumb-container">
            <Breadcrumb />
          </div>
          <div className="logo-mobile">
            <Logo />
          </div>
          <div className="header-button">
            <NavLink to={"/profile"} className="header-user">
              <div className="avatar">
                <img
                  src="https://www.shutterstock.com/image-vector/adorable-vector-illustration-cute-turtle-600nw-2351051927.jpg"
                  alt=""
                />
              </div>
              <div className="username">
                {user?.full_name ? user.full_name : ""}
              </div>
            </NavLink>
          </div>
        </div>
        <div className="main-content">
          <Routes>
            <Route path="/garden/*" element={<GardenPage />} />
            <Route path="/reminder/*" element={<ReminderPage />} />
            <Route path="/statistic/*" element={<Chart />} />
            <Route path="/library/*" element={<Library />} />
            <Route path="/profile/*" element={<ProfilePage />} />
          </Routes>
        </div>
      </div>
    </div>
  );
};

export default MainLayout;
