/* eslint-disable no-unused-vars */
import { Navigate, Route, Routes } from "react-router-dom";

import "./AuthLayout.scss";
// import { useSelector } from "react-redux";
import Login from "../../pages/auth/Login";
import Register from "../../pages/auth/Register";


const AuthLayout = () => {
  // const { token } = useSelector((state) => state.Auth);
  // const token = false;
  // if (token) return <Navigate to="/" />;
  // else
  return (
    <div className="auth-layout">
      <div className="auth-box">
        <Routes>
          <Route path="register" element={<Register />} />
          <Route path="*" element={<Login />} />
        </Routes>
      </div>
    </div>
  );
};

export default AuthLayout;
