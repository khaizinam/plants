/* eslint-disable react-hooks/exhaustive-deps */
import "./AdminLayout.scss";

import { Navigate, Route, Routes } from "react-router-dom";

import AdminSidebar from "../../components/Sidebar/AdminSidebar";
import Statistic from "../../pages-admin/Statistics/Statistics";
import DashBoard from "../../pages-admin/DashBoard";
import FeedBack from "../../pages-admin/FeedBack";
import { useSelector } from "react-redux";
import LibraryDashboard from "../../pages-admin/LibraryDashboard";
import Breadcrumb from "../../components/Breadcrumb/Breadcrumb";

const AdminLayout = () => {
  const { token, user } = useSelector((state) => state.Auth);

  if (!token) return <Navigate to="/auth/login" />;
  if (user.role !== "ADMIN") return <Navigate to="/" />;

  return (
    <div className="admin-main-layout">
      <div className="sidebar-container">
        <AdminSidebar />
      </div>
      <div className="main-body">
        <div className="app-header">
          <Breadcrumb />
        </div>
        <div className="main-content">
          <Routes>
            <Route path="/" element={<DashBoard />} />
            <Route path="/statistic/*" element={<Statistic />} />
            <Route path="/library/*" element={<LibraryDashboard />} />
            <Route path="/feedbacks/*" element={<FeedBack />} />
          </Routes>
        </div>
      </div>
    </div>
  );
};

export default AdminLayout;
