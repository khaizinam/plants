const { default: BaseService } = require("./baseService");
const { default: URL_CONST } = require("./URL_const");

class GardenService extends BaseService {
  getGarden() {
    return this.get(URL_CONST.SERVER_URL + URL_CONST.GARDEN.GET);
  }

  getAreas() {
    return this.get(URL_CONST.SERVER_URL + URL_CONST.GARDEN.AREAS);
  }

  addArea(area) {
    return this.post(`${URL_CONST.SERVER_URL}${URL_CONST.GARDEN.AREAS}`, area);
  }

  addTree(tree) {
    return this.post(`${URL_CONST.SERVER_URL}${URL_CONST.GARDEN.TREE}`, tree);
  }

  deleteTree(treeId) {
    return this.delete(
      `${URL_CONST.SERVER_URL}${URL_CONST.GARDEN.TREE}${treeId}`
    );
  }

  updateArea(area_id, data) {
    return this.put(
      `${URL_CONST.SERVER_URL}${URL_CONST.GARDEN.AREAS}${area_id}`,
      data
    );
  }

  deleteArea(areaId) {
    return this.delete(
      `${URL_CONST.SERVER_URL}${URL_CONST.GARDEN.AREAS}${areaId}`
    );
  }

  addGarden(garden) {
    return this.post(
      `${URL_CONST.SERVER_URL}${URL_CONST.GARDEN.AREAS}`,
      garden
    );
  }

  getTree(tree_id, sort = false) {
    return this.get(
      `${URL_CONST.SERVER_URL}${URL_CONST.GARDEN.GET_DIARY}${tree_id}`,
      {}
    );
  }

  createDiary(tree_id, data) {
    return this.post(
      `${URL_CONST.SERVER_URL}${URL_CONST.GARDEN.GET_DIARY}${tree_id}`,
      data
    );
  }

  updateDiary(diary_id, data) {
    return this.put(
      `${URL_CONST.SERVER_URL}${URL_CONST.GARDEN.UPDATE_DIARY}${diary_id}`,
      data
    );
  }

  updateTree(tree_id, data) {
    return this.put(
      `${URL_CONST.SERVER_URL}${URL_CONST.GARDEN.TREE}${tree_id}`,
      data
    );
  }

  deleteDiary(id) {
    return this.delete(
      `${URL_CONST.SERVER_URL}${URL_CONST.GARDEN.DELETE_DIARY}${id}`
    );
  }
}

const gardenService = new GardenService();

export default gardenService;
