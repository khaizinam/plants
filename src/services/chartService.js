const { default: BaseService } = require("./baseService");
const { default: URL_CONST } = require("./URL_const");

class ChartService extends BaseService {
  getTreeStatusData({ area_id, year, month }) {
    let queryStr = `?year=${year}`;
    if (area_id ?? false) {
      let areaQuery = area_id.reduce((acc, currentValue) => {
        if (currentValue >= 0) return acc + `&area_id=${currentValue}`;
        else return acc;
      }, "");
      if (area_id?.length > 0 && area_id[area_id.length - 1] === -1)
        areaQuery += `&other_area=true`;
      queryStr += areaQuery;
    }
    if (month ?? false) {
      queryStr += `&month=${month}`;
    }
    return this.get(
      URL_CONST.SERVER_URL + URL_CONST.CHART.GET_TREE_STATUS + queryStr
    );
  }
  getFirstDiaryTime(area_id) {
    let queryStr = area_id?.reduce((acc, currentValue, currentIndex) => {
      if (currentValue >= 0) {
        if (currentIndex === 0) return acc + `?area_id=${currentValue}`;
        else
          return acc.length === 0
            ? acc + `?area_id=${currentValue}`
            : acc + `&area_id=${currentValue}`;
      } else return acc;
    }, "");
    if (area_id?.length > 0 && area_id[area_id.length - 1] === -1)
      queryStr === ""
        ? (queryStr += `?other_area=true`)
        : (queryStr += `&other_area=true`);
    return this.get(
      URL_CONST.SERVER_URL +
        URL_CONST.CHART.GET_FIRST_DIARY_TIME +
        (area_id ?? false ? queryStr : ``)
    );
  }
  getNumberStatuses() {
    return this.get(URL_CONST.SERVER_URL + URL_CONST.CHART.GET_NUMBER_STATUS);
  }
  getChartPlantAdmin(plant_id) {
    return this.get(
      URL_CONST.SERVER_URL + URL_CONST.STATISTIC.GET_NUM_PLANT_ADMIN + plant_id
    );
  }
  getGeneralStatisticData() {
    return this.get(
      URL_CONST.SERVER_URL + URL_CONST.STATISTIC.GET_GENERAL_STATISTIC_DATA
    );
  }
  getData({ type = 0, areas = [], isNullArea = false }) {
    if (type === 0) {
      return {
        themes: ["#4caf50", "#ffb300", "#2196f3", "#f44336"],
        data: [
          ["labels", "Khoẻ mạnh", "Bị bệnh", "Thu hoạch", "Đã chết"],
          ["1", 20, 3, 4, 5],
          ["2", 20, 3, 4, 5],
          ["3", 20, 3, 4, 5],
          ["3", 20, 3, 4, 5],
          ["3", 20, 3, 4, 5],
          ["3", 20, 3, 4, 5],
          ["3", 20, 3, 4, 5],
          ["3", 20, 3, 4, 5],
          ["3", 20, 3, 4, 5],
          ["3", 20, 3, 4, 5],
          ["3", 20, 3, 4, 5],
          ["3", 20, 3, 4, 5],
          ["3", 20, 3, 4, 5],
          ["3", 20, 3, 4, 5],
          ["3", 20, 3, 4, 5],
          ["3", 20, 3, 4, 5],
          ["3", 20, 3, 4, 5],
          ["3", 20, 3, 4, 5],
          ["3", 20, 3, 4, 5],
          ["3", 20, 3, 4, 5],
          ["3", 20, 3, 4, 5],
        ],
      };
    } else {
      return {
        themes: ["#4caf50", "#f44336"],
        data: [
          ["labels", "Còn sống", "Đã chết"],
          ["1", 20, 3],
          ["2", 20, 30],
          ["3", 40, 5],
          ["4", 40, 5],
          ["5", 40, 5],
          ["6", 40, 5],
          ["7", 50, 5],
          ["8", 40, 5],
          ["9", 80, 5],
          ["10", 40, 5],
          ["11", 70, 5],
          ["12", 60, 5],
        ],
      };
    }
  }
}
// eslint-disable-next-line import/no-anonymous-default-export
export default new ChartService();
