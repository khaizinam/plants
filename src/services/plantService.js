// import axios from "axios";

const { default: BaseService } = require("./baseService");
const { default: URL_CONST } = require("./URL_const");

class PlantService extends BaseService {
  getPlant(plantId) {
    return this.get(URL_CONST.SERVER_URL + URL_CONST.PLANT.GET + `${plantId}`);
  }
  updateDescription(id, des) {
    return this.put(
      `${URL_CONST.SERVER_URL}${URL_CONST.PLANT.DES_UPDATE}${id}`,
      { description: des }
    );
  }
  updatePost(payload) {
    return this.put(
      `${URL_CONST.SERVER_URL}${URL_CONST.PLANT.POST_UPDATE}${payload.id}`,
      payload
    );
  }
  createPost(plantId, payload) {
    return this.post(
      `${URL_CONST.SERVER_URL}${URL_CONST.PLANT.POST_CREATE}${plantId}`,
      payload
    );
  }
  deletePost(id) {
    return this.delete(
      `${URL_CONST.SERVER_URL}${URL_CONST.PLANT.DELETE_POST}${id}`,
      {}
    );
  }
  getPlants(queries) {
    return this.get(
      URL_CONST.SERVER_URL +
        URL_CONST.PLANT.GET +
        `?type=${queries.type || ""}&offset=${queries.offset || ""}&limit=${
          queries.limit || ""
        }&searchTerm=${queries.searchTerm || ""}`
    );
  }
  getCategoryList() {
    return this.get(URL_CONST.SERVER_URL + URL_CONST.PLANT.GET_CATEGORY_LIST);
  }

  getStages(treeId) {
    return this.get(
      `${URL_CONST.SERVER_URL}${URL_CONST.GARDEN.STAGE}${treeId}`
    );
  }

  createStage(plantId, stage) {
    return this.post(
      `${URL_CONST.SERVER_URL}${URL_CONST.PLANT.STAGE}${plantId}`,
      stage
    );
  }
  updateStage(stageId, stage) {
    return this.put(
      `${URL_CONST.SERVER_URL + URL_CONST.PLANT.STAGE}${stageId}`,
      stage
    );
  }
  changeOrderStage(plantId, oldIndex, newIndex) {
    return this.put(
      `${URL_CONST.SERVER_URL + URL_CONST.PLANT.ORDER}${plantId}`,
      { oldIndex, newIndex }
    );
  }
  deleteStage(stageId) {
    return this.delete(
      `${URL_CONST.SERVER_URL}${URL_CONST.PLANT.STAGE}${stageId}`
    );
  }
  createPlant(payload) {
    return this.post(
      `${URL_CONST.SERVER_URL}${URL_CONST.PLANT.CREATE}`,
      payload
    );
  }

  updatePlant(id, payload) {
    return this.put(
      `${URL_CONST.SERVER_URL}${URL_CONST.PLANT.PUT}${id}`,
      payload
    );
  }

  deletePlant(id) {
    return this.delete(
      `${URL_CONST.SERVER_URL}${URL_CONST.PLANT.DELETE}${id}`,
      {}
    );
  }

  sendFeedBack(payload) {
    return this.post(
      `${URL_CONST.SERVER_URL}${URL_CONST.PLANT.SEND_FEED_BACK}`,
      payload
    );
  }

  getFeedBack(payload) {
    return this.post(
      `${URL_CONST.SERVER_URL}${URL_CONST.PLANT.GET_FEED_BACk}`,
      payload
    );
  }

  updateFeedBack(payload) {
    return this.put(
      `${URL_CONST.SERVER_URL}${URL_CONST.PLANT.SET_FEED_BACk}`,
      payload
    );
  }
  seenFeedback(id) {
    return this.put(
      `${URL_CONST.SERVER_URL}${URL_CONST.PLANT.SET_FEED_BACk}/${id}`,
      {}
    );
  }
  uploadImage(payload) {
    return this.post(
      `${URL_CONST.SERVER_URL}${URL_CONST.PLANT.UPLOAD_IMG}`,
      payload
    );
  }
}

// eslint-disable-next-line import/no-anonymous-default-export
export default new PlantService();
