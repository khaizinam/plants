const { default: BaseService } = require("./baseService");
const { default: URL_CONST } = require("./URL_const");

class StatisticService extends BaseService {
  getTreeStatusData({ year, month }) {
    let queryStr = `?year=${year}`;
    if (month ?? false) {
        queryStr += `&month=${month}`;
    }
    return this.get(URL_CONST.SERVER_URL + URL_CONST.STATISTIC.GET_TREE_STATUS + queryStr);
  }
  getFirstDiaryTime() {
    return this.get(URL_CONST.SERVER_URL + URL_CONST.STATISTIC.GET_FIRST_DIARY_TIME);
  }
  getNumberStatuses() {
    return this.get(URL_CONST.SERVER_URL + URL_CONST.STATISTIC.GET_NUMBER_STATUS);
  }
  getFirstUserRegisterTime() {
    return this.get(URL_CONST.SERVER_URL + URL_CONST.STATISTIC.GET_FIRST_USER_REGISTER_TIME);
  }
  getNewSubscriberStatisticalData(year) {
    return this.get(URL_CONST.SERVER_URL + URL_CONST.STATISTIC.GET_NEW_SUBSCRIBER_STATISTIC_DATA + `?year=${year}`);
  }
   getFirstTreeCreateTime() {
    return this.get(URL_CONST.SERVER_URL + URL_CONST.STATISTIC.GET_FIRST_TREE_CREATE_TIME);
  }
  getTreeStatisticalData(year) {
    return this.get(URL_CONST.SERVER_URL + URL_CONST.STATISTIC.GET_TREE_STATISTICAL_DATA + `?year=${year}`);
  }
}

export default new StatisticService();
