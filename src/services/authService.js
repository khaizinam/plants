const { default: BaseService } = require("./baseService");
const { default: URL_CONST } = require("./URL_const");

class AuthService extends BaseService {
  initPermission() {
    return this.get(URL_CONST.SERVER_URL + URL_CONST.AUTH.PERMISSION, {});
  }

  login(email, password) {
    return this.post(URL_CONST.SERVER_URL + URL_CONST.AUTH.LOGIN, {
      email: email,
      password: password,
    });
  }
  register(email, password, full_name) {
    return this.post(URL_CONST.SERVER_URL + URL_CONST.AUTH.SIGNUP, {
      email,
      password,
      full_name,
    });
  }
  logout() {
    return this.get(URL_CONST.SERVER_URL + URL_CONST.AUTH.LOGOUT, {});
    //this.clearCookie();
  }
  getCurrentUser() {
    return this.get(URL_CONST.SERVER_URL + URL_CONST.AUTH.CURRENT, {});
  }
  updateUser(data) {
    return this.put(URL_CONST.SERVER_URL + URL_CONST.AUTH.USER, data);
  }
  changePassword(oldPassword, newPassword) {
    return this.put(URL_CONST.SERVER_URL + URL_CONST.AUTH.PASSWORD, {
      oldPassword,
      newPassword,
    });
  }
}

// eslint-disable-next-line import/no-anonymous-default-export
export default new AuthService();
