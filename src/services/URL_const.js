//https://plants-api-x9hy.onrender.com/api
//http://localhost:8080/api
const URL_CONST = {
  //SERVER_URL: "https://plants-api-x9hy.onrender.com/api",
  SERVER_URL: process.env.REACT_APP_BE_URL,

  AUTH: {
    LOGIN: "/auth/login",
    SIGNUP: "/auth/sign-up",
    PERMISSION: "/auth/permission",
    CURRENT: "/auth/current",
    USER: "/auth/user",
    PASSWORD: "/auth/user/password",
  },
  PLANT: {
    GET: "/plant/",
    CREATE: "/plant/",
    PUT: "/plant/",
    DELETE: "/plant/",
    POST_UPDATE: "/plant/post/",
    POST_CREATE: "/plant/post/",
    DES_UPDATE: "/plant/description/",
    DELETE_POST: "/plant/post/",
    GET_CATEGORY_LIST: "/plant/category-list",
    SEND_FEED_BACK: "/plant/feed-back",
    GET_FEED_BACk: "/plant/feed-backs",
    SET_FEED_BACk: "/plant/feed-back",
    STAGE: "/plant/stage/",
    ORDER: "/plant/stage/arrange/",
    UPLOAD_IMG: "/uploads",
  },

  GARDEN: {
    GET: "/garden/",
    AREAS: "/garden/areas/",
    TREE: "/garden/tree/",
    GET_DIARY: "/garden/diary/",
    UPDATE_DIARY: "/garden/diary/",
    STAGE: "/garden/stages/",
    DELETE_DIARY: "/garden/diary/",
  },

  CHART: {
    GET_TREE_STATUS: "/chart/tree-status",
    GET_FIRST_DIARY_TIME: "/chart/firt-diary-time",
    GET_NUMBER_STATUS: "/chart/number-tree-status",
  },

  STATISTIC: {
    GET_TREE_STATUS: "/statistics/tree-status",
    GET_FIRST_DIARY_TIME: "/statistics/firt-diary-time",
    GET_NUMBER_STATUS: "/statistics/number-tree-status",
    GET_NUM_PLANT_ADMIN: "/statistics/admin-plants/",
    GET_FIRST_USER_REGISTER_TIME: "/statistics/get-first-user-register-time/",
    GET_NEW_SUBSCRIBER_STATISTIC_DATA:
      "/statistics/get-new-subscriber-statistical-data/",
    GET_GENERAL_STATISTIC_DATA: "/statistics/get-general-statistic-data/",
    GET_TREE_STATISTICAL_DATA: "/statistics/get-tree-statistical-data/",
    GET_FIRST_TREE_CREATE_TIME: "/statistics/get-first-tree-create-time",
  },

  REMINDER: {
    GET: "/reminder/",
    CREATE: "/reminder/",
    DELETE: "/reminder/",
    UPDATE: "/reminder/",
  },
};

export default URL_CONST;
