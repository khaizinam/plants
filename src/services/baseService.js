import axios from "axios";
export default class BaseService {
  constructor() {
    this.updateHeader();
  }
  updateHeader() {
    this.header = {
      Authorization: "Bearer " + this.getToken(),
      "Access-Control-Allow-Origin": "*",
      // "Content-type": "*",
      "Access-Control-Allow-Methods": "GET,PUT,POST,DELETE",
      // "Access-Control-Allow-Headers": "Content-Type",
      "Access-Control-Allow-Headers": "*",
      "Access-Control-Allow-Credentials": "true",
    };
  }

  localSet(key, val){
    localStorage.setItem(key, JSON.stringify(val));
  }

  localGet(key){
    return JSON.parse(localStorage.getItem(key)) || null;
  }

  setToken(token) {
    localStorage.setItem("token", token);
  }

  getToken() {
    return localStorage.getItem("token") || "";
  }

  updateSettingDiary(setting){
    localStorage.setItem("diary_setting", setting);
  }
  getSettingDiary(){
    return localStorage.getItem("diary_setting") === "false" ? false : true;
  }

  getCookie(cname) {
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(";");
    for (let i = 0; i < ca.length; i++) {
      let c = ca[i];
      while (c.charAt(0) === " ") {
        c = c.substring(1);
      }
      if (c.indexOf(name) === 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }
  clearCookie() {
    this.setCookie("jwt", "");
  }
  setCookie(cname, cvalue, exdays = 1) {
    const d = new Date();
    d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
    let expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  }
  post(url, payload) {
    this.updateHeader();
    return axios.post(url, payload, { headers: this.header });
  }
  get(url, header) {
    this.updateHeader();
    return axios.get(url, { headers: { ...header, ...this.header } });
  }
  put(url, payload) {
    this.updateHeader();
    return axios.put(url, payload, { headers: this.header });
  }
  delete(url, payload = {}) {
    this.updateHeader();
    return axios.delete(url, { headers: this.header });
  }
}
