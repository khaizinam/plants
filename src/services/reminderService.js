/* eslint-disable import/no-anonymous-default-export */
// import axios from "axios";

const { default: BaseService } = require("./baseService");
const { default: URL_CONST } = require("./URL_const");

class ReminderService extends BaseService {
  getReminder() {
    return this.get(URL_CONST.SERVER_URL + URL_CONST.REMINDER.GET);
  }
  createReminder(payload) {
    return this.post(
      `${URL_CONST.SERVER_URL}${URL_CONST.REMINDER.CREATE}`,
      payload
    );
  }
  deleteReminder(id) {
    return this.delete(
      `${URL_CONST.SERVER_URL}${URL_CONST.REMINDER.DELETE}${id}`,
      {}
    );
  }
  updateReminder(id, payload) {
    return this.put(
      `${URL_CONST.SERVER_URL}${URL_CONST.REMINDER.UPDATE}${id}`,
      payload
    );
  }
}

export default new ReminderService();
