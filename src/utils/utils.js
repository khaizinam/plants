export const normalizeString = (str) => {
  return str
    .toLowerCase()
    .trim()
    .normalize("NFD")
    .replace(/[\u0300-\u036f]/g, "");
};

export const URL_BREADCRUMB = {
  admin: "Trang chủ",
  feedbacks: "Phản hồi",
  statistic: "Thống kê",
  reminder: "Nhắc nhở",
  library: "Thư viện",
  phase: "Giai đoạn",
  garden: "Vườn của tôi",
  profile: "Trang cá nhân",
};

export const TREE_STATUS = ["Khoẻ mạnh", "Bi bệnh", "Thu hoạch", "Đã chết"];

export const ERROR_NAME = {
  SYSTEM_ERROR: "Hệ thống bị lỗi",
  DATA_IS_NOT_LOADED: "Không tải được dữ liệu",
};
