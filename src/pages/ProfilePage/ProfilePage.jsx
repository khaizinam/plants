/* eslint-disable react-hooks/exhaustive-deps */
import "./ProfilePage.scss";

import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import moment from "moment";
import { Button, Input, Modal } from "rsuite";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faXmark } from "@fortawesome/free-solid-svg-icons";
import authService from "../../services/authService";
import { UpdateUserAction } from "../../redux/auth";
import { toast } from "react-toastify";

const ModalUpdateInfo = ({ open, onClose, data }) => {
  const dispatch = useDispatch();

  const [form, setForm] = useState({ ...data });
  const [loading, setLoading] = useState(false);

  const [empty, setEmpty] = useState(false);

  const handleChange = (tag, v) => setForm((prev) => ({ ...prev, [tag]: v }));
  const { full_name, phone_number, location } = form;

  const handleSubmit = async () => {
    setLoading(true);
    try {
      const pack = {};
      pack.full_name = full_name.trim();
      pack.phone_number = phone_number.trim();
      pack.location = location.trim();
      if (pack.full_name) {
        const { data } = await authService.updateUser(pack);
        dispatch(UpdateUserAction(data));
        onClose && onClose();
        toast.success(<>Đã cập nhật</>);
      } else {
        setEmpty(true);
      }
    } catch (error) {
      console.log(error);
    }
    setLoading(false);
  };

  return (
    <Modal open={open} onClose={onClose} className="modal-update-info">
      <div className="header-title">
        <span>CẬP NHẬT THÔNG TIN</span>
        <div className="close-btn" onClick={onClose}>
          <FontAwesomeIcon icon={faXmark} />
        </div>
      </div>
      <div className="body">
        <div className="line">
          <div className="title">Tên (*)</div>
          <div className="value">
            <Input
              value={full_name}
              onChange={(v) => {
                handleChange("full_name", v);
                setEmpty(false);
              }}
              style={
                empty
                  ? {
                      boxShadow: "0 0 2px 1px red",
                      border: "none",
                      outline: "none",
                    }
                  : {}
              }
            />
            {empty && (
              <code style={{ color: "red" }}>Tên không được trống</code>
            )}
          </div>
        </div>
        <div className="line">
          <div className="title">Số điện thoại</div>
          <div className="value">
            <Input
              value={phone_number}
              onChange={(v) => {
                let value;
                let matches = v.match(/(\d+)/);
                if (matches) {
                  value = matches[0];
                } else value = "";
                handleChange("phone_number", value);
              }}
            />
          </div>
        </div>
        <div className="line">
          <div className="title">Địa chỉ</div>
          <div className="value">
            <Input
              value={location}
              onChange={(v) => handleChange("location", v)}
            />
          </div>
        </div>
      </div>
      <div className="submit-area">
        <Button appearance="primary" onClick={handleSubmit} loading={loading}>
          Cập nhật
        </Button>
      </div>
    </Modal>
  );
};

const PasswordChangeComponent = () => {
  const [oldPassword, setOldPassword] = useState("");
  const [newPassword, setNewPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");

  const handleChangePassword = async () => {
    if (oldPassword && newPassword && newPassword === confirmPassword) {
      try {
        await authService.changePassword(oldPassword, newPassword);
        toast.success(<>Đã đổi mật khẩu</>);
        setOldPassword("");
        setNewPassword("");
        setConfirmPassword("");
      } catch (error) {
        console.log(error);
        const message =
          error.response.data.message || error.response.data || "Lỗi hệ thống";
        toast.error(<>{message}</>);
      }
    }
  };
  return (
    <>
      <div className="profile-header">Đổi mật khẩu</div>
      <div className="profile-body">
        <div className="line">
          <div className="title">Mật khẩu hiện tại</div>
          <div className="value">
            <Input
              type={"password"}
              placeholder="..."
              value={oldPassword}
              onChange={(v) => setOldPassword(v)}
            />
          </div>
        </div>
        <div className="line">
          <div className="title">Mật khẩu mới</div>
          <div className="value">
            <Input
              type={"password"}
              value={newPassword}
              placeholder="..."
              onChange={(v) => setNewPassword(v)}
            />
          </div>
        </div>
        <div className="line">
          <div className="title">Nhập lại mật khẩu mới</div>
          <div className="value">
            <Input
              type={"password"}
              placeholder="..."
              value={confirmPassword}
              onChange={(v) => setConfirmPassword(v)}
              className={`${
                newPassword &&
                confirmPassword &&
                newPassword !== confirmPassword
                  ? "error"
                  : null
              } `}
            />
          </div>
        </div>
      </div>
      <div className="password-bottom">
        <Button
          color="green"
          appearance="primary"
          onClick={handleChangePassword}
          disabled={
            !oldPassword ||
            !newPassword ||
            !confirmPassword ||
            newPassword !== confirmPassword
          }
        >
          Đổi
        </Button>
      </div>
    </>
  );
};

const ProfilePage = () => {
  const { user } = useSelector((state) => state.Auth);

  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  useEffect(() => {
    const fetchData = async () => {
      try {
      } catch (error) {}
    };
    fetchData();
  }, []);

  useEffect(() => {
    window.document.title = "Trang cá nhân";
  }, []);

  const { full_name, location, email, create_at, phone_number, avatar } = user;

  return (
    <div className="profile-page">
      <div className="wr">
        <div className="profile-container">
          <div className="profile-left">
            <div className="avatar">
              <img
                src={
                  avatar ??
                  "https://www.shutterstock.com/image-vector/adorable-vector-illustration-cute-turtle-600nw-2351051927.jpg"
                }
                alt=""
              />
            </div>
            <div className="role">USER</div>
          </div>
          <div className="profile-right">
            <div className="container">
              <div className="profile-header">
                <span>Trang cá nhân</span>
                <Button appearance="link" color="green" onClick={handleOpen}>
                  Chỉnh sửa
                </Button>
              </div>
              <div className="profile-body">
                <div className="line">
                  <div className="title">Tên</div>
                  <div className="value">{full_name}</div>
                </div>
                <div className="line">
                  <div className="title">Email</div>
                  <div className="value">{email}</div>
                </div>
                <div className="line">
                  <div className="title">Số điện thoại</div>
                  <div className="value">{phone_number ?? "Chưa có"}</div>
                </div>
                <div className="line">
                  <div className="title">Địa chỉ</div>
                  <div className="value">{location ?? "Chưa có"}</div>
                </div>
                <div className="line">
                  <div className="title">Ngày tham gia:</div>
                  <div className="value">
                    {moment(create_at).format("DD-MM-YYYY")}
                  </div>
                </div>
              </div>
            </div>
            <div className="container">
              <PasswordChangeComponent />
            </div>
          </div>
        </div>
      </div>
      {open && (
        <ModalUpdateInfo
          open={open}
          onClose={handleClose}
          data={{ full_name, phone_number, location }}
        />
      )}
    </div>
  );
};

export default ProfilePage;
