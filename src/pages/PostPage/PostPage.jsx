import "./PostPage.scss";

import { NavLink } from "react-router-dom";
import { faEllipsis } from "@fortawesome/free-solid-svg-icons";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHeart as NotLike, faComment } from "@fortawesome/free-regular-svg-icons";
import { faHeart as Like } from "@fortawesome/free-solid-svg-icons";

import CommentInput from "../../components/CommentInput/CommentInput";
import Comment from "../../components/Comment/Comment";
import { useState } from "react";
import SlideImage from "../../components/SlideImage/SlideImage";

const PostPage = () => {
    const [liked, setLiked] = useState(false);

    const toggleLike = () => setLiked(prev => !prev)

    return (
        <div className="post-page">
            <div className="detail-image">
                <SlideImage />
            </div>
            <div className="detail-info">
                <div className="post-header">
                    <div className="post-info">
                        <div className="owner-avatar">
                            <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSaNJiTJT8snzOQGwoEPu9JSp9U0ytkzR7reg&usqp=CAU" alt="" />
                        </div>

                        <div className="post-detail">
                            <div className="post-owner">
                                <NavLink
                                    to={`/profile`}
                                    className="owner-username"
                                >
                                    Hồng Khánh
                                </NavLink>
                                <div className="post-date">
                                    5 phút trước
                                </div>
                            </div>
                            <div className="post-plant">
                                <div className="dot"></div>
                                Cây táo
                            </div>
                        </div>
                    </div>
                    <div className="post-setting">
                        <FontAwesomeIcon icon={faEllipsis} />
                    </div>
                </div>

                <div className="post-description">
                    Vườn của tôi là một trong những khu vườn mát mẻ nhất khu vực.
                </div>
                <div className="post-actions">
                    <div className="action">
                        <div className="action-icon">
                            <FontAwesomeIcon icon={liked ? Like : NotLike} onClick={toggleLike} />
                            <div className="tooltip">
                                {liked ? 'Bỏ thích' : 'Thích'}
                            </div>
                        </div>
                        <div className="action-number">
                            10.234 lượt thích
                        </div>
                    </div>
                    <div className="action">
                        <div className="action-icon">
                            <FontAwesomeIcon icon={faComment} />
                        </div>
                        <div className="action-number">
                            102 bình luận
                        </div>
                    </div>
                </div>

                <div className="post-comments-box">
                    <div>
                        <div className="comments-list">
                            <Comment />
                            <Comment />
                            <Comment />
                        </div>
                    </div>
                </div>



                <div className="post-comment-input">
                    <CommentInput />
                </div>
            </div>
        </div>
    );
}

export default PostPage;