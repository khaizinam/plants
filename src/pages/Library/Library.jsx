import "./Library.scss";

import { Route, Routes, useLocation } from "react-router-dom";
import PlantPage from "../PlantPage/PlantPage";
import PlantList from "../../components/PlantList/PlantList";
import { useEffect } from "react";
import { useSelector } from "react-redux";

const Library = () => {
  const location = useLocation();
  const plant = useSelector((state) => state.Plant);
  useEffect(() => {
    document.title = "Thư viện cây";
    const el = document.getElementById("bread-1");
    if (el) {
      el.innerHTML = plant.name;
    }
  }, [location, plant.name]);
  return (
    <div className="library">
      <Routes>
        <Route path="/:id/*" element={<PlantPage />} />
        <Route path="/" element={<PlantList />} />
      </Routes>
    </div>
  );
};

export default Library;
