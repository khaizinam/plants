import styled from 'styled-components'
import { Modal, Input, Form, Upload, Divider, Spin } from "antd";

export const SpanLabel = styled.span`
    
    text-align: center;
    font-size: 14px;
    font-style: normal;
    font-weight: 600;
    line-height: normal;
    display: flex;
    align-items: center;
    gap: 10px;
`

export const CustomStyleFormItem = styled(Form.Item)`
  //width: 100%;
  margin-bottom: 10px;
  & .ant-form-item-label {
    text-align: left;
  }
  & > .ant-form-item {
    marginbottom: 0px;
    justify-content: flex-start;
  }
  & .ant-form-item-label > label.ant-form-item-required:not(.ant-form-item-required-mark-optional)::before {
    display: none;
  }
  & .ant-form-item-control-input {
    min-height: 0;
  }
`

export const CustomSpin = styled(Spin)`
  & .ant-spin-nested-loading {
    display: flex;
    align-items: center;
    justify-content: center;
    height: 100vh; /* Đảm bảo chiều cao đầy đủ của màn hình */
  }
`