import "./GardenPage.scss";

import { useEffect } from "react";

import { Route, Routes } from "react-router-dom";
import Garden from "./child/Garden/Garden";
import TreeDiaryInfo from "./child/Diary/TreeDiaryInfo";

const GardenPage = () => {
  useEffect(() => {
    document.title = "Vườn của tôi";
  });
  return (
    <div className="garden-page">
      <Routes>
        <Route path="/" element={<Garden />} />
        <Route path="/:id" element={<TreeDiaryInfo />} />
      </Routes>
    </div>
  );
};

export default GardenPage;
