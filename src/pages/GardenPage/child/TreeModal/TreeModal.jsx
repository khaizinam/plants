import "./TreeModal.scss";

import { useEffect, useRef, useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faXmark } from "@fortawesome/free-solid-svg-icons";

import Loading from "../../../../components/Loading/Loading";
import { SelectPicker, DatePicker, Modal, Dropdown, Input } from "rsuite";

import plantService from "../../../../services/plantService";

import { useDispatch } from "react-redux";
import gardenService from "../../../../services/gardenService";
import { AddTree } from "../../../../redux/garden";
import { toast } from "react-toastify";

const TreeModal = ({ onClose, show }) => {
  const dispatch = useDispatch();
  const nameRef = useRef();

  const [tree, setTree] = useState({
    name: "",
    plant: null,
    area: null,
    description: "",
    date_created: null,
  });
  const [plants, setPlants] = useState([]);
  const [areas, setAreas] = useState([]);

  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const fetchPlants = async () => {
      try {
        const { data } = await plantService.getPlants({});
        setPlants(data.plants);
      } catch (error) {}
    };
    const fetchAreas = async () => {
      try {
        const { data } = await gardenService.getAreas();
        data.push({ id: null, name: "Khác" });
        setAreas(data);
      } catch (error) {}
    };
    fetchPlants();
    fetchAreas();
  }, [show]);

  const handleChange = (e) =>
    setTree((prev) => ({
      ...prev,
      [e.target.name]: e.target.value,
    }));

  const handleSubmit = async (e) => {
    e.preventDefault();
    setLoading(true);
    try {
      let { name, plant, area, description, date_created } = tree;
      name = name.trim();
      if (!name) {
        toast.error("Vui lòng nhập tên hợp lệ");
        nameRef.current.focus();
      } else if (!plant) {
        toast.error("Vui lòng chọn loài cây");
      } else {
        const { data } = await gardenService.addTree({
          name,
          plant,
          area,
          description,
          date_created,
        });
        dispatch(AddTree(data));
        onClose && onClose();
        setTree({
          name: "",
          plant: null,
          area: null,
          description: "",
        });
        toast.success("Đã thêm cây mới");
      }
    } catch (error) {
      console.log(error);
      toast.error(error.response.data);
    }
    setLoading(false);
  };

  return (
    <Modal open={show} onClose={onClose} className="tree-modal">
      <form className="tree-modal-box" onSubmit={handleSubmit}>
        <div className="tree-modal-header">
          Thêm cây
          <div className="close-btn" onClick={onClose}>
            <FontAwesomeIcon icon={faXmark} />
          </div>
        </div>
        <div className="tree-modal-body">
          <div className="tree-info-list">
            <div className="tree-info">
              <div className="title">Tên (*): </div>
              <div className="value">
                <Input
                  ref={nameRef}
                  type="text"
                  value={tree.name}
                  onChange={(v) => setTree({ ...tree, name: v })}
                  placeholder="Nhập tên cây"
                />
              </div>
            </div>
            <div className="tree-info">
              <div className="title">Loài (*): </div>
              <div className="value">
                <SelectPicker
                  data={plants.map((tree) => ({
                    label: tree.name,
                    value: tree.id,
                  }))}
                  renderMenuItem={(label, item) => (
                    <div style={{ fontSize: "14px" }}>{label}</div>
                  )}
                  placeholder="Chọn loài "
                  style={{ minWidth: 110 }}
                  onSelect={(value) => setTree({ ...tree, plant: value })}
                />
              </div>
            </div>
            <div className="tree-info">
              <div className="title">Khu vực (*): </div>
              <div className="value">
                <Dropdown
                  placement="bottomEnd"
                  title={areas.find((a) => a.id === tree.area)?.name}
                  onSelect={(v) => setTree({ ...tree, area: v })}
                >
                  {areas.map((area, index) => (
                    <Dropdown.Item
                      active={tree.area === area.id}
                      key={index}
                      eventKey={area.id}
                    >
                      {area.name}
                    </Dropdown.Item>
                  ))}
                </Dropdown>
              </div>
            </div>

            <div className="tree-info">
              <div className="title">Ngày trồng:</div>
              <div className="value">
                <DatePicker
                  oneTap
                  format="dd/MM/yyyy"
                  value={tree.date_created}
                  name="date_created"
                  onChange={(v) =>
                    setTree((prev) => ({
                      ...prev,
                      date_created: v,
                    }))
                  }
                  placeholder="Chọn ngày"
                />
              </div>
            </div>
            <div className="tree-info">
              <div className="title">Mô tả:</div>
            </div>
          </div>
          <textarea
            spellCheck="false"
            className="tree-info-description"
            rows={2}
            name="description"
            onChange={handleChange}
            placeholder="Nhập mô tả của cây ..."
            value={tree.description}
            style={{ fontSize: 14 }}
          ></textarea>
        </div>
        <div className="tree-modal-footer">
          <button className="submit-btn btn">
            {loading ? <Loading /> : "Thêm"}
          </button>
        </div>
      </form>
    </Modal>
  );
};

export default TreeModal;
