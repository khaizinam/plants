/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import React, { useEffect, useState } from "react";
import "./TreeDiaryInfo.scss";
import PlusIcon from "../../../../icons/PlusIcon";
import { useParams } from "react-router-dom";
import DiaryModalCreate from "./DiaryModalCreate";
import gardenService from "../../../../services/gardenService";

import BoxDetail from "./BoxDetail";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEye, faEyeSlash } from "@fortawesome/free-solid-svg-icons";
import { useDispatch, useSelector } from "react-redux";
import { SaveTree } from "../../../../redux/tree";
import { toast } from "react-toastify";
import Loading from "../../../../components/Loading/Loading";
import { TREE_STATUS } from "../../../../utils/utils";

const subTime = (strTime) => {
  return strTime.substring(0, strTime.length - 3);
};

export default function TreeDiaryInfo() {
  const { id } = useParams();
  const dispatch = useDispatch();
  const [isShowStage, setIsShowStage] = useState(
    gardenService.getSettingDiary()
  );
  const [loading, setLoading] = useState(false);
  const [addAction, setAddAction] = useState(false);
  const [loadDataSuccess, setLoadDataSuccess] = useState(false);
  const [noData, setNoData] = useState(false);
  const treeRedux = useSelector((state) => state.Tree);
  // SERVICES --------------
  useEffect(() => {
    async function apiGetTree() {
      setLoading(true);
      try {
        const res = await gardenService.getTree(id);
        if (res.data.tree !== null) {
          dispatch(SaveTree(res.data));
          setNoData(false);
        } else {
          toast.error("Cây không tồn tại!");
          setNoData(true);
        }
      } catch (error) {
        if (!error?.response) {
          toast.error("Lấy dữ liệu bị lỗi!");
        } else {
          toast.error("Lỗi từ hệ thống!");
        }
        setNoData(true);
      }
      setLoadDataSuccess(true);
      setLoading(false);
    }
    apiGetTree();
  }, []);

  // ACTION FUNC =================

  const { diaries, tree } = treeRedux;

  function changeSettingShowStage() {
    gardenService.updateSettingDiary(!isShowStage);

    setIsShowStage((prev) => {
      return !prev;
    });
  }

  function getLatestDiary() {
    if (diaries.length > 0) {
      const filtereDiaries = diaries.filter((d) => d.diary);
      if (filtereDiaries.length > 0) {
        return filtereDiaries[filtereDiaries.length - 1];
      } else {
        return null;
      }
    } else {
      return null;
    }
  }

  function getStatus(status_id) {
    return TREE_STATUS[status_id] || "";
  }
  function getCurrentTreeStatus() {
    const _tree = getLatestDiary();
    if (_tree === null) return -1;
    else {
      return _tree.diary.title;
    }
  }
  // MODAL FUNC ================

  const closeModal = () => {
    setAddAction(false);
  };

  return (
    <>
      {loadDataSuccess && !noData ? (
        <div className="wr-diary-page-responsive">
          <div className="wr-diary-page--tree-info">
            <div className="box-shadow-sm br-5 pd-10 bg-white">
              <div className="c-text-2xl text-center bold">THÔNG TIN</div>
              <div className="wr-container">
                <div className="row-flex row-jc-center">
                  <div className="sq-lg b-circle bg-green plant-image">
                    <img src={tree?.plant?.avatar} alt="" />
                  </div>
                </div>
                <div className="row-flex tree-info">
                  <div className="tree-title">Tên:</div>
                  <div className="tree-value">{tree.name}</div>
                </div>
                <div className="row-flex tree-info">
                  <div className="tree-title">Loài:</div>
                  <div className="tree-value">{tree.plant.name}</div>
                </div>
                <div className="row-flex tree-info">
                  <div className="tree-title">Khu vực:</div>
                  <div className="tree-value">
                    {tree.area === null ? "Khác" : tree.area.name}
                  </div>
                </div>
                <div className="row-flex tree-info">
                  <div className="tree-title">Tình trạng:</div>
                  <div className="tree-value">
                    {getStatus(getCurrentTreeStatus())}
                  </div>
                </div>
                <div className="row-flex tree-info">
                  <div className="tree-title">Mô tả:</div>
                  <div className="tree-description">
                    {tree.description ? tree.description : "..."}
                  </div>
                </div>
              </div>
            </div>
            <div className="box-shadow-sm br-5 mt-20 bg-white pd-10">
              <div className="c-text-2xl text-center bold">Công việc</div>
              <div className="wr-container tree-remind">
                {tree.reminders.length > 0 ? (
                  tree.reminders.map((e, index) => {
                    return (
                      <div
                        key={`diary__reminder-${index}`}
                        className="row-flex b-top remind-item"
                      >
                        <a
                          href="/reminder"
                          target="__blank"
                          className="c-text-base"
                        >
                          {e.note ?? ""}
                        </a>
                        <div className="c-text-base">
                          {subTime(e.time ?? "...")}
                        </div>
                      </div>
                    );
                  })
                ) : (
                  <div className="row-flex b-top remind-item">
                    <a
                      href="/reminder"
                      target="__blank"
                      className="c-text-base"
                    >
                      Hiện tại
                    </a>
                    <div className="c-text-base">Không có công việc</div>
                  </div>
                )}
              </div>
            </div>
          </div>
          <div className="wr-diary-page--diary relative">
            <div className="wr">
              <div className="c-text-2xl text-center bold">NHẬT KÝ</div>
              <div className="timer-path"></div>
              <div className="diary-list-wrapper">
                <BoxDetail
                  isLoading={loading}
                  data={diaries}
                  isShowStage={isShowStage}
                  tree={tree}
                />
              </div>
            </div>
          </div>
          <div className="group-button-fixed">
            <button
              className={`btn-mode-fixed${isShowStage ? "" : " filler-blur"}`}
              onClick={changeSettingShowStage}
            >
              {isShowStage ? (
                <FontAwesomeIcon icon={faEye} />
              ) : (
                <FontAwesomeIcon icon={faEyeSlash} />
              )}
            </button>
            {getCurrentTreeStatus() < 2 && (
              <button
                className="btn-mode-fixed"
                onClick={() => setAddAction(true)}
              >
                <PlusIcon />
              </button>
            )}
          </div>

          <DiaryModalCreate hidden={addAction} closeModal={closeModal} />
        </div>
      ) : noData ? (
        <div className="wr-diary-page-responsive">Không có bài viết</div>
      ) : (
        <div className="overlay-background">
          <Loading />
        </div>
      )}
    </>
  );
}
