/* eslint-disable no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
import "./DiaryModal.scss";
import CancelIcon from "../../../../icons/CancelIcon";
import UploadPlantImgIcon from "../../../../icons/UploadPlantImgIcon";

import ButtonLoading from "../../../../components/ButtonLoadind/ButtonLoading";
import DropDown from "../../../../components/DropDown/DropDown";
import Textarea from "../../../../components/Textarea/Textarea";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faClock,
  faSun,
  faHeart,
  faNoteSticky,
  faImages,
} from "@fortawesome/free-regular-svg-icons";
import { faChartLine, faXmark } from "@fortawesome/free-solid-svg-icons";
import { useEffect, useRef, useState } from "react";

import gardenService from "../../../../services/gardenService";
import plantService from "../../../../services/plantService";
import moment from "moment";
import { toast } from "react-toastify";
import { TREE_STATUS } from "../../../../utils/utils";
import { useDispatch, useSelector } from "react-redux";
import { SaveTree } from "../../../../redux/tree";

const WEATHER_LIST = ["Nắng", "Mưa", "Có gió"];

export default function DiaryModalCreate({ hidden = true, closeModal }) {
  const imageRef = useRef();

  const treeRedux = useSelector((state) => state.Tree);
  const dispatch = useDispatch();
  const [data, setData] = useState({
    date_published: moment(new Date()).format("YYYY-MM-DD"),
    stage_no: null,
    weather: WEATHER_LIST[0],
    status: 0,
    note: "",
    image: null,
  });

  const [stages, setStages] = useState([]);
  const [loading, setLoading] = useState(false);

  const lastDate = getLastDay(treeRedux);

  function getLastDay(_data) {
    let _lastDate = "";
    _data.diaries.forEach((element, index) => {
      if (element.time !== "" && element.time !== null) {
        const currentDate = new Date(Date.parse(element.time));
        currentDate.setDate(currentDate.getDate() + 1);
        _lastDate = moment(currentDate).format("YYYY-MM-DD");
      }
    });
    return _lastDate;
  }

  useEffect(() => {
    const fetchStages = async () => {
      try {
        const { tree } = treeRedux;
        const { data } = await plantService.getStages(tree.id);
        setStages(data);
      } catch (error) {}
    };
    if (hidden) {
      fetchStages();
    }
  }, [hidden]);

  const changeValue = (name, value) =>
    setData((prev) => ({ ...prev, [name]: value }));

  const changeImage = (e) => {
    const image = e.target.files[0];
    image && changeValue("image", image);
  };

  const removeImage = (e) => {
    e.stopPropagation();
    changeValue("image", null);
  };
  const { tree } = treeRedux;

  const handleSubmit = async () => {
    if (!loading) {
      if (!data.stage_no) {
        toast.warning("Giai đoạn không bỏ trống!");
        return;
      }
      const formRequest = new FormData();
      for (let key in data) {
        formRequest.append(key, data[key]);
      }

      // CHECK PLANT ID
      formRequest.append("plant_id", tree.plant_id);
      setLoading(true);
      try {
        // LẤY TREEID từ trên
        const { data } = await gardenService.createDiary(tree.id, formRequest);
        dispatch(SaveTree(data));
        closeModal();
        toast.info("Đã thêm nhật ký mới!");
      } catch (error) {
        if (error?.response) {
          toast.error(error?.response.data.message);
        } else toast.error("Chỉnh sửa không thành công!");
      }
      setLoading(false);
    }
  };

  const weatherOptions = WEATHER_LIST.map((w) => ({ name: w, id: w }));

  const StatusOptions = TREE_STATUS.map((w, index) => ({ name: w, id: index }));

  let stageOptions = stages.map((s) => ({
    id: s.no,
    name: s.no + ". " + s.name,
  }));

  const lastDiary = treeRedux.diaries
    .map((d, index) => ({ ...d, stage_no: index + 1 }))
    .filter((d) => d.diary);

  if (lastDiary.length > 0) {
    console.log(lastDiary);
    const l = lastDiary[lastDiary.length - 1];
    stageOptions = stageOptions.filter((s) => s.id >= l.stage_no);
  }

  return (
    <div className={`c-modal a ${hidden ? "" : "d-hidden"}`}>
      <div className={`bg-prevent-click `} onClick={() => closeModal()}></div>
      <div
        className={`modal-index diary-modal-edit ${hidden ? "" : "d-hidden"}`}
      >
        <div className="modal-custom-header">
          <div className="flex-10"></div>
          <div className="modal-title c-text-base bold color-white">
            Viết nhật ký
          </div>
          <div>
            <button className="btn-cancel" onClick={() => closeModal()}>
              <CancelIcon />
            </button>
          </div>
        </div>

        <div className="modal-custom-body">
          <div className="body-top">
            <div className="body-line">
              <div className="title">
                <FontAwesomeIcon icon={faClock} />
                Ngày
              </div>
              <div className="value">
                <input
                  type="date"
                  onChange={(e) =>
                    e.target.value &&
                    changeValue("date_published", e.target.value)
                  }
                  min={lastDate ? lastDate : undefined}
                  max={moment(new Date()).format("YYYY-MM-DD")}
                  value={data.date_published ? data.date_published : undefined}
                />
              </div>
            </div>

            <div className="body-line">
              <div className="title">
                <FontAwesomeIcon icon={faChartLine} />
                Giai đoạn (*)
              </div>
              <div className="value">
                <DropDown
                  options={stageOptions}
                  onChange={(value) => changeValue("stage_no", value)}
                  value={data.stage_no}
                />
              </div>
            </div>

            <div className="body-line">
              <div className="title">
                <FontAwesomeIcon icon={faSun} />
                Thời tiết
              </div>
              <div className="value">
                <DropDown
                  options={weatherOptions}
                  onChange={(value) => changeValue("weather", value)}
                  value={data.weather}
                />
              </div>
            </div>

            <div className="body-line">
              <div className="title">
                <FontAwesomeIcon icon={faHeart} />
                Tình trạng
              </div>
              <div className="value">
                <DropDown
                  options={StatusOptions}
                  onChange={(value) => changeValue("status", value)}
                  value={data.status}
                />
              </div>
            </div>
          </div>
          <div className="body-bottom">
            <div className="body-line">
              <div className="title">
                <FontAwesomeIcon icon={faNoteSticky} />
                Ghi chú
              </div>
              <div className="value">
                <div className="wr-textarea">
                  <Textarea
                    spellCheck="false"
                    rows={3}
                    maxRows={4}
                    placeholder="Thêm ghi chú ..."
                    onChange={(value) => changeValue("note", value)}
                  />
                </div>
              </div>
            </div>

            <div className="body-line">
              <div className="title">
                <FontAwesomeIcon icon={faImages} />
                Hình ảnh
              </div>
              <div className="value">
                <div
                  className="wr-image"
                  onClick={() => imageRef.current.click()}
                >
                  <input
                    type="file"
                    hidden
                    ref={imageRef}
                    onChange={changeImage}
                    accept="image/*"
                  />
                  {data.image ? (
                    <>
                      <img src={URL.createObjectURL(data.image)} alt="" />
                      <FontAwesomeIcon
                        icon={faXmark}
                        className="remove-img"
                        onClick={removeImage}
                      />
                    </>
                  ) : (
                    <UploadPlantImgIcon />
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="modal-custom-footer">
          <ButtonLoading
            isLoading={loading}
            click={handleSubmit}
            title={"Lưu"}
          />
        </div>
      </div>
    </div>
  );
}
