import "./DiaryModal.scss";
// import Loading from "../../../../components/Loading/Loading";
import CancelIcon from "../../../../icons/CancelIcon";
import UploadPlantImgIcon from "../../../../icons/UploadPlantImgIcon";

import ButtonLoading from "../../../../components/ButtonLoadind/ButtonLoading";
import DropDown from "../../../../components/DropDown/DropDown";
import Textarea from "../../../../components/Textarea/Textarea";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faClock,
  faSun,
  faHeart,
  faNoteSticky,
  faImages,
} from "@fortawesome/free-regular-svg-icons";
import { faChartLine, faXmark } from "@fortawesome/free-solid-svg-icons";
import { useEffect, useRef, useState } from "react";

import gardenService from "../../../../services/gardenService";
import plantService from "../../../../services/plantService";
import { DatePicker } from "rsuite";
import { TREE_STATUS } from "../../../../utils/utils";

const WEATHER_LIST = ["Nắng", "Mưa", "Có gió"];

export default function DiaryModalUpdate(props) {
  const imageRef = useRef();

  const [data, setData] = useState({
    id: props.data.id,
    date_published: props.data.date_published,
    stage_no: props.data.stage_no,
    weather: props.data.weather,
    status: props.data.status,
    note: props.data.note,
    image: props.data.img,
  });
  const [stages, setStages] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setData({
      id: props.data.id,
      date_published: props.data.date_published,
      stage_no: props.data.stage_no,
      weather: props.data.weather,
      status: props.data.status,
      note: props.data.note,
      image: props.data.img,
    });
  }, [props.data]);

  useEffect(() => {
    const fetchStages = async () => {
      try {
        const tree = props.tree;
        const { data } = await plantService.getStages(tree.id);
        setStages(data);
      } catch (error) {}
    };
    fetchStages();
  }, [props.tree]);

  const changeValue = (name, value) => {
    setData((prev) => ({ ...prev, [name]: value }));
  };

  const changeImage = (e) => {
    const image = e.target.files[0];
    image && changeValue("image", image);
  };

  const removeImage = (e) => {
    e.stopPropagation();
    changeValue("image", null);
  };

  const handleSubmit = async () => {
    if (!loading) {
      const formRequest = new FormData();
      // init form data
      for (let key in data) {
        // check if not upload new img -> type string
        if (
          key === "image" &&
          (typeof data.image === "string" || data.image === null)
        ) {
          formRequest.append("_img", data[key]);
        } else {
          // uploaded new image
          formRequest.append(key, data[key]);
        }
      }

      setLoading(true);
      try {
        const res = await gardenService.updateDiary(data.id, formRequest);
        props.onSuccess(res.data);
        props.closeModal();
      } catch (error) {}
      setLoading(false);
    }
  };

  const weatherOptions = WEATHER_LIST.map((w) => ({ name: w, id: w }));

  const StatusOptions = TREE_STATUS.map((w, index) => ({ name: w, id: index }));

  const stageOptions = stages.map((s) => ({
    id: s.no,
    name: s.no + ". " + s.name,
  }));

  return (
    <div className={`c-modal a ${props.hidden ? "" : "d-hidden"}`}>
      <div
        className={`bg-prevent-click `}
        onClick={() => props.closeModal()}
      ></div>
      <div
        className={`modal-index diary-modal-edit ${
          props.hidden ? "" : "d-hidden"
        }`}
      >
        <div className="modal-custom-header">
          <div className="flex-10"></div>
          <div className="modal-title c-text-base bold color-white">
            Chỉnh sửa
          </div>
          <div>
            <button className="btn-cancel" onClick={() => props.closeModal()}>
              <CancelIcon />
            </button>
          </div>
        </div>

        <div className="modal-custom-body">
          <div className="body-top">
            <div className="body-line">
              <div className="title">
                <FontAwesomeIcon icon={faClock} />
                Ngày
              </div>
              <div className="value">
                <DatePicker
                  editable={false}
                  oneTap
                  value={new Date(data.date_published)}
                  format="dd/MM/yyyy"
                  style={{ pointerEvents: "none", width: "150px" }}
                />
              </div>
            </div>

            <div className="body-line">
              <div className="title">
                <FontAwesomeIcon icon={faChartLine} />
                Giai đoạn
              </div>
              <div className="value">
                <DropDown
                  options={stageOptions}
                  onChange={(value) => changeValue("stage_no", value)}
                  value={data.stage_no}
                  disabled
                />
              </div>
            </div>

            <div className="body-line">
              <div className="title">
                <FontAwesomeIcon icon={faSun} />
                Thời tiết
              </div>
              <div className="value">
                <DropDown
                  options={weatherOptions}
                  onChange={(value) => changeValue("weather", value)}
                  value={data.weather}
                />
              </div>
            </div>

            <div className="body-line">
              <div className="title">
                <FontAwesomeIcon icon={faHeart} />
                Tình trạng
              </div>
              <div className="value">
                <DropDown
                  options={StatusOptions}
                  onChange={(value) => changeValue("status", value)}
                  value={data.status}
                />
              </div>
            </div>
          </div>
          <div className="body-bottom">
            <div className="body-line">
              <div className="title">
                <FontAwesomeIcon icon={faNoteSticky} />
                Ghi chú
              </div>
              <div className="value">
                <div className="wr-textarea">
                  <Textarea
                    spellCheck="false"
                    rows={3}
                    maxRows={4}
                    placeholder="Thêm ghi chú ..."
                    onChange={(value) => changeValue("note", value)}
                    value={data.note}
                  />
                </div>
              </div>
            </div>

            <div className="body-line">
              <div className="title">
                <FontAwesomeIcon icon={faImages} />
                Hình ảnh
              </div>
              <div className="value">
                <div
                  className="wr-image"
                  onClick={() => imageRef.current.click()}
                >
                  <input
                    type="file"
                    hidden
                    ref={imageRef}
                    onChange={changeImage}
                    accept="image/*"
                  />
                  {data.image ? (
                    <>
                      <img
                        src={
                          typeof data.image === "string"
                            ? data.image
                            : URL.createObjectURL(data.image)
                        }
                        alt=""
                      />
                      <FontAwesomeIcon
                        icon={faXmark}
                        className="remove-img"
                        onClick={removeImage}
                      />
                    </>
                  ) : (
                    <UploadPlantImgIcon />
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="modal-custom-footer">
          <ButtonLoading
            isLoading={loading}
            click={handleSubmit}
            title={"Lưu"}
          />
        </div>
      </div>
    </div>
  );
}
