/* eslint-disable no-unused-vars */
import Loading from "../../../../components/Loading/Loading";
import parse from "html-react-parser";
import moment from "moment";
import DiaryModalUpdate from "./DiaryModalUpdate";
import { useState } from "react";
import { toast } from "react-toastify";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import gardenService from "../../../../services/gardenService";
import { faEllipsisVertical } from "@fortawesome/free-solid-svg-icons";
import ModalConfirm from "../../../../components/ModalConfirm/ModalConfirm";
import { useDispatch, useSelector } from "react-redux";
import { SaveTree } from "../../../../redux/tree";
import { TREE_STATUS } from "../../../../utils/utils";
import { Dropdown } from "rsuite";
export default function BoxDetail(props) {
  const [addAction, setAddAction] = useState(false);
  const [confirmUp, setConfirmUp] = useState(true);
  const [isLoading, setIsLoading] = useState(false);
  const [idDelete, setIdDelete] = useState(0);
  const [updateModal, setUpdateModal] = useState({
    id: 0,
    date_published: "",
    stage_no: 1,
    weather: "Nắng",
    status: "Khỏe mạnh",
    note: "",
    img: "",
  });

  const dispatch = useDispatch();

  const treeRedux = useSelector((state) => state.Tree);

  async function deleteDiary(id) {
    setConfirmUp(false);
    setIdDelete(id);
  }

  const closeModal = () => {
    setAddAction(false);
    setUpdateModal({
      id: 0,
      date_published: "",
      stage_no: 1,
      weather: "Nắng",
      status: "Khỏe mạnh",
      note: "",
      img: "",
    });
  };

  function updateSuccess(newData) {
    toast.success("Cập nhật thành công!");
    dispatch(SaveTree(newData));
  }
  var stage_no = 0;

  function renderBoxStage(stage) {
    if (stage !== null) {
      return (
        <div key={`stage-${stage.id}`} className="diary-item-container">
          <div className={`date-txt c-text-base`}>
            {parse(stage.date_estimate)}
          </div>
          <div className="diary-content-item diary-stage">
            <div className="header d-flex">
              <div className="title">[ {stage.title} ]</div>
              <div className="dot stage"></div>
              <div className="title-sub">{stage.sub_title}</div>
            </div>
            <div className="content">
              <div className="note c-text-base">{parse(stage.note)}</div>
            </div>
          </div>
        </div>
      );
    }
  }
  function getStatus(status_id) {
    return TREE_STATUS[status_id] ?? "Khỏe mạnh";
  }

  function actionEditBox(diary, time) {
    setUpdateModal({
      id: diary.id,
      date_published: time,
      stage_no: diary.stage_no,
      stage_name: diary.stage_name,
      weather: diary.sub_title,
      status: diary.title,
      note: diary.note,
      img: diary.img,
    });
    setAddAction(true);
  }
  function renderBoxDiary(diary, time, index) {
    if (diary !== null) {
      let note = "";
      if (diary.img !== null && diary.img !== "" && diary.img !== "null") {
        note += `<img className="float-right img" src=${diary.img} alt="img" />`;
      }
      note += `${diary.note}`;
      return (
        <div key={`diary-${diary.id}`} className="diary-item-container">
          <div className={`date-txt c-text-base`}>
            {moment(time).format("DD/MM/YYYY")}
          </div>
          <div className="diary-content-item" id={`diary_box-${diary.id}`}>
            <div
              className="header d-flex"
              style={{ justifyContent: "space-between" }}
            >
              <div style={{ display: "flex", gap: 10 }}>
                <div className="title">{getStatus(diary.title)}</div>
                <div className="title-sub">{diary.sub_title}</div>
              </div>

              <Dropdown
                renderToggle={(props, ref) => (
                  <button
                    {...props}
                    ref={ref}
                    className="tree__diary-box-header-menu"
                    style={{ backgroundColor: "transparent" }}
                  >
                    <FontAwesomeIcon icon={faEllipsisVertical} />
                  </button>
                )}
                trigger="hover"
                placement="bottomEnd"
              >
                <Dropdown.Item
                  onSelect={() => {
                    actionEditBox(diary, time);
                  }}
                >
                  Sửa
                </Dropdown.Item>

                <Dropdown.Item
                  onSelect={() => {
                    deleteDiary(diary.id);
                  }}
                >
                  Xóa
                </Dropdown.Item>
              </Dropdown>

              {/* <div className="tree__diary-box-header-menu">
                <div className="tree__diary-box-dropdown-options">
                  {index !== 0 ? (
                    <div
                      className="tree__diary-box-dropdown-item"
                      onClick={() => {
                        deleteDiary(diary.id);
                      }}
                    >
                      Xoá
                    </div>
                  ) : (
                    <></>
                  )}
                  <div
                    className="tree__diary-box-dropdown-item"
                    onClick={() => {
                      actionEditBox(diary, time);
                    }}
                  >
                    Sửa
                  </div>
                </div>
              </div> */}
            </div>
            <div className="content">
              <div className="note c-text-base">{parse(note)}</div>
            </div>
          </div>
        </div>
      );
    }
  }

  function renderItemDiary(diary, time, key, is_stage = false) {
    if (diary !== null) {
      if (is_stage) return renderBoxStage(diary);
      else return renderBoxDiary(diary, time, key);
    } else
      return (
        <div key={`no-data-${key}`} className="diary-item-container"></div>
      );
  }

  function insertRow(left, point, right, row) {
    row.push(left);
    row.push(
      <div key={point.index} title={point.time} className="point-item">
        {point.no}
      </div>
    );
    row.push(right);
  }

  function insertItem(element, index, row) {
    let no = "";
    if (element.stage) {
      stage_no = stage_no + 1;
      no = stage_no;
    }

    insertRow(
      renderItemDiary(element.stage, element.time, index, true),
      { index: index, no, time: element.time },
      renderItemDiary(element.diary, element.time, index),
      row
    );

    return;
  }

  function renderDiary(data) {
    let el = [];
    let isLeft = true;
    if (!props.isShowStage) {
      data.forEach((element, index) => {
        let row = [];
        if (element.diary !== null) {
          if (isLeft) {
            insertRow(
              renderItemDiary(element.diary, element.time, index),
              { index: index, no: "", time: element.time },
              renderItemDiary(null, element.time, index),
              row
            );
          } else {
            insertRow(
              renderItemDiary(null, element.time, index),
              { index: index, no: "", time: element.time },
              renderItemDiary(element.diary, element.time, index),
              row
            );
          }

          el.push(
            <div key={index} className={`row-diary`}>
              {row}
            </div>
          );

          isLeft = !isLeft;
        }
      });
    } else {
      data.forEach((element, index) => {
        let row = [];

        insertItem(element, index, row);
        el.push(
          <div key={index} className={`row-diary`}>
            {row}
          </div>
        );
      });
    }
    return el;
  }
  const deleteConfirm = async () => {
    try {
      setIsLoading(true);
      const res = await gardenService.deleteDiary(idDelete);
      const diaries = treeRedux.diaries.filter(
        (item) => !item?.diary || item.diary.id !== idDelete
      );
      dispatch(SaveTree({ tree: treeRedux.tree, diaries: diaries }));
      setIdDelete(0);
      setConfirmUp(true);
      toast.success("Xoá nhật ký thành công!", { theme: "colored" });
    } catch (error) {
      console.log(error);
      toast.error("Xoá nhật ký thất bại!", { theme: "colored" });
    }
    setIsLoading(false);
  };
  const closeConfirm = () => {
    setIdDelete(0);
    setConfirmUp(true);
  };
  return (
    <>
      {props.isLoading ? <Loading /> : renderDiary(props.data || [])}
      <DiaryModalUpdate
        hidden={addAction}
        closeModal={closeModal}
        onSuccess={updateSuccess}
        data={updateModal}
        tree={props.tree}
      />
      <ModalConfirm
        isLoading={isLoading}
        message={"Bạn chắc chắn muốn xoá!"}
        hidden={confirmUp}
        onConfirm={deleteConfirm}
        onCancel={closeConfirm}
      />
    </>
  );
}
