import "./DeleteAreaModal.scss";

import { useRef, useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faWarning } from "@fortawesome/free-solid-svg-icons";
import Loading from "../../../../components/Loading/Loading";

const DeleteAreaModal = ({ onClose, onConfirm }) => {
  const thisModal = useRef();
  const outerClose = (e) => {
    e.target === thisModal.current && onClose && onClose();
  };

  const [loading, setLoading] = useState(false);

  const handleConfirm = () => {
    setLoading(true);
    onConfirm && onConfirm();
    setLoading(false);
    onClose && onClose();
  };

  return (
    <div className={`confirm-modal`} ref={thisModal} onClick={outerClose}>
      <div className="confirm-modal-box">
        <div className="confirm-modal-header">
          <div className="header-icon">
            <FontAwesomeIcon icon={faWarning} />
          </div>
          <div className="header-title">Bạn chắc chắn chưa?</div>
        </div>
        <div className="confirm-modal-footer">
          <div className="selection-group">
            <div className="select cancel-btn" onClick={onClose}>
              Hủy
            </div>
            <div className="select confirm-btn" onClick={handleConfirm}>
              {loading ? <Loading /> : "OK"}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default DeleteAreaModal;
