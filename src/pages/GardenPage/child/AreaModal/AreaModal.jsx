import "./AreaModal.scss";

import { useEffect, useRef, useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faXmark } from "@fortawesome/free-solid-svg-icons";

import Loading from "../../../../components/Loading/Loading";

import { useDispatch } from "react-redux";
import gardenService from "../../../../services/gardenService";
import { CreateArea } from "../../../../redux/garden";
import { toast } from "react-toastify";
import { Modal } from "rsuite";

const AreaModal = ({ onClose, show }) => {
  const dispatch = useDispatch();

  const inputRef = useRef();

  const [area, setArea] = useState("");
  const [loading, setLoading] = useState(false);
  const [empty, setEmpty] = useState(false);

  useEffect(() => {
    if (show) {
      inputRef.current.focus();
    }
  }, [show]);

  const handleChange = (e) => {
    setArea(e.target.value);
    setEmpty(false);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    setLoading(true);
    try {
      const areaName = area.trim().toUpperCase();
      setArea(areaName);
      if (areaName) {
        const { data } = await gardenService.addArea({ name: areaName });
        dispatch(CreateArea(data));
        setArea("");
        toast.success("Đã thêm khu vực mới", { autoClose: 1500 });
        onClose && onClose();
      } else {
        setEmpty(true);
        inputRef.current.focus();
      }
    } catch (error) {
      console.log(error);
      toast.error(error.response.data);
      inputRef.current.focus();
    }
    setLoading(false);
  };

  return (
    <Modal className={`area-modal`} open={show} onClose={onClose}>
      <form className="area-modal-box" onSubmit={handleSubmit}>
        <div className="area-modal-header">
          Thêm khu vực
          <div className="close-btn" onClick={onClose}>
            <FontAwesomeIcon icon={faXmark} />
          </div>
        </div>
        <div className="area-modal-body">
          <div className="area-info-list">
            <div className="area-info">
              <div className="title">Tên: </div>
              <div className="value">
                <input
                  ref={inputRef}
                  name="name"
                  type="text"
                  value={area}
                  onChange={handleChange}
                  placeholder="Khu vực ..."
                />
              </div>
            </div>
            {empty && (
              <div className="area-info">
                <div className="title"></div>
                <div
                  className="value"
                  style={{
                    width: "70%",
                    justifyContent: "flex-start",
                    paddingLeft: 5,
                    color: "red",
                  }}
                >
                  Không được bỏ trống
                </div>
              </div>
            )}
          </div>
        </div>
        <div className="area-modal-footer">
          <button className="submit-btn btn">
            {loading ? <Loading /> : "Thêm"}
          </button>
        </div>
      </form>
    </Modal>
  );
};

export default AreaModal;
