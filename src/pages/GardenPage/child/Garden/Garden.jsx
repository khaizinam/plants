import "./Garden.scss";

import { useEffect, useRef, useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faGear,
  faMinus,
  faPlus,
  faSearch,
  faTrashCan,
  faXmark,
} from "@fortawesome/free-solid-svg-icons";

import { toast } from "react-toastify";
import { NavLink } from "react-router-dom";

import { useDispatch, useSelector } from "react-redux";
import { DeleteTree, GetGarden, UpdateArea } from "../../../../redux/garden";
import gardenService from "../../../../services/gardenService";
import ChartService from "../../../../services/chartService";

import TreeModal from "../TreeModal/TreeModal";
import EditModal from "../EditModal/EditModal";
import AreaModal from "../AreaModal/AreaModal";
import EditIcon from "../../../../icons/EditIcon";
import GeneralStatusStatistic from "../../../../components/GeneralStatusStatistic/GeneralStatusStatistic";
import { TREE_STATUS, normalizeString } from "../../../../utils/utils";
import { Modal, List, Input } from "rsuite";
import Loading from "../../../../components/Loading/Loading";
import { faEye } from "@fortawesome/free-regular-svg-icons";
import ModalDelete from "../../../../components/ModalDelete/ModalDelete";

function formatDateToDDMMYYYY(str) {
  // Extract day, month, and year from the date object
  const date = new Date(str);
  const day = date.getDate();
  const month = date.getMonth() + 1; // Adding 1 because months are zero-indexed
  const year = date.getFullYear();

  // Pad single digit day/month with leading zero if necessary
  const formattedDay = day < 10 ? "0" + day : day;
  const formattedMonth = month < 10 ? "0" + month : month;

  // Return the formatted date string
  return formattedDay + "/" + formattedMonth + "/" + year;
}

const Status = ({ status }) => {
  let classStatus;
  if (status === 0) classStatus = "well";
  if (status === 1) classStatus = "week";
  if (status === 2) classStatus = "harvest";
  if (status === 3) classStatus = "dead";
  return <div className={`status ${classStatus}`}>{TREE_STATUS[status]}</div>;
};

const TreeItem = ({
  id,
  name,
  plant_id,
  plant,
  status,
  modified_at,
  description,
  index,
}) => {
  const dispatch = useDispatch();
  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const [edit, setEdit] = useState(false);
  const handleOpenEdit = () => setEdit(true);
  const handleCloseEdit = () => setEdit(false);

  const deleteTree = async () => {
    try {
      await gardenService.deleteTree(id);
      dispatch(DeleteTree(id));
      toast.success(<>Đã xóa cây</>, { autoClose: 1500 });
    } catch (error) {
      console.log(error);
      toast.error(<>Lỗi hệ thống, vui lòng tải lại trang</>, {
        autoClose: 1500,
      });
    }
  };
  return (
    <>
      <List.Item
        index={index}
        as="tr"
        className={`tree-item`}
        collection={"sort"}
      >
        <td>
          <span>#{id}</span>
        </td>
        <td>
          <span>{name}</span>
        </td>
        <td>
          <a
            className="plant-name"
            href={`/library/${plant_id}`}
            target="_blank"
            rel="noreferrer"
          >
            <div className="plant-image">
              <img
                src={
                  plant?.avatar ||
                  "https://newfreshfoods.com.vn/datafiles/3/2018-02-27/16100958548622_hoaqua_1-500x500.jpg"
                }
                alt=""
              />
            </div>
            <span>{plant?.name}</span>
          </a>
        </td>
        <td>
          <span>{formatDateToDDMMYYYY(modified_at)}</span>
        </td>
        <td>
          <Status status={status} />
        </td>
        <td className="actions">
          <NavLink
            to={`./${id}`}
            className="action-icon"
            title="Chi tiết"
            onClick={handleOpen}
          >
            <FontAwesomeIcon icon={faEye} className="view" />
          </NavLink>
          <button
            className="action-icon"
            title="Chỉnh sửa"
            onClick={handleOpenEdit}
          >
            <EditIcon size={20} />
          </button>
          <button className="action-icon" title="Xóa" onClick={handleOpen}>
            <FontAwesomeIcon icon={faXmark} className="delete" />
          </button>
        </td>
      </List.Item>
      <ModalDelete
        open={open}
        onClose={handleClose}
        onDelete={deleteTree}
        content={
          <>
            Bạn chắc chắn xóa <b>{name}</b> ?
          </>
        }
      />

      {edit && (
        <EditModal
          onClose={handleCloseEdit}
          show={edit}
          tree={{ id, name, description }}
        />
      )}
    </>
  );
};

const UpdateAreaModal = ({ onClose, open, area }) => {
  const dispatch = useDispatch();
  const areaRef = useRef();

  const [loading, setLoading] = useState(false);
  const [name, setName] = useState(area.name);
  const [empty, setEmpty] = useState(false);

  const handleSubmit = async (e) => {
    e.preventDefault();
    setLoading(true);
    try {
      let n = name.trim();
      if (n) {
        const { data } = await gardenService.updateArea(area.id, { name: n });
        dispatch(UpdateArea(data));
        onClose && onClose();
        toast.info(<>Đã đổi tên khu vực</>, { autoClose: 1500 });
      } else {
        areaRef.current.focus();
        setEmpty(true);
      }
    } catch (error) {
      console.log(error);
      const msgErr = error.response?.data || "Lỗi hệ thống";
      toast.error(<>{msgErr}</>, { autoClose: 1500 });
    }
    setLoading(false);
  };

  return (
    <Modal onClose={onClose} open={open} className="tree-modal">
      <form className="tree-modal-box" onSubmit={handleSubmit}>
        <div className="tree-modal-header">Đổi tên</div>
        <div className="tree-modal-body">
          <div className="tree-info-list">
            <div className="tree-info">
              <div className="title">Tên: </div>
              <div className="value" style={{ width: "60%" }}>
                <Input
                  value={name}
                  onChange={(v) => {
                    setName(v);
                    setEmpty(false);
                  }}
                  placeholder="Khu vực ..."
                  ref={areaRef}
                />
              </div>
            </div>
            {empty && (
              <div className="tree-info">
                <div className="title"></div>
                <small
                  className="value"
                  style={{
                    width: "60%",
                    justifyContent: "flex-start",
                    paddingLeft: 5,
                    color: "red",
                  }}
                >
                  Vui lòng nhập tên khu vực
                </small>
              </div>
            )}
          </div>
        </div>
        <div className="tree-modal-footer">
          <button className="submit-btn btn">
            {loading ? <Loading /> : "Cập nhật"}
          </button>
        </div>
      </form>
    </Modal>
  );
};

const Area = ({ area, search, index }) => {
  const dispatch = useDispatch();
  const [hide, setHide] = useState(false);

  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const [remove, setRemove] = useState(false);
  const openRemove = () => setRemove(true);
  const closeRemove = () => setRemove(false);

  const isContain = area?.trees?.length > 0;

  const deleteArea = async () => {
    try {
      const { data } = await gardenService.deleteArea(area.id);
      dispatch(GetGarden(data));
      toast.warn(`Đã xóa khu vực ${area.name}`, {
        autoClose: 1500,
      });
    } catch (error) {
      toast.error(<>{error.response.data || "Lỗi hệ thông"}</>, {
        autoClose: 1500,
      });
    }
  };

  const newTrees = area.trees.filter(
    (item) =>
      !search || normalizeString(item.name).includes(normalizeString(search))
  );

  const newArea = { ...area, trees: newTrees };

  return (
    <>
      <tbody className="area">
        <List.Item
          as="tr"
          className="heading"
          disabled
          index={index}
          collection={index === 0 ? "none" : "sort"}
        >
          <td
            colSpan={3}
            className="area-title"
            onClick={isContain ? () => setHide((prev) => !prev) : null}
          >
            <FontAwesomeIcon
              icon={hide ? faPlus : faMinus}
              className={`area-collapse-icon ${isContain ? "" : "hideee"}`}
            />
            <span>{newArea.name}</span>
          </td>
          <td colSpan={2}></td>
          <td className="area-actions">
            {newArea.id && (
              <>
                <FontAwesomeIcon
                  icon={faGear}
                  className="action"
                  onClick={handleOpen}
                />
                <FontAwesomeIcon
                  icon={faTrashCan}
                  className="action"
                  onClick={openRemove}
                />
              </>
            )}
          </td>
        </List.Item>

        <UpdateAreaModal onClose={handleClose} open={open} area={area} />

        {!hide &&
          newArea.trees.map((tree, i) => (
            <TreeItem key={tree.id} {...tree} index={index + i + 1} />
          ))}
      </tbody>
      <ModalDelete
        open={remove}
        onClose={closeRemove}
        onDelete={deleteArea}
        content={
          <>
            Bạn chắc chắn xóa khu vực <b>{area.name}</b> ?
          </>
        }
      />
    </>
  );
};

const Garden = () => {
  const dispatch = useDispatch();

  const [dataGeneralStatus, setDataGeneralStatus] = useState([]);
  const [treeModal, setTreeModal] = useState(false);
  const [areaModal, setAreaModal] = useState(false);

  const [loading, setLoading] = useState(true);

  const [search, setSearch] = useState("");

  const areas = useSelector((state) => state.Garden);

  useEffect(() => {
    getNumberStatuses();
  }, []);

  const getNumberStatuses = async () => {
    try {
      const { data } = await ChartService.getNumberStatuses();
      setDataGeneralStatus(data);
    } catch (error) {
      if (!error.response) {
        toast.error("Không tải được dữ liệu", { autoClose: 1500 });
      } else {
        toast.error("Hệ thống bị lỗi", { autoClose: 1500 });
      }
    }
  };

  useEffect(() => {
    const fetchGarden = async () => {
      setLoading(true);
      try {
        const { data } = await gardenService.getGarden();
        dispatch(GetGarden(data));
      } catch (error) {
        if (!error.response) {
          toast.error("Không tải được dữ liệu", { autoClose: 1500 });
        } else {
          toast.error("Hệ thống bị lỗi", { autoClose: 1500 });
        }
      }
      setLoading(false);
    };
    fetchGarden();
  }, [dispatch]);

  const handleSortEnd = async ({ oldIndex, newIndex }) => {
    if (oldIndex === newIndex) return;
    const findItem = (index) => {
      let areaIndex = 0;
      while (index > areas[areaIndex].trees.length) {
        index = index - areas[areaIndex].trees.length - 1;
        areaIndex++;
      }
      return [areaIndex, index - 1];
    };
    const [oldAreaIndex, oldTreeIndex] = findItem(oldIndex);
    let [newAreaIndex, newTreeIndex] = findItem(
      newIndex - (oldIndex < newIndex ? 0 : 1)
    );
    if (newAreaIndex !== oldAreaIndex) newTreeIndex++;
    if (newAreaIndex === oldAreaIndex && newIndex < oldIndex) newTreeIndex++;

    try {
      const tree = { ...areas[oldAreaIndex].trees[oldTreeIndex] };
      let newGarden;
      if (oldAreaIndex !== newAreaIndex) {
        await gardenService.updateTree(tree.id, {
          area_id: areas[newAreaIndex].id,
        });
        tree.area_id = areas[newAreaIndex].id;
      }
      newGarden = areas.map((area, areaIndex) => {
        let newTrees = [...area.trees];
        if (areaIndex === oldAreaIndex) {
          newTrees.splice(oldTreeIndex, 1);
        }
        if (areaIndex === newAreaIndex) {
          newTrees.splice(newTreeIndex, 0, tree);
        }
        return { ...area, trees: newTrees };
      });
      dispatch(GetGarden(newGarden));
    } catch (error) {
      console.log(error);
    }
  };

  const renderArea = () => {
    const result = [];
    let number = 0;
    for (let i = 0; i < areas.length; i++) {
      result.push(
        <Area
          key={areas[i].id}
          area={areas[i]}
          search={search}
          index={number}
        />
      );
      number = number + areas[i].trees.length + 1;
    }
    return result;
  };

  return (
    <div className="garden">
      <GeneralStatusStatistic dataStatus={dataGeneralStatus} />
      <div className="garden-container">
        <div className="garden-header">
          <div className="search-tree">
            <FontAwesomeIcon icon={faSearch} className="search-icon" />
            <input
              type="text"
              value={search}
              onChange={(e) => setSearch(e.target.value)}
              placeholder="Tìm kiếm tên cây ..."
            />
          </div>
          <div className="header-button">
            <div
              className="add-tree header-btn"
              onClick={() => setTreeModal(true)}
            >
              <FontAwesomeIcon icon={faPlus} size="xs" />
              <div className="btn-title">Cây trồng</div>
            </div>
            <div
              className="add-group header-btn"
              onClick={() => setAreaModal(true)}
            >
              <FontAwesomeIcon icon={faPlus} />
              <div className="btn-title">Khu vực</div>
            </div>
          </div>
        </div>
        {loading ? (
          <div style={{ paddingBottom: 10 }}>
            <Loading />
          </div>
        ) : (
          <List
            as="table"
            sortable
            onSort={handleSortEnd}
            className="garden-body"
          >
            <thead>
              <tr className="table-header">
                <th className="header-title">ID</th>
                <th className="header-title">Tên</th>
                <th className="header-title">Loài</th>
                <th className="header-title">Ngày cập nhật</th>
                <th className="header-title">Tình trạng</th>
                <th className="header-title">Thao tác</th>
              </tr>
            </thead>

            {renderArea()}
          </List>
        )}
      </div>
      <TreeModal onClose={() => setTreeModal(false)} show={treeModal} />
      <AreaModal onClose={() => setAreaModal(false)} show={areaModal} />
    </div>
  );
};

export default Garden;
