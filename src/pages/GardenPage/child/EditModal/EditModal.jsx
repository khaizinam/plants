import "./EditModal.scss";

import { useRef, useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faXmark } from "@fortawesome/free-solid-svg-icons";

import Loading from "../../../../components/Loading/Loading";
import { Modal, Input } from "rsuite";

import { useDispatch } from "react-redux";
import gardenService from "../../../../services/gardenService";
import { UpdateTree } from "../../../../redux/garden";
import { toast } from "react-toastify";

const EditModal = ({ onClose, show, tree }) => {
  const dispatch = useDispatch();
  const nameRef = useRef();

  const [loading, setLoading] = useState(false);

  const [input, setInput] = useState({
    name: tree.name,
    description: tree.description,
  });

  const handleChange = (e) =>
    setInput((prev) => ({
      ...prev,
      [e.target.name]: e.target.value,
    }));

  const handleSubmit = async (e) => {
    e.preventDefault();
    setLoading(true);
    try {
      let { name, description } = input;
      name = name.trim();
      if (!name) {
        toast.error("Vui lòng nhập tên");
        nameRef.current.focus();
      } else {
        const { data } = await gardenService.updateTree(tree.id, {
          name,
          description,
        });
        dispatch(UpdateTree(data));
        onClose && onClose();
        toast.success("Đã cập nhật");
      }
    } catch (error) {
      console.log(error);
      toast.error(error.response.data);
    }
    setLoading(false);
  };

  return (
    <Modal open={show} onClose={onClose} className="tree-modal">
      <form className="tree-modal-box" onSubmit={handleSubmit}>
        <div className="tree-modal-header">
          Chỉnh sửa thông tin cây
          <div className="close-btn" onClick={onClose}>
            <FontAwesomeIcon icon={faXmark} />
          </div>
        </div>
        <div className="tree-modal-body">
          <div className="tree-info-list">
            <div className="tree-info">
              <div className="title">Tên (*): </div>
              <div className="value">
                <Input
                  ref={nameRef}
                  type="text"
                  value={input.name}
                  onChange={(v) => setInput({ ...input, name: v })}
                  placeholder="Nhập tên cây"
                />
              </div>
            </div>
            <div className="tree-info">
              <div className="title">Mô tả:</div>
            </div>
          </div>
          <textarea
            spellCheck="false"
            className="tree-info-description"
            rows={2}
            name="description"
            onChange={handleChange}
            placeholder="Nhập mô tả của cây ..."
            value={input.description}
            style={{ fontSize: 14 }}
          ></textarea>
        </div>
        <div className="tree-modal-footer">
          <button className="submit-btn btn">
            {loading ? <Loading /> : "Cập nhật"}
          </button>
        </div>
      </form>
    </Modal>
  );
};

export default EditModal;
