import "./ConfirmModal.scss";

import { Component, createRef } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faWarning } from "@fortawesome/free-solid-svg-icons";

class ConfirmModal extends Component {
  constructor(props) {
    super(props);
    this.modal = createRef();
    this.state = {
      hidden: true,
    };
  }

  show(body, confirm) {
    this.setState({ body, onConfirm: confirm, hidden: false });
  }

  outerClose = (e) => {
    if (e.target === this.modal.current) {
      this.setState({ hidden: true });
    }
  };

  handleConfirm = () => {
    this.state.onConfirm();
    this.setState({ hidden: true });
  };

  render() {
    const { hidden, body } = this.state;
    return (
      <div
        className={`confirm-modal ${hidden ? "hidden" : ""}`}
        ref={this.modal}
        onClick={this.outerClose}
      >
        <div className="confirm-modal-box">
          <div className="confirm-modal-header">
            <div className="header-icon">
              <FontAwesomeIcon icon={faWarning} />
            </div>
            <div className="header-title">{body}</div>
          </div>
          <div className="confirm-modal-footer">
            <div className="selection-group">
              <div className="select cancel-btn">Hủy</div>
              <div className="select confirm-btn" onClick={this.handleConfirm}>
                OK
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ConfirmModal;
