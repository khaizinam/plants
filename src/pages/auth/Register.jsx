import { Link, NavLink, useNavigate } from "react-router-dom";
import Logo from "../../components/Logo/Logo";
import { useState, useCallback, useMemo } from "react";
import { toast } from "react-toastify";
import "./style.scss";
import authService from "../../services/authService";
import Loading from "../../components/Loading/Loading";

const Register = () => {
  const navigate = useNavigate();
  const [loading, setLoading] = useState(false);
  const [input, setInput] = useState({
    email: "",
    fullname: "",
    password: "",
    confirm: "",
  });
  const [errors, setErrors] = useState({
    email: "",
    fullname: "",
    password: "",
    confirm: "",
  });

  const handleChange = useCallback((e) => {
    const { name, value } = e.target;
    setInput((prev) => ({ ...prev, [name]: value }));
    setErrors((prev) => ({ ...prev, [name]: "" }));
  }, []);

  const emailRegex = useMemo(() => /^[^\s@]+@[^\s@]+\.[^\s@]+$/, []);

  const validate = useCallback(() => {
    const newErrors = {};
    if (!input.fullname) {
      newErrors.fullname = "Tên hiển thị không được để trống";
    } else if (input.fullname.length > 20) {
      newErrors.fullname = "Tên hiển thị không quá 20 ký tự";
    }
    if (!input.email) {
      newErrors.email = "Email không được để trống";
    } else if (input.email.length > 50) {
      newErrors.email = "Email không quá 50 ký tự";
    } else if (!emailRegex.test(input.email)) {
      newErrors.email = "Email không hợp lệ";
    }
    if (!input.password) {
      newErrors.password = "Mật khẩu không được để trống";
    } else if (input.password.length < 6 || input.password.length > 50) {
      newErrors.password = "Mật khẩu phải có ít nhất 6 ký tự và không quá 50 ký tự";
    }
    if (!input.confirm) {
      newErrors.confirm = "Nhập lại mật khẩu không được để trống";
    } else if (input.password !== input.confirm) {
      newErrors.confirm = "Mật khẩu không khớp";
    }
    setErrors(newErrors);
    return Object.keys(newErrors).length === 0;
  }, [input, emailRegex]);

  const handleSubmit = useCallback(async (e) => {
    e.preventDefault();
    if (loading) return;

    setLoading(true);
    if (validate()) {
      try {
        const { email, fullname, password } = input;
        await authService.register(email, password, fullname);
        toast.success("Đăng ký thành công", { theme: "colored" });
        navigate("/auth/login");
      } catch (error) {
        toast.error(error.response?.data?.message || "Lỗi hệ thống", { theme: "colored" });
      }
    }
    setLoading(false);
  }, [input, loading, navigate, validate]);

  return (
    <div className="register-container">
      <form className="register-box" onSubmit={handleSubmit}>
        <NavLink to={"/"} className="register-logo">
          <Logo />
        </NavLink>

        {["fullname", "email", "password", "confirm"].map((field, idx) => (
          <div key={idx} className="input-line">
            <input
              type={field.includes("password") || field.includes("confirm") ? "password" : "text"}
              placeholder={field === "fullname" ? "Tên hiển thị" : field === "email" ? "Email" : field === "password" ? "Mật khẩu" : "Nhập lại mật khẩu"}
              name={field}
              onChange={handleChange}
              value={input[field]}
              className={errors[field] ? "input-error" : ""}
            />
            {errors[field] && <div className="error-message">{errors[field]}</div>}
          </div>
        ))}

        <div className="input-line">
          <button className="register-btn" type="submit">
            {loading ? <Loading size={24} /> : "Tạo tài khoản"}
          </button>
        </div>

        <div className="input-line or-name">
          <span></span>
          <div>HOẶC</div>
          <span></span>
        </div>

        <div className="input-line register-direct">
          <p>Đã có tài khoản?</p>
          <Link to="/auth/login">Đăng nhập</Link>
        </div>
      </form>
    </div>
  );
};

export default Register;


// import { Link, NavLink, useNavigate } from "react-router-dom";
// import Logo from "../../components/Logo/Logo";
// import { useState } from "react";
// import { toast } from "react-toastify";
// import "./style.scss";
// import authService from "../../services/authService";
// import Loading from "../../components/Loading/Loading";

// const Register = () => {
//   const navigate = useNavigate();
//   const [loading, setLoading] = useState(false);
//   const [input, setInput] = useState({
//     email: "",
//     fullname: "",
//     password: "",
//     confirm: "",
//   });
//   const [errors, setErrors] = useState({
//     email: "",
//     fullname: "",
//     password: "",
//     confirm: "",
//   });

//   const handleChange = (e) => {
//     setInput({
//       ...input,
//       [e.target.name]: e.target.value,
//     });
//     setErrors({
//       ...errors,
//       [e.target.name]: "",
//     });
//   };

//   const validate = () => {
//     const newErrors = {};
//     const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
//     if (!input.fullname) {
//       newErrors.fullname = "Tên hiển thị không được để trống";
//     } else if (input.fullname.length > 20) {
//       newErrors.fullname = "Tên hiển thị không quá 20 ký tự";
//     }
//     if (!input.email) {
//       newErrors.email = "Email không được để trống";
//     } else if (input.email.length > 50) {
//       newErrors.email = "Email không quá 50 ký tự";
//     } else if (!emailRegex.test(input.email)) {
//       newErrors.email = "Email không hợp lệ";
//     }
//     if (!input.password) {
//       newErrors.password = "Mật khẩu không được để trống";
//     } else if (input.password.length < 6 || input.password.length > 50) {
//       newErrors.password = "Mật khẩu phải có ít nhất 6 ký tự và không quá 50 ký tự";
//     }
//     if (!input.confirm) {
//       newErrors.confirm = "Nhập lại mật khẩu không được để trống";
//     } else if (input.password !== input.confirm) {
//       newErrors.confirm = "Mật khẩu không khớp";
//     }
//     setErrors(newErrors);
//     return Object.keys(newErrors).length === 0;
//   };

//   const handleSubmit = async (e) => {
//     e.preventDefault();
//     setLoading(true);
//     if (validate()) {
//       try {
//         const { email, fullname, password } = input;
//         await authService.register(email, password, fullname);
//         toast.success("Đăng ký thành công", { theme: "colored" });
//         navigate("/auth/login");
//       } catch (error) {
//         toast.error(error.response?.data?.message || "Lỗi hệ thống", { theme: "colored" });
//       }
//     }
//     setLoading(false);
//   };

//   return (
//     <div className="register-container">
//       <form className="register-box" onSubmit={handleSubmit}>
//         <NavLink to={"/"} className="register-logo">
//           <Logo />
//         </NavLink>

//         <div className="input-line">
//           <input
//             type="text"
//             placeholder="Tên hiển thị"
//             name="fullname"
//             onChange={handleChange}
//             value={input.fullname}
//             className={errors.fullname ? "input-error" : ""}
//           />
//           {errors.fullname && <div className="error-message">{errors.fullname}</div>}
//         </div>
//         <div className="input-line">
//           <input
//             type="email"
//             placeholder="Email"
//             name="email"
//             onChange={handleChange}
//             value={input.email}
//             className={errors.email ? "input-error" : ""}
//           />
//           {errors.email && <div className="error-message">{errors.email}</div>}
//         </div>
//         <div className="input-line">
//           <input
//             type="password"
//             placeholder="Mật khẩu"
//             name="password"
//             onChange={handleChange}
//             value={input.password}
//             className={errors.password ? "input-error" : ""}
//           />
//           {errors.password && <div className="error-message">{errors.password}</div>}
//         </div>
//         <div className="input-line">
//           <input
//             type="password"
//             placeholder="Nhập lại mật khẩu"
//             name="confirm"
//             onChange={handleChange}
//             value={input.confirm}
//             className={errors.confirm ? "input-error" : ""}
//           />
//           {errors.confirm && <div className="error-message">{errors.confirm}</div>}
//         </div>
//         <div className="input-line">
//           <button className="register-btn" type="submit">
//             {loading ? <Loading size={24} /> : "Tạo tài khoản"}
//           </button>
//         </div>

//         <div className="input-line or-name">
//           <span></span>
//           <div>HOẶC</div>
//           <span></span>
//         </div>

//         <div className="input-line register-direct">
//           <p>Đã có tài khoản?</p>
//           <Link to="/auth/login">Đăng nhập</Link>
//         </div>
//       </form>
//     </div>
//   );
// };

// export default Register;
