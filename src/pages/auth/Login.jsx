import { Link, NavLink } from "react-router-dom";
import Logo from "../../components/Logo/Logo";
import Loading from "../../components/Loading/Loading";

import { useEffect, useState } from "react";

import "./style.scss";
import authService from "../../services/authService";
import { LoginAction } from "../../redux/auth";
import { useDispatch } from "react-redux";
import { toast } from "react-toastify";
const Login = () => {
  const dispatch = useDispatch();

  const [loading, setLoading] = useState(false);
  const [input, setInput] = useState({
    email: "",
    password: "",
  });

  useEffect(() => {
    document.title = "Đăng nhập";
  }, []);

  const handleChange = (e) => {
    setInput({
      ...input,
      [e.target.name]: e.target.value,
    });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    setLoading(true);
    try {
      const { email, password } = input;
      const { data } = await authService.login(email, password);
      // localStorage.setItem("user", JSON.stringify(data.user));
      dispatch(LoginAction({ token: data.token, user: data.user }));
      if (data?.user.role === "ADMIN") {
        window.location.href = "/admin/";
      } else {
        window.location.href = "/garden";
      }
    } catch (error) {
      console.log(error);
      const msg =
        error.response?.data?.message || error.response?.data || "Lỗi hệ thống";
      toast.error(msg, { autoClose: 1500 });
    }
    setLoading(false);
  };
  return (
    <div className="login-container">
      <form className="login-box" onSubmit={handleSubmit}>
        <NavLink to={"/"} className="login-logo">
          <Logo />
        </NavLink>
        <div className="input-line">
          <input
            type="email"
            placeholder="Email"
            required
            name="email"
            value={input.email}
            onChange={handleChange}
          />
        </div>
        <div className="input-line">
          <input
            type="password"
            placeholder="Mật khẩu"
            required
            name="password"
            value={input.password}
            onChange={handleChange}
          />
        </div>
        <div className="input-line">
          <button className="login-btn">
            {loading ? <Loading size={24} /> : "Đăng nhập"}
          </button>
        </div>

        <div className="input-line or-name">
          <span></span>
          <div>HOẶC</div>
          <span></span>
        </div>

        <div className="input-line register-direct">
          <p>Bạn chưa có tài khoản ư?</p>
          <Link to="/auth/register">Đăng ký</Link>
        </div>
      </form>
    </div>
  );
};

export default Login;
