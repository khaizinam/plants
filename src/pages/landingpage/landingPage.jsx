// import { Link } from "react-router-dom";
import { Navigate, useNavigate } from "react-router-dom";
import "./landingPage.scss";
import { useSelector } from "react-redux";

import Logo from "../../components/Logo/Logo";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFacebookSquare } from "@fortawesome/free-brands-svg-icons";

import LeftTop from "./lefttop.jpeg";
import LeftBottom from "./leftbottom.jpg";
import Right from "./right.jpeg";
import { faArrowRight } from "@fortawesome/free-solid-svg-icons";

export default function LandingPage() {
  const { user } = useSelector((state) => state.Auth);

  const navigate = useNavigate();

  if (user) {
    const isAdmin = user && user.role === "ADMIN";
    if (isAdmin) {
      return <Navigate to="/admin" />;
    } else {
      return <Navigate to="/garden" />;
    }
  } else {
    return (
      <div className="landing-page-container">
        <div className="background-layer">
          <div className="wr">
            <div className="header">
              <Logo />
              <div className="contact">
                <a
                  href="https://www.facebook.com/undefined.200114071004"
                  target="_blank"
                  className="fb-icon"
                  rel="noreferrer"
                >
                  <FontAwesomeIcon icon={faFacebookSquare} />
                </a>
              </div>
            </div>
            <div className="body">
              <div className="left">
                <div className="quote-container">
                  <div className="quote">"XANH HƠN MỖI NGÀY"</div>
                  <div className="description">
                    <p>
                      Plants.com là ứng dụng hỗ trợ trồng cây hoàn hảo dành cho
                      những người yêu thiên nhiên và mong muốn góp phần bảo vệ
                      môi trường.
                    </p>
                    <p>
                      Chúng tôi mang đến các giải pháp thông minh giúp bạn biến
                      không gian sống trở nên xanh tươi, bền vững hơn.
                    </p>
                    <p>
                      Hãy cùng Plants.com gieo mầm yêu thương và gặt hái tương
                      lai xanh!
                    </p>
                  </div>
                  <div className="actions">
                    <div
                      className="login-btn"
                      onClick={() => navigate("/auth")}
                    >
                      <span>Tham gia</span>
                      <FontAwesomeIcon icon={faArrowRight} className="arrow" />
                    </div>
                  </div>
                </div>
              </div>
              <div className="right">
                <div className="image-container">
                  <div className="image-left">
                    <div className="top">
                      <img src={LeftTop} alt="" />
                    </div>
                    <img src={LeftBottom} alt="" className="bottom" />
                  </div>
                  <div className="image-right">
                    <img src={Right} alt="" />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
