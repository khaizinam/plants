import "./ReminderModal.scss";

import { useEffect, useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faRepeat, faTag, faXmark } from "@fortawesome/free-solid-svg-icons";

import { useDispatch } from "react-redux";

import {
  faCalendar,
  faClock,
  faNoteSticky,
} from "@fortawesome/free-regular-svg-icons";
import {
  Badge,
  DatePicker,
  DateRangePicker,
  Dropdown,
  Modal,
  Tag,
  TagPicker,
} from "rsuite";
import gardenService from "../../../../services/gardenService";
import reminderService from "../../../../services/reminderService";
import { GetReminder } from "../../../../redux/reminder";
import { toast } from "react-toastify";
import { TREE_STATUS } from "../../../../utils/utils";

const defaultReminder = {
  loop: null,
  date_start: new Date(new Date().setHours(0, 0, 0)),
  date_end: new Date(new Date().setHours(23, 59, 59)),
  time: new Date(2023, 1, 1, 0, 0),
  note: "",
  trees: [],
};

const loopOptions = [
  {
    value: null,
    label: "Không",
  },
  {
    value: "Ngày",
    label: "Ngày",
  },
  {
    value: "Tuần",
    label: "Tuần",
  },
  {
    value: "Tháng",
    label: "Tháng",
  },
];

const getColorStatus = (status) => {
  switch (status) {
    case 0:
      return "green";
    case 1:
      return "yellow";
    case 2:
      return "cyan";
    case 3:
      return "red";
    default:
      return "green";
  }
};

const { beforeToday } = DateRangePicker;

const ReminderModal = ({ onClose, show }) => {
  const dispatch = useDispatch();

  const [reminder, setReminder] = useState(defaultReminder);

  const [loading, setLoading] = useState(false);
  const [tagOptions, setTagOptions] = useState([]);

  const handleChange = (name, value) =>
    setReminder((prev) => ({
      ...prev,
      [name]: value,
    }));

  const renderTags = (values, items) => {
    const removeTag = (tag) => {
      const nextTags = values.filter((x) => x !== tag);
      handleChange("trees", nextTags);
    };

    return (
      <>
        {items.map((item, key) => (
          <Tag
            key={key}
            color={`${getColorStatus(item.status)}`}
            closable
            onClose={() => removeTag(item.value)}
          >
            {item.label}
          </Tag>
        ))}
      </>
    );
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    setLoading(true);
    try {
      if (reminder.note && reminder.time) {
        const { data } = await reminderService.createReminder(reminder);
        dispatch(GetReminder(data));
        onClose();
        toast.success("Đã thêm nhắc nhở");
        setReminder(defaultReminder);
      } else {
        toast.warning("Vui lòng điền đầy đủ thông tin");
      }
    } catch (error) {}
    setLoading(false);
  };

  useEffect(() => {
    const fetchGarden = async () => {
      try {
        const { data } = await gardenService.getGarden();
        const newTrees = [];
        data.forEach((area) => {
          area.trees.forEach((tree) => {
            newTrees.push({
              ...tree,
              label: tree.name,
              value: tree.id,
              area: area.name,
            });
          });
        });
        setTagOptions(newTrees);
      } catch (error) {}
    };
    fetchGarden();
  }, []);

  const disableDateEnd = (date) => {
    const startDate = reminder.date_start;
    if (date.getTime() - startDate < 0) return true;
    if (reminder.loop === "Tuần") {
      if (date.getDay() !== startDate.getDay()) return true;
    } else if (reminder.loop === "Tháng") {
      if (date.getDate() !== startDate.getDate()) return true;
    }
    return false;
  };

  const changeStartDate = (date) => {
    date.setHours(0, 0, 0);
    handleChange("date_start", date);
    const date_end = date;
    date_end.setHours(23, 59, 59);
    handleChange("date_end", date_end);
  };

  return (
    <Modal open={show} onClose={onClose} className="reminder-modal">
      <form className="reminder-modal-box" onSubmit={handleSubmit}>
        <div className="reminder-modal-header">
          Thêm nhắc nhở
          <div className="close-btn" onClick={onClose}>
            <FontAwesomeIcon icon={faXmark} />
          </div>
        </div>
        <div className="reminder-modal-body">
          <div className="reminder-info-list">
            <div className="reminder-info">
              <div className="title">
                <FontAwesomeIcon icon={faNoteSticky} className="icon-title" />
                Công việc: (*)
              </div>
              <div className="value">
                <textarea
                  spellCheck="false"
                  className="reminder-info-description"
                  rows={1}
                  onChange={(e) => handleChange("note", e.target.value)}
                  placeholder="Nội dung ..."
                  value={reminder.note}
                ></textarea>
              </div>
            </div>
            <div className="reminder-info">
              <div className="title">
                <FontAwesomeIcon icon={faRepeat} className="icon-title" />
                Lặp: (*)
              </div>
              <div className="value">
                <Dropdown
                  title={
                    loopOptions.find((l) => l.value === reminder.loop).label
                  }
                  onSelect={(v) => {
                    handleChange("loop", v);
                    const date_end = reminder.date_start;
                    date_end.setHours(23, 59, 59);
                    handleChange("date_end", date_end);
                  }}
                  placement="bottomEnd"
                >
                  {loopOptions.map((item, i) => (
                    <Dropdown.Item
                      key={i}
                      active={reminder.loop === item.value}
                      eventKey={item.value}
                    >
                      {item.label}
                    </Dropdown.Item>
                  ))}
                </Dropdown>
              </div>
            </div>
            <div className="reminder-info">
              <div className="title">
                <FontAwesomeIcon icon={faCalendar} className="icon-title" />
                {!reminder.loop ? "Ngày" : "Ngày bắt đầu"}: (*)
              </div>
              <div className="value">
                <DatePicker
                  oneTap
                  isoWeek
                  className={`mode-btn ${reminder.date ? "active" : ""}`}
                  format="dd/MM/yyyy"
                  onChange={changeStartDate}
                  value={reminder.date_start}
                  shouldDisableDate={beforeToday()}
                />
              </div>
            </div>

            {reminder.loop && (
              <div className="reminder-info">
                <div className="title">
                  <FontAwesomeIcon icon={faCalendar} className="icon-title" />
                  Ngày cuối cùng:
                </div>
                <div className="value">
                  <DatePicker
                    oneTap
                    isoWeek
                    className={`mode-btn ${reminder.date ? "active" : ""}`}
                    format="dd/MM/yyyy"
                    onChange={(date) => handleChange("date_end", date)}
                    value={reminder.date_end}
                    shouldDisableDate={disableDateEnd}
                  />
                </div>
              </div>
            )}

            <div className="reminder-info">
              <div className="title">
                <FontAwesomeIcon icon={faClock} className="icon-title" />
                Thời gian: (*)
              </div>
              <div className="value">
                <DatePicker
                  className="tpick"
                  format="HH:mm"
                  locale={{ hours: "Giờ", minutes: "Phút" }}
                  value={reminder.time}
                  onChange={(time) => handleChange("time", time)}
                  placement="left"
                />
                {/* <input type="time" /> */}
              </div>
            </div>

            <div className="reminder-info">
              <div className="title">
                <FontAwesomeIcon icon={faTag} className="icon-title" />
                Các cây
              </div>
              <div className="value"></div>
            </div>
            <div>
              <TagPicker
                data={tagOptions}
                className="tag-picker"
                groupBy="area"
                placeholder={"Chọn cây"}
                block
                onChange={(v) => handleChange("trees", v)}
                value={reminder.trees}
                renderValue={renderTags}
                renderMenuItem={(label, item) => {
                  return (
                    <small>
                      <Badge color={getColorStatus(item.status)} /> {label}
                      {TREE_STATUS[item.status] != null && (
                        <> - ({TREE_STATUS[item.status]})</>
                      )}
                    </small>
                  );
                }}
              />
              {/* <TagPicker
                data={tagOptions}
                className="tag-picker"
                groupBy="area"
                placeholder={"Chọn cây"}
                block
                // sort={(isGroup) => {
                //   if (isGroup) {
                //     return (a, b) => {
                //       return a.groupTitle > b.groupTitle;
                //     };
                //   }

                //   return (a, b) => {
                //     return a.value > b.value;
                //   };
                // }}
                onChange={(v) => handleChange("trees", v)}
                value={reminder.trees}
                tagProps={{ color: "blue", size: "lg" }}
              /> */}
            </div>
          </div>
        </div>
        <div className="reminder-modal-footer">
          <button className="submit-btn btn">{loading ? "" : "Thêm"}</button>
        </div>
      </form>
    </Modal>
  );
};

export default ReminderModal;
