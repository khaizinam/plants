import "./ReminderInfo.scss";

import { useEffect, useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faRepeat, faTag, faXmark } from "@fortawesome/free-solid-svg-icons";

import { faCalendar, faClock } from "@fortawesome/free-regular-svg-icons";
import { Badge, DatePicker, Input, Modal, Tag, TagPicker } from "rsuite";
import gardenService from "../../../../services/gardenService";

import { TREE_STATUS } from "../../../../utils/utils";

const separateTime = (time) => {
  return time.split(":");
};

const t = (time) => {
  const [hours, minutes] = separateTime(time);
  const newTime = new Date();
  newTime.setHours(hours, minutes, 0);
  return newTime;
};

const getColorStatus = (status) => {
  switch (status) {
    case 0:
      return "green";
    case 1:
      return "yellow";
    case 2:
      return "cyan";
    case 3:
      return "red";
    default:
      return "green";
  }
};

const ReminderInfo = ({ onClose, open, data, disabled }) => {
  const [reminder, setReminder] = useState({
    ...data,
    date_start: new Date(data.date_start),
    date_end: data.date_end ? new Date(data.date_end) : null,
    time: t(data.time),
    trees: data.trees.map((x) => x.id),
  });

  const [tagOptions, setTagOptions] = useState([]);

  const handleChange = (name, value) =>
    setReminder((prev) => ({
      ...prev,
      [name]: value,
    }));

  useEffect(() => {
    const fetchGarden = async () => {
      try {
        const { data } = await gardenService.getGarden();
        const newTrees = [];
        data.forEach((area) => {
          area.trees.forEach((tree) => {
            newTrees.push({
              ...tree,
              label: tree.name,
              value: tree.id,
              area: area.name,
            });
          });
        });
        setTagOptions(newTrees);
      } catch (error) {}
    };
    fetchGarden();
  }, []);

  const renderTags = (values, items) => {
    const removeTag = (tag) => {
      const nextTags = values.filter((x) => x !== tag);
      handleChange("trees", nextTags);
    };

    return (
      <>
        {items.map((item, key) => (
          <Tag
            key={key}
            color={`${getColorStatus(item.status)}`}
            onClose={() => removeTag(item.value)}
            closable={false}
          >
            {item.label}
          </Tag>
        ))}
      </>
    );
  };

  return (
    <Modal open={open} onClose={onClose} className="reminder-modal">
      <div className="reminder-modal-box" style={{ maxWidth: 400 }}>
        <div className="reminder-modal-header">
          {reminder.note}
          <div className="close-btn" onClick={onClose}>
            <FontAwesomeIcon icon={faXmark} />
          </div>
        </div>
        <div className="reminder-modal-body">
          <div className="reminder-info-list">
            <div className="reminder-info">
              <div className="title">
                <FontAwesomeIcon icon={faRepeat} className="icon-title" />
                Lặp: (*)
              </div>
              <div className="value">
                <Input
                  plaintext
                  value={reminder.loop ? `Hằng ${reminder.loop}` : "Không"}
                />
              </div>
            </div>
            <div className="reminder-info">
              <div className="title">
                <FontAwesomeIcon icon={faCalendar} className="icon-title" />
                {!reminder.loop ? "Ngày" : "Ngày bắt đầu: (*)"}:
              </div>
              <div className="value">
                <DatePicker
                  oneTap
                  isoWeek
                  format="dd/MM/yyyy"
                  value={reminder.date_start}
                  plaintext
                  style={{ width: "min-content" }}
                  readOnly
                />
              </div>
            </div>

            {reminder.loop && (
              <div className="reminder-info">
                <div className="title">
                  <FontAwesomeIcon icon={faCalendar} className="icon-title" />
                  Ngày cuối cùng:
                </div>
                <div className="value">
                  <DatePicker
                    isoWeek
                    className={`mode-btn ${reminder.date ? "active" : ""}`}
                    format="dd/MM/yyyy"
                    value={reminder.date_end}
                    style={{ width: "min-content" }}
                    plaintext
                  />
                </div>
              </div>
            )}

            <div className="reminder-info">
              <div className="title">
                <FontAwesomeIcon icon={faClock} className="icon-title" />
                Thời gian: (*)
              </div>
              <div className="value">
                <DatePicker
                  className="tpick"
                  format="HH:mm"
                  value={reminder.time}
                  placement="left"
                  plaintext
                  readOnly
                  style={{ width: "min-content" }}
                />
              </div>
            </div>

            {reminder.trees.length > 0 && (
              <>
                <div className="reminder-info">
                  <div className="title">
                    <FontAwesomeIcon icon={faTag} className="icon-title" />
                    Các cây
                  </div>
                  <div className="value"></div>
                </div>
                <div>
                  <TagPicker
                    data={tagOptions}
                    className="tag-picker"
                    groupBy="area"
                    placeholder={"Chọn cây"}
                    block
                    cleanable={false}
                    readOnly
                    value={reminder.trees}
                    renderValue={renderTags}
                    renderMenuItem={(label, item) => {
                      return (
                        <small>
                          <Badge color={getColorStatus(item.status)} /> {label}
                          {TREE_STATUS[item.status] != null && (
                            <> - ({TREE_STATUS[item.status]})</>
                          )}
                        </small>
                      );
                    }}
                  />
                </div>
              </>
            )}
          </div>
        </div>
      </div>
    </Modal>
  );
};

export default ReminderInfo;
