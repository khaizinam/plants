/* eslint-disable jsx-a11y/anchor-is-valid */
import "./ReminderPage.scss";
import { Badge, Button, Calendar, Modal, Popover, Whisper } from "rsuite";

import React, { useEffect, useState } from "react";
import PlusIcon from "../../icons/PlusIcon";
import ReminderModal from "./child/ReminderModal/ReminderModal";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faXmark } from "@fortawesome/free-solid-svg-icons";
import reminderService from "../../services/reminderService";
import { useDispatch, useSelector } from "react-redux";
import { GetReminder } from "../../redux/reminder";
import moment from "moment";
import ReminderEdit from "./child/ReminderEdit/ReminderEdit";
import ReminderInfo from "./child/ReminderInfo/ReminderInfo";

const getColorStatuss = (status) => {
  switch (status) {
    case 0:
      return "green";
    case 1:
      return "yellow";
    case 2:
      return "cyan";
    case 3:
      return "red";
    default:
      break;
  }
};

const customLocale = {
  sunday: "Sun",
  monday: "Mon",
  tuesday: "Tue",
  wednesday: "Wed",
  thursday: "Thu",
  friday: "Fri",
  saturday: "Sat",
};

function compareTimeWithString(timeString) {
  const [hours, minutes] = timeString.split(":").map(Number);
  const specifiedTime = new Date();
  specifiedTime.setHours(hours);
  specifiedTime.setMinutes(minutes);
  specifiedTime.setSeconds(0);

  const currentTime = new Date();

  if (specifiedTime > currentTime) {
    return 1;
  } else if (specifiedTime < currentTime) {
    return -1;
  } else {
    return 0;
  }
}

const subTime = (strTime) => {
  return strTime.substring(0, strTime.length - 3);
};

function compareTimes(time1, time2) {
  // Extract hours, minutes, and seconds from times
  const [hours1, minutes1] = time1.split(":").map(Number);
  const [hours2, minutes2] = time2.split(":").map(Number);

  // Compare hours
  if (hours1 < hours2) {
    return -1;
  } else if (hours1 > hours2) {
    return 1;
  }

  // Compare minutes if hours are equal
  if (minutes1 < minutes2) {
    return -1;
  } else if (minutes1 > minutes2) {
    return 1;
  }

  // Times are equal
  return 0;
}
const getLoopString = (reminder) => {
  if (!reminder.loop) return moment(reminder.date_start).format("DD/MM/yyyy");
  else if (reminder.loop === "Ngày") return "Mỗi ngày";
  else if (reminder.loop === "Tuần") {
    const day = new Date(reminder.date_start).getDay();
    const DayName = [
      "Thứ hai",
      "Thứ ba",
      "Thứ tư",
      "Thứ năm",
      "Thứ sáu",
      "Thứ bảy",
      "Chủ nhật",
    ];
    return `${DayName[day]} ${reminder.date_end ? "hằng tuần" : ""}`;
  } else if (reminder.loop === "Tháng") {
    const date = new Date(reminder.date_start).getDate();
    return `Ngày ${date} hằng tháng`;
  }
};

const ReminderItem = ({ reminder, disabled = false }) => {
  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const [edit, setEdit] = useState(false);
  const openEdit = () => setEdit(true);
  const closeEdit = () => setEdit(false);
  const dispatch = useDispatch();

  const handleDelete = async () => {
    try {
      const { data } = await reminderService.deleteReminder(reminder.id);
      dispatch(GetReminder(data));
      handleClose();
    } catch (error) {}
  };
  const { trees = [] } = reminder;

  const getColorStatus = () => {
    const max = trees.reduce((a, b) => (b.status > a ? b.status : a), 0);
    return getColorStatuss(max);
  };

  return (
    <div className="reminder-item ">
      <div className="left" onClick={openEdit}>
        <div className="note">
          <Badge
            style={{
              backgroundColor: `${getColorStatus()}`,
            }}
          />

          <Whisper
            placement="auto"
            trigger="hover"
            speaker={
              <Popover arrow={false}>
                <ul className="tree-list">
                  {trees.map((tree, index) => (
                    <li key={index} className="tree-info">
                      <img src={tree.plant.avatar} alt="img" />
                      {index + 1}. {tree.name}
                    </li>
                  ))}
                </ul>
              </Popover>
            }
          >
            <a
              style={{
                color: `${getColorStatus()}`,
              }}
            >
              {reminder.note}
            </a>
          </Whisper>
        </div>

        <div className="datetime">
          <div className="time">{subTime(reminder.time)}</div>
          <div>-</div>
          <div className="date">{getLoopString(reminder)}</div>
        </div>
      </div>
      {!disabled && (
        <div className="right">
          <div className="remove-btn" onClick={handleOpen}>
            <FontAwesomeIcon icon={faXmark} />
          </div>
        </div>
      )}

      <Modal open={open} onClose={handleClose}>
        <Modal.Header>
          <Modal.Title>Xác nhận</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <small>
            Bạn chắc chắn xóa "<strong>{reminder.note}</strong>"?
          </small>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={handleDelete} appearance="primary">
            Đồng ý
          </Button>
          <Button onClick={handleClose} appearance="subtle">
            Hủy
          </Button>
        </Modal.Footer>
      </Modal>

      {disabled ? (
        <ReminderInfo open={edit} onClose={closeEdit} data={reminder} />
      ) : (
        <ReminderEdit open={edit} onClose={closeEdit} data={reminder} />
      )}
    </div>
  );
};

const ReminderPage = () => {
  const dispatch = useDispatch();
  const reminders = useSelector((state) => state.Reminder);

  useEffect(() => {
    document.title = "Công việc";
  }, []);

  useEffect(() => {
    const fetchReminder = async () => {
      try {
        const { data } = await reminderService.getReminder();
        dispatch(GetReminder(data));
      } catch (error) {
        console.log(error);
      }
    };
    fetchReminder();
  }, [dispatch]);

  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const list_reminder = reminders
    .filter((reminder) => {
      const date_end = new Date(`${reminder.date_end} ${reminder.time}`);
      if (date_end.getTime() - new Date().getTime() < 0) return false;
      else return true;
    })
    .map((reminder) => {
      if (reminder.loop) {
        const next =
          compareTimeWithString(reminder.time) === 1
            ? moment(
                new Date(`${reminder.date_start} ${reminder.time}`)
              ).format("yyyy-MM-DD")
            : moment(new Date(`${reminder.date_start} ${reminder.time}`))
                .add(reminder.loop === "Ngày" ? 1 : 7, "days")
                .format("yyyy-MM-DD");
        return { ...reminder, next };
      } else return reminder;
    })
    .sort((a, b) => {
      const aDate = new Date(`${a.next} ${a.time}`);
      const bDate = new Date(`${b.next} ${b.time}`);
      return aDate - bDate;
    });

  const past_reminder = reminders
    .filter((reminder) => {
      const date_end = new Date(`${reminder.date_end} ${reminder.time}`);
      if (date_end.getTime() - new Date().getTime() < 0) return true;
      else return false;
    })
    .map((reminder) => {
      if (reminder.loop) {
        const next =
          compareTimeWithString(reminder.time) === 1
            ? moment(
                new Date(`${reminder.date_start} ${reminder.time}`)
              ).format("yyyy-MM-DD")
            : moment(new Date(`${reminder.date_start} ${reminder.time}`))
                .add(reminder.loop === "Ngày" ? 1 : 7, "days")
                .format("yyyy-MM-DD");
        return { ...reminder, next };
      } else return reminder;
    })
    .sort((a, b) => {
      const aDate = new Date(`${a.next} ${a.time}`);
      const bDate = new Date(`${b.next} ${b.time}`);
      return aDate - bDate;
    });

  const getTodoList = (date) => {
    let todolist = [];
    reminders.forEach((reminder) => {
      if (
        new Date(date) >= new Date(reminder.date_start).setHours(0, 0, 0, 0) &&
        new Date(date) <= new Date(reminder.date_end).setHours(23, 59, 59, 0)
      ) {
        const { trees = [] } = reminder;
        todolist.push({
          color: trees.reduce((a, b) => (b.status > a ? b.status : a), 0),
          time: subTime(reminder.time),
          title: reminder.note,
          loop: reminder.loop,
        });
      }
    });

    todolist = todolist.sort((a, b) => compareTimes(a.time, b.time));
    return todolist;
  };

  const renderCell = (date) => {
    const list = getTodoList(date);
    const displayList = list.filter((item, index) => index < 2);

    if (list.length) {
      const moreCount = list.length - displayList.length;

      return (
        <Whisper
          placement="auto"
          trigger="hover"
          speaker={
            <Popover>
              {list.map((item, index) => (
                <div key={index}>
                  <b>{item.time}</b> - {item.title}
                </div>
              ))}
            </Popover>
          }
        >
          <ul className="calendar-todo-list">
            {displayList && (
              <>
                {displayList.map((item, index) => (
                  <li key={index} className="text-calendar">
                    <Badge color={getColorStatuss(item.color)} />{" "}
                    <b className="reminder-time">{item.time} - </b>
                    {item.title}
                  </li>
                ))}

                {moreCount ? (
                  <li className="text-more">{moreCount} more</li>
                ) : (
                  ""
                )}
                <div className="replace">
                  <Badge content={list.length === 1 ? null : list.length} />
                </div>
              </>
            )}
          </ul>
        </Whisper>
      );
    }

    return null;
  };

  return (
    <div className="reminder-page">
      <div className="calendar-container">
        <Calendar
          bordered
          renderCell={renderCell}
          className="custom-calendar"
          locale={customLocale}
          isoWeek
        />
      </div>
      <div className="reminder-container">
        <button className="btn-add-post" onClick={handleOpen}>
          <PlusIcon />
        </button>
        <div className="reminder-box">
          <div className="remind-title">Sắp tới</div>
          <div className="list-reminder">
            {list_reminder.length > 0 ? (
              list_reminder.map((reminder) => (
                <ReminderItem key={reminder.id} reminder={reminder} />
              ))
            ) : (
              <small>Không có nhắc nhở</small>
            )}
          </div>
        </div>

        {past_reminder.length > 0 && (
          <div className="reminder-box" style={{ marginTop: 20 }}>
            <div className="remind-title">Đã qua</div>
            <div className="list-reminder">
              {past_reminder.map((reminder) => (
                <ReminderItem key={reminder.id} reminder={reminder} disabled />
              ))}
            </div>
          </div>
        )}
      </div>
      <ReminderModal show={open} onClose={handleClose} />
    </div>
  );
};

export default ReminderPage;
