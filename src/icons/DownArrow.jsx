const DownArrow = ({ down = true }) => {
  return (
    <svg
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d={down ? "M5 7.5L10 12.5L15 7.5" : "M15 12.5L10 7.5L5 12.5"}
        stroke="#F9F9F9"
        strokeWidth="4"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};

export default DownArrow;
