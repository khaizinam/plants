const DeletePhaseIcon = ({ size = 30, color = "currentColor" }) => {
  return (
    <svg
      width={size}
      height={size}
      viewBox={`0 0 ${size} ${size}`}
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M15 26.25C21.2132 26.25 26.25 21.2132 26.25 15C26.25 8.7868 21.2132 3.75 15 3.75C8.7868 3.75 3.75 8.7868 3.75 15C3.75 21.2132 8.7868 26.25 15 26.25Z"
        stroke={color}
        strokeWidth="1.25"
      />
      <path d="M9.375 15H20.625" stroke={color} strokeWidth="1.25" />
    </svg>
  );
};

export default DeletePhaseIcon;
