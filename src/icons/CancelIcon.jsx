const CancelIcon = ({ size = 24 }) => {
  return (
    <svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M13 23.8333C18.9831 23.8333 23.8333 18.983 23.8333 13C23.8333 7.01687 18.9831 2.16663 13 2.16663C7.01691 2.16663 2.16667 7.01687 2.16667 13C2.16667 18.983 7.01691 23.8333 13 23.8333Z" stroke="white" strokeLinecap="round" strokeLinejoin="round" />
      <path d="M16.25 9.75L9.75 16.25" stroke="white" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
      <path d="M9.75 9.75L16.25 16.25" stroke="white" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
    </svg>

  );
};

export default CancelIcon;
