const StatisticIcon = ({ size = 30, color = "currentColor" }) => {
  return (
    <svg
      width={size}
      height={size}
      viewBox={`0 0 ${size} ${size}`}
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g clipPath="url(#clip0_1023_2254)">
        <path
          d="M24.375 1.875H5.625C3.55393 1.875 1.875 3.55393 1.875 5.625V24.375C1.875 26.4461 3.55393 28.125 5.625 28.125H24.375C26.4461 28.125 28.125 26.4461 28.125 24.375V5.625C28.125 3.55393 26.4461 1.875 24.375 1.875Z"
          stroke={color}
          strokeWidth="1.875"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        <path
          d="M18.75 22.5V13.125"
          stroke={color}
          strokeWidth="1.875"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        <path
          d="M22.5 22.5V7.5"
          stroke={color}
          strokeWidth="1.875"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        <path
          d="M7.5 22.5V18.75"
          stroke={color}
          strokeWidth="1.875"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        <path
          d="M11.25 15V22.5"
          stroke={color}
          strokeWidth="1.875"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        <path
          d="M15 16.875V22.5"
          stroke={color}
          strokeWidth="1.875"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
      </g>
      <defs>
        <clipPath id="clip0_1023_2254">
          <rect width={size} height={size} fill="white" />
        </clipPath>
      </defs>
    </svg>
  );
};

export default StatisticIcon;
