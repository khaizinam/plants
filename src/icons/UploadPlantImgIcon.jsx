const UploadPlantImgIcon = ({ size = 32 }) => {
  return (
    <svg
      width={size}
      height={size}
      viewBox={`0 0 ${size} ${size}`}
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M21 9.33333H21.0167M16.8333 31H6C4.67392 31 3.40215 30.4732 2.46447 29.5355C1.52678 28.5979 1 27.3261 1 26V6C1 4.67392 1.52678 3.40215 2.46447 2.46447C3.40215 1.52678 4.67392 1 6 1H26C27.3261 1 28.5979 1.52678 29.5355 2.46447C30.4732 3.40215 31 4.67392 31 6V16.8333"
        stroke="#0C9869"
        strokeWidth="1.66667"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M1 22.6669L9.33333 14.3335C10.88 12.8452 12.7867 12.8452 14.3333 14.3335L21 21.0002"
        stroke="#0C9869"
        strokeWidth="1.66667"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M19.3333 19.3333L20.9999 17.6666C22.1166 16.5933 23.4166 16.2933 24.6366 16.7666M22.6666 27.6666H32.6666M27.6666 22.6666V32.6666"
        stroke="#0C9869"
        strokeWidth="1.66667"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};

export default UploadPlantImgIcon;
