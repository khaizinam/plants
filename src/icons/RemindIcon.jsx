const RemindIcon = ({ size = 30, color = "currentColor" }) => {
  return (
    <svg
      width={size}
      height={size}
      viewBox={`0 0 ${size} ${size}`}
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M16.875 26.25C16.875 26.4962 16.8265 26.74 16.7323 26.9675C16.638 27.195 16.4999 27.4017 16.3258 27.5758C16.1517 27.7499 15.945 27.888 15.7175 27.9823C15.49 28.0765 15.2462 28.125 15 28.125"
        stroke={color}
        strokeWidth="1.87485"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M13.125 26.25C13.125 26.4962 13.1735 26.74 13.2677 26.9675C13.362 27.195 13.5001 27.4017 13.6742 27.5758C13.8483 27.7499 14.055 27.888 14.2825 27.9823C14.51 28.0765 14.7538 28.125 15 28.125"
        stroke={color}
        strokeWidth="1.87485"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M13.125 24.3751V26.2501"
        stroke={color}
        strokeWidth="1.87485"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M16.875 24.3751L16.875 26.2501"
        stroke={color}
        strokeWidth="1.87485"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M26.25 22.5C22.5 24.375 16.875 24.375 15 24.375"
        stroke={color}
        strokeWidth="1.87485"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M26.25 22.5C26.25 18.75 22.5 20.625 22.5 11.25C22.5 7.05735 18.75 3.75012 16.875 3.75012"
        stroke={color}
        strokeWidth="1.87485"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M3.75 22.5C7.5 24.375 13.125 24.375 15 24.375"
        stroke={color}
        strokeWidth="1.87485"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M3.75 22.4999C3.75 18.7499 7.5 20.6249 7.5 11.2499C7.5 7.05723 11.25 3.75 13.125 3.75"
        stroke={color}
        strokeWidth="1.87485"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M13.125 3.75C13.125 3.25272 13.3225 2.7758 13.6742 2.42417C14.0258 2.07254 14.5027 1.875 15 1.875"
        stroke={color}
        strokeWidth="1.87485"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M16.875 3.75C16.875 3.25272 16.6775 2.7758 16.3258 2.42417C15.9742 2.07254 15.4973 1.875 15 1.875"
        stroke={color}
        strokeWidth="1.87485"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};

export default RemindIcon;
