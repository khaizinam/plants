const initState = { 
    "tree": {
        "id": 1,
        "name": "",
        "status": 1,
        "plant_id": 1,
        "area_id": null,
        "description": ""
    },
    "diaries" : []
};

function TreeReducer (state = initState, action) {
  const { type, payload } = action;
  switch (type) {
    case "SAVE_TREE":
        console.log("redux [tree]-SAVE_TREE:");
        return payload;
    default:
        return state;
  }
};

export const SaveTree = (tree) => (dispatch) => {
    dispatch({ type: "SAVE_TREE", payload: tree });
};

export default TreeReducer;
