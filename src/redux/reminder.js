const initState = [];

const ReminderReducer = (state = initState, action) => {
  const { type, payload, id } = action;
  switch (type) {
    case "GET_REMINDER":
      return [...payload];
    case "UPDATE_REMINDER":
      return state.map((reminder) => {
        if (reminder.id === id) return payload;
        else return reminder;
      });
    default:
      return state;
  }
};

export const GetReminder = (data) => (dispatch) => {
  dispatch({ type: "GET_REMINDER", payload: data });
};

export const UpdateReminder = (id, payload) => (dispatch) => {
  dispatch({ type: "UPDATE_REMINDER", payload: payload, id });
};

export default ReminderReducer;
