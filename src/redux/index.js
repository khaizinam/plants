import { combineReducers } from "redux";
import { configureStore } from "@reduxjs/toolkit";

import Cart from "./cart";
import Auth from "./auth";
import Plant from "./plant";
import Garden from "./garden";
import Reminder from "./reminder";
import Tree from "./tree";

const rootReducers = combineReducers({
  Cart,
  Auth,
  Plant,
  Garden,
  Reminder,
  Tree
});

const store = configureStore({
  reducer: rootReducers,
});

export default store;
