const cart = [];
export const addCart = (product) => {
  return {
    type: "ADDITEM",
    payload: product,
  };
};

export const clearCart = (product) => {
  return {
    type: "CLEARCART",
    payload: product,
  };
};

const Cart = (state = cart, action) => {
  const product = action.payload;
  switch (action.type) {
    case "ADDITEM":
      return [...state, product];

    case "CLEARCART":
      state = [];
      return state;
    default:
      return state;
  }
};

export default Cart;
