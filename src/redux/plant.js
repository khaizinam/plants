const initState = { stages: [], posts: [] };

const PlantReducer = (state = initState, action) => {
  const { type, payload } = action;
  switch (type) {
    case "GET_PLANT":
      return { ...payload };
    case "UPDATED_STAGES":
      return { ...state, stages: payload };
    case "UPDATED_FEED_BACK":
      return { ...state, feedbacks: [payload, ...state.feedbacks] };
    default:
      return state;
  }
};

export const GetPlant = (plant) => (dispatch) => {
  dispatch({ type: "GET_PLANT", payload: plant });
};

export const UpdateStages = (stages) => (dispatch) => {
  dispatch({ type: "UPDATED_STAGES", payload: stages });
};

export const UpdateFeedBack = (feedback) => (dispatch) => {
  dispatch({ type: "UPDATED_FEED_BACK", payload: feedback });
};

export default PlantReducer;
