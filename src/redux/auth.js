const initState = {
  token: localStorage.getItem("token") || "",
  user: JSON.parse(localStorage.getItem("user")) || null,
};

const AuthReducer = (state = initState, action) => {
  const { type, payload } = action;
  switch (type) {
    case "LOGIN":
      localStorage.setItem("token", payload.token);
      localStorage.setItem("user", JSON.stringify(payload.user));
      return { token: payload.token, user: payload.user };
    case "LOGOUT":
      localStorage.removeItem("token");
      localStorage.removeItem("user");
      state = { token: "", user: null };
      return state;
    case "SET_USER":
      localStorage.setItem("token", payload.token);
      localStorage.setItem("user", JSON.stringify(payload.user));
      return { token: payload.token, user: payload.user };
    case "UPDATE_USER":
      localStorage.setItem("user", JSON.stringify(payload));
      return { ...state, user: payload };
    default:
      return state;
  }
};

export const LoginAction = (payload) => (dispatch) => {
  dispatch({ type: "LOGIN", payload: payload });
};

export const LogoutAction = () => (dispatch) => {
  dispatch({ type: "LOGOUT" });
};

export const SetUserAction = (user) => (dispatch) => {
  dispatch({ type: "SET_USER", payload: user });
};

export const UpdateUserAction = (user) => (dispatch) => {
  dispatch({ type: "UPDATE_USER", payload: user });
};

export default AuthReducer;
