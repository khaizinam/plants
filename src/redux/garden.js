const initState = [];

const GardenReducer = (state = initState, action) => {
  const { type, payload } = action;
  let newAreas;
  switch (type) {
    case "GET_GARDEN":
      return payload;
    case "ADD_TREE":
      const { area_id } = payload;
      newAreas = state.map((area) => {
        if ((area.id == null && area_id === null) | (area_id === area.id)) {
          return { ...area, trees: [...area.trees, payload] };
        } else return area;
      });
      return newAreas;
    case "DELETE_TREE":
      const treeId = payload;
      newAreas = state.map((area) => {
        const newTrees = area.trees.filter((tree) => tree.id !== treeId);
        return { ...area, trees: newTrees };
      });
      return newAreas;
    case "UPDATE_TREE":
      newAreas = state.map((area) => {
        const newTrees = area.trees.map((tree) => {
          if (tree.id === payload.id) {
            return {
              ...tree,
              name: payload.name,
              description: payload.description,
            };
          } else return tree;
        });
        return { ...area, trees: newTrees };
      });
      return newAreas;

    case "UPDATE_AREA":
      newAreas = state.map((area) => {
        if (area.id === payload.id) {
          return { ...area, name: payload.name };
        } else {
          return area;
        }
      });
      return newAreas;
    case "CREATE_AREA":
      let newGarden = [...state];
      newGarden.splice(newGarden.length - 1, 0, { ...payload, trees: [] });
      return newGarden;
    default:
      return state;
  }
};

export const GetGarden = (listAreas) => (dispatch) => {
  dispatch({ type: "GET_GARDEN", payload: listAreas });
};

export const AddTree = (newTree) => (dispatch) => {
  dispatch({ type: "ADD_TREE", payload: newTree });
};

export const CreateArea = (area) => (dispatch) => {
  dispatch({ type: "CREATE_AREA", payload: area });
};

export const UpdateTree = (tree) => (dispatch) => {
  dispatch({ type: "UPDATE_TREE", payload: tree });
};

export const UpdateArea = (area) => (dispatch) => {
  dispatch({ type: "UPDATE_AREA", payload: area });
};

export const DeleteTree = (treeId) => (dispatch) => {
  dispatch({ type: "DELETE_TREE", payload: treeId });
};

export default GardenReducer;
