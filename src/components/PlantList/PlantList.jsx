// /* eslint-disable no-unused-vars */
// /* eslint-disable react-hooks/exhaustive-deps */
// import "./PlantList.scss";
// import { NavLink, useLocation } from "react-router-dom";
// import { Pagination, Image as ImageAnt } from 'antd';
// import { toast } from "react-toastify";

// import plantService from "../../services/plantService";
// import DeleteIcon from "../../icons/DeletePostIcon";
// import PlusIcon from "../../icons/PlusIcon";

// import FilterAndSearch from "./child/FilterAndSearch";
// import ModalConfirm from "../ModalConfirm/ModalConfirm";
// import ModalCreatePlant from "./child/ModalCreateEditPlant";
// import parse from "html-react-parser";

// import { useEffect, useState, useRef } from "react";
// import { useSelector } from "react-redux";

// const limit = 20;

// const PlantList = () => {
//   const location = useLocation();

//   const searchParams = new URLSearchParams(location.search);
//   const categoryQuery = searchParams.get('type') ?? "";
//   const searchTermQuery = searchParams.get('searchTerm') ?? "";

//   const { user } = useSelector((state) => state.Auth);
//   const isEditable = user?.role === "ADMIN";

//   const [categories, setCategories] = useState([]);
//   const [plants, setPlants] = useState([]);
//   const [category, setCategory] = useState(categoryQuery);
//   const [searchTerm, setSearchTerm] = useState(searchTermQuery);
//   const [totalPlants, setTotalPlants] = useState(0);
//   const [currentPage, setCurrentPage] = useState(1);
//   const [currPlantId, setCurrPlantId] = useState(0);
//   const [isLoadingCreate, setIsLoadingCreate] = useState(false);
//   const [isLoadingDelete, setIsLoadingDelete] = useState(false);
//   const [isOpenCreateModal, setIsOpenCreateModal] = useState(false);
//   const [isOpenDeleteModal, setIsOpenDeleteModal] = useState(false);
//   const [isOpenFilterSreach, setIsOpenFilterSreach] = useState(window.innerWidth <= 1024 ? false : true);

//   useEffect(() => {
//     getListCategory();
//     setCategory(categoryQuery);
//     setSearchTerm(searchTermQuery);
//   }, []);

//   useEffect(() => {
//     getListPlants(currentPage, category, searchTermQuery);
//   }, [category, searchTermQuery, currentPage]);

//   useEffect(() => {
//     const currCategory = categories.includes(categoryQuery) ? categoryQuery : ""
//     setCategory(currCategory);
//     setSearchTerm(searchTermQuery);
//     setCurrentPage(1);
//   }, [categoryQuery, searchTermQuery])

//   useEffect(() => {
//     const currCategory = categories.includes(categoryQuery) ? categoryQuery : ""
//     setCategory(currCategory);
//   }, [categories])

//   useEffect(() => {
//     if (currentPage > Math.ceil(totalPlants / limit))
//       setCurrentPage(Math.ceil(totalPlants / limit))
//   }, [totalPlants])

//   const getListCategory = async () => {
//     try {
//       const { data } = await plantService.getCategoryList();
//       setCategories(data.categoryList.map(ele => {
//         return ele.category;
//       }));
//     } catch (error) {
//       setCategories([]);
//     }
//   };

//   const getListPlants = async (currPage = 1, currCategory, currSearchTerm) => {
//     const queries = {
//       type: currCategory,
//       offset: currPage === 0 ? 0 : (currPage - 1) * limit,
//       limit: limit,
//       searchTerm: currSearchTerm
//     }
//     try {
//       const { data } = await plantService.getPlants(queries);
//       setPlants(data?.plants);
//       setTotalPlants(data?.total)
//     } catch (error) {
//       setPlants([]);
//       setTotalPlants(0)
//     }
//   };

//   const onChange = (pageNumber) => {
//     setCurrentPage(pageNumber)
//   }

//   const handleDelete = async () => {
//     try {
//       setIsLoadingDelete(true);
//       await plantService.deletePlant(currPlantId);
//       setIsLoadingDelete(false);
//       setIsOpenDeleteModal(false);
//       getListPlants(currentPage, category, searchTermQuery);
//       getListCategory();
//       toast.success("Delete success!", { theme: "colored" });
//     } catch (error) {
//       toast.error("Delete fail!", { theme: "colored" });
//     }
//   };

//   const closeConfirm = () => {
//     setIsOpenDeleteModal(false)
//   }

//   const OpenModalDelete = (id) => {
//     setIsOpenDeleteModal(true);
//     setCurrPlantId(id);
//   }

//   const handleCreatePlant = async (data) => {
//     try {
//       setIsLoadingCreate(true)
//       await plantService.createPlant(data)
//       setIsLoadingCreate(false)
//       toast.success("update success!", { theme: "colored" });
//       setIsOpenCreateModal(false)
//       getListPlants(currentPage, category, searchTermQuery);
//       getListCategory();
//       return true
//     } catch (error) {
//       setIsLoadingCreate(false);
//       toast.error("update fail!", { theme: "colored" });
//       return false
//     }
//   }

//   return (
//     <>
//       <div className="plant-list">
        
//         <div className="plant-info">
//           {
//             <div className="row-info">
//               {
//                 <div className="header-title">
//                   <h1>
//                     {category ? category : "Tất cả"}
//                   </h1>
//                 </div>
//               }
//               <div className="post-list">
//                 {plants?.length > 0 ? plants.map((post, subIndex) => {
//                   return (
//                     <NavLink to={`./${post.id}`} key={subIndex}>
//                       <div className="post-card">

//                         <div className="thumpnail">
//                           <ImageAnt
//                             preview={false}
//                             src={post?.avatar}
//                             style={{ objectFit: 'contain' }}
//                           />
//                         </div>
//                         <div className="content">
//                           <div className="content-head">
//                             <div className="name-plant">
//                               {post?.name}
//                             </div>
//                             <span hidden={!isEditable} onClick={(event) => { event.preventDefault(); OpenModalDelete(post?.id) }}>
//                               <></>
//                               <DeleteIcon />
//                             </span>
//                           </div>
//                           <div className="description">
//                             {parse(post?.description)}
//                           </div>
//                         </div>
//                       </div>
//                     </NavLink>
//                   )
//                 }) : <></>}
//               </div>
//             </div>
//           }
//           {parseFloat(totalPlants / limit) > 1 && <Pagination
//             defaultCurrent={1}
//             total={totalPlants}
//             pageSize={limit}
//             showSizeChanger={false}
//             onChange={onChange}
//           />}
//           <button
//             className="btn-add-plant"
//             hidden={!isEditable}
//             style={isOpenFilterSreach ? {right: 'var(--reference-width)'} : {right: '0'}}
//             onClick={() => { setIsOpenCreateModal(true) }}
//           >
//             <PlusIcon />
//           </button>
//           <ModalConfirm
//             isLoading={isLoadingDelete}
//             message={"Bạn chắc chắn muốn xoá!"}
//             hidden={!isOpenDeleteModal}
//             onConfirm={handleDelete}
//             onCancel={closeConfirm}
//           />
//           </div>
//         <FilterAndSearch
//           categories={categories}
//           category={category}
//           searchTerm={searchTerm}
//           setSearchTerm={setSearchTerm}
//           searchTermQuery={searchTermQuery}
//           isOpenFilterSreach={isOpenFilterSreach}
//           setIsOpenFilterSreach={setIsOpenFilterSreach}
//         />
//       </div>
//       <ModalCreatePlant
//         action="CREATE"
//         isOpenModal={isOpenCreateModal}
//         setIsOpenModal={setIsOpenCreateModal}
//         isLoading={isLoadingCreate}
//         categoriesProp={categories}
//         handlePlant={handleCreatePlant}
//         toast={toast}
//       />
//     </>
//   );
// };

// export default PlantList;

import "./PlantList.scss";
import { NavLink, useLocation } from "react-router-dom";
import { Pagination, Image as ImageAnt, Spin } from 'antd';
import { toast } from "react-toastify";
import { useCallback, useEffect, useMemo, useState } from "react";
import { useSelector } from "react-redux";
import plantService from "../../services/plantService";
import DeleteIcon from "../../icons/DeletePostIcon";
import PlusIcon from "../../icons/PlusIcon";
import FilterAndSearch from "./child/FilterAndSearch";
import ModalConfirm from "../ModalConfirm/ModalConfirm";
import ModalCreatePlant from "./child/ModalCreateEditPlant";
import parse from "html-react-parser";
import { debounce } from 'lodash';

const limit = 20;

const PlantList = () => {
  const location = useLocation();
  const searchParams = new URLSearchParams(location.search);
  const categoryQuery = searchParams.get('type') ?? "";
  const searchTermQuery = searchParams.get('searchTerm') ?? "";
  const { user } = useSelector((state) => state.Auth);
  const isEditable = useMemo(() => user?.role === "ADMIN", [user]);

  const [categories, setCategories] = useState([]);
  const [plants, setPlants] = useState([]);
  const [category, setCategory] = useState(categoryQuery);
  const [searchTerm, setSearchTerm] = useState(searchTermQuery);
  const [totalPlants, setTotalPlants] = useState(0);
  const [currentPage, setCurrentPage] = useState(1);
  const [currPlantId, setCurrPlantId] = useState(0);
  const [isLoadingPlantList, setIsLoadingPlantList] = useState(false);
  const [isLoadingCreate, setIsLoadingCreate] = useState(false);
  const [isLoadingDelete, setIsLoadingDelete] = useState(false);
  const [isOpenCreateModal, setIsOpenCreateModal] = useState(false);
  const [isOpenDeleteModal, setIsOpenDeleteModal] = useState(false);
  const [isOpenFilterSreach, setIsOpenFilterSreach] = useState(window.innerWidth <= 1024 ? false : true);

  useEffect(() => {
    getListCategory();
    setCategory(categoryQuery);
    setSearchTerm(searchTermQuery);
  }, []);

  useEffect(() => {
    getListPlants(currentPage, category, searchTermQuery);

  }, [category, searchTermQuery, currentPage]);

  useEffect(() => {
    if(searchTerm === "")
    getListPlants(currentPage, category, "");
  }, [searchTerm]);

  useEffect(() => {
    const currCategory = categories.includes(categoryQuery) ? categoryQuery : "";
    setCategory(currCategory);
    setSearchTerm(searchTermQuery);
    setCurrentPage(1);
  }, [categoryQuery, searchTermQuery]);

  useEffect(() => {
    const currCategory = categories.includes(categoryQuery) ? categoryQuery : "";
    setCategory(currCategory);
  }, [categories]);

  useEffect(() => {
    if (currentPage > Math.ceil(totalPlants / limit)) {
      setCurrentPage(Math.ceil(totalPlants / limit));
    }
  }, [totalPlants]);

  const getListCategory = useCallback(async () => {
    try {
      const { data } = await plantService.getCategoryList();
      setCategories(data.categoryList.map(ele => ele.category));
    } catch (error) {
      setCategories([]);
    }
  }, []);

  const getListPlants = useCallback(debounce(async (currPage = 1, currCategory, currSearchTerm) => {
    const queries = {
      type: currCategory,
      offset: currPage === 0 ? 0 : (currPage - 1) * limit,
      limit,
      searchTerm: currSearchTerm
    };
    try {
      setIsLoadingPlantList(true);
      const { data } = await plantService.getPlants(queries);
      setPlants(data?.plants);
      setTotalPlants(data?.total);
      setIsLoadingPlantList(false);
    } catch (error) {
      setPlants([]);
      setTotalPlants(0);
      setIsLoadingPlantList(false);
    }
  }, 300), []);

  const onChange = useCallback((pageNumber) => {
    setCurrentPage(pageNumber);
  }, []);

  const handleDelete = useCallback(async () => {
    try {
      setIsLoadingDelete(true);
      await plantService.deletePlant(currPlantId);
      setIsLoadingDelete(false);
      setIsOpenDeleteModal(false);
      getListPlants(currentPage, category, searchTermQuery);
      getListCategory();
      toast.success("Delete success!", { theme: "colored" });
    } catch (error) {
      toast.error("Delete fail!", { theme: "colored" });
    }
  }, [currPlantId, currentPage, category, searchTermQuery, getListCategory, getListPlants]);

  const closeConfirm = useCallback(() => {
    setIsOpenDeleteModal(false);
  }, []);

  const OpenModalDelete = useCallback((id) => {
    setIsOpenDeleteModal(true);
    setCurrPlantId(id);
  }, []);

  const handleCreatePlant = useCallback(async (data) => {
    try {
      setIsLoadingCreate(true);
      await plantService.createPlant(data);
      setIsLoadingCreate(false);
      setIsOpenCreateModal(false);
      getListPlants(currentPage, category, searchTermQuery);
      getListCategory();
      toast.success("Tạo cây thành công!", { theme: "colored" });
      return {
        result: true,
        status_code: 200
      };
    } catch (error) {
      setIsLoadingCreate(false);
      if (error?.response?.status != 409) {
        toast.error("Hệ thống bị lỗi", { theme: "colored" });
      }
      return {
        result: false,
        status_code: error?.response?.status
      };
    }
  }, [currentPage, category, searchTermQuery, getListCategory, getListPlants]);

  return (
    <>
      <div className="plant-list">
        <div className="plant-info">
          <div className="row-info">
            <div className="header-title">
              <h1>{category ? category : "Tất cả"}</h1>
            </div>
            <Spin
              spinning={isLoadingPlantList}
              fullscreen={isLoadingPlantList}
              style={{
                zIndex: "5000",
                height: "100%",
                flexGrow: "1",
                overflowY: "scroll",
              }}
            >
            <div className="post-list">
              {plants?.length > 0 ? plants.map((post, subIndex) => (
                <NavLink to={`./${post.id}`} key={subIndex}>
                  <div className="post-card">
                    <div className="thumpnail">
                      <ImageAnt
                        preview={false}
                        src={post?.avatar}
                        style={{ objectFit: 'contain' }}
                      />
                    </div>
                    <div className="content">
                      <div className="content-head">
                        <div className="name-plant">
                          {post?.name}
                        </div>
                        <span hidden={!isEditable} onClick={(event) => { event.preventDefault(); OpenModalDelete(post?.id); }}>
                          <DeleteIcon />
                        </span>
                      </div>
                      <div className="description">
                        {parse(post?.description)}
                      </div>
                    </div>
                  </div>
                </NavLink>
              )) : <>Không có cây trồng phù hợp</>}
            </div>
            </Spin>
          </div>
          {parseFloat(totalPlants / limit) > 1 && <Pagination
            defaultCurrent={1}
            total={totalPlants}
            pageSize={limit}
            showSizeChanger={false}
            onChange={onChange}
          />}
          <button
            className="btn-add-plant"
            hidden={!isEditable}
            style={isOpenFilterSreach ? { right: 'var(--reference-width)' } : { right: '0' }}
            onClick={() => { setIsOpenCreateModal(true); }}
          >
            <PlusIcon />
          </button>
          <ModalConfirm
            isLoading={isLoadingDelete}
            message={"Bạn chắc chắn muốn xoá!"}
            hidden={!isOpenDeleteModal}
            onConfirm={handleDelete}
            onCancel={closeConfirm}
          />
        </div>
        <FilterAndSearch
          categories={categories}
          category={category}
          searchTerm={searchTerm}
          setSearchTerm={setSearchTerm}
          searchTermQuery={searchTermQuery}
          isOpenFilterSreach={isOpenFilterSreach}
          setIsOpenFilterSreach={setIsOpenFilterSreach}
        />
      </div>
      <ModalCreatePlant
        action="CREATE"
        isOpenModal={isOpenCreateModal}
        setIsOpenModal={setIsOpenCreateModal}
        isLoading={isLoadingCreate}
        categoriesProp={categories}
        handlePlant={handleCreatePlant}
        toast={toast}
      />
    </>
  );
};

export default PlantList;
