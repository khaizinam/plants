/* eslint-disable no-unused-vars */
import "./ModalCreateEditPlant.scss";
import React, { useState, useRef, useEffect, useCallback, useMemo } from "react";

import CancelIcon from "../../../icons/CancelIcon";
import UploadPlantImgIcon from "../../../icons/UploadPlantImgIcon";
import CameraIcon from "../../../icons/CameraIcon";
import FileIcon from "../../../icons/FileIcon";
import Loading from "../../Loading/Loading";
import { faXmark } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import { Form, Input, Row, Col, Select, Image, message } from "antd";
import ImgCrop from "antd-img-crop";
import {
  CustomModal,
  SpanLabel,
  CustomStyleFormItem,
  CustomTextArea,
  CustomDivider,
} from "./Custom.style";

import plantService from "../../../services/plantService";

export default function ModalCreateEditPlant({
  action,
  isOpenModal,
  setIsOpenModal,
  isLoading,
  categoriesProp,
  handlePlant,
  plantData
}) {
  const inputFileRef = useRef(null);
  const [form] = Form.useForm();
  const [fileList, setFileList] = useState([]);
  const [currAvatarImg, setCurrAvatarImg] = useState(null);
  const [categories, setCategories] = useState([]);
  const [isPressSaveBtn, setIsPressSaveBtn] = useState(false);

  useEffect(() => {
    categoriesProp ? setCategories(categoriesProp) : getListCategory();
  }, [categoriesProp]);

  useEffect(() => {
    if (plantData?.avatar) {
      setCurrAvatarImg(plantData?.avatar);
    }
  }, [plantData]);

  useEffect(() => {
    if (fileList[0]) {
      setCurrAvatarImg(URL.createObjectURL(fileList[0]));
    } else setCurrAvatarImg(null);
  }, [fileList]);

  const resetState = useCallback(() => {
    setFileList([]);
    setIsPressSaveBtn(false);
    form.resetFields();
  }, [form]);

  const getListCategory = useCallback(async () => {
    try {
      const { data } = await plantService.getCategoryList();
      setCategories(data.categoryList.map(ele => ele.category));
    } catch (error) {
      setCategories([]);
    }
  }, []);

  const onChange = useCallback((e) => {
    const file = e.target.files[0];
    const acceptedFormats = ["image/jpeg", "image/jpg", "image/png"];
    // console.log("check checkFileIsValid(file, acceptedFormats): ", checkFileIsValid(file, acceptedFormats))
    if (!checkFileIsValid(file, acceptedFormats)) {
      message.error("You aren't allowed to upload this file type");
    } else {
      setFileList([file]);
      // form.setFieldValue("avatar", file);
      setCurrAvatarImg(file);
    }
  }, [form]);

  const handleDeleteCurrImg = useCallback(() => {
    setIsPressSaveBtn(true);
    setCurrAvatarImg(null);
    setFileList([]);
    form.setFieldValue("avatar", null);
    form.setFields([
      {
        name: 'avatar',
        errors: ['Bạn cần nhập thông tin này'],
      },
    ]);
  }, [form]);

  const handleChangeCategory = useCallback((value) => {}, []);

  const checkFileIsValid = useCallback((file, acceptedFormats) => {
    return acceptedFormats.some((format) => {
      if (format.startsWith(".")) {
        return file.name.toLowerCase().endsWith(format);
      }
      return file.type.startsWith(format);
    });
  }, []);

  const trimFieldValue = useCallback((nameField) => {
    form.setFieldValue(nameField, form.getFieldValue(nameField).trim())
  }, [form]);

  const handleSave = useCallback((values) => {
    form.validateFields().then(async (values) => {
      const formData = new FormData();
      formData.append("image", fileList[0]);
      formData.append("category", values?.category[0]);
      formData.append("description", values?.description);
      formData.append("name", values?.name);
      const result = await handlePlant(formData);
      if (result?.result) resetState();
      else {
        if(result?.status_code === 409)
        form.setFields([
          {
            name: 'name',
            errors: ['Tên cây trồng đã tồn tại'],
          },
        ]);
      }
    }).catch((err) => {

    });
  }, [fileList, form, handlePlant, resetState]);

  const modalTitle = useMemo(() => (
    <div className="modal-create-header">
      <div></div>
      <div>{action === "CREATE" ? "Thêm cây" : action === "UPDATE" ? "Chỉnh sửa" : "" }</div>
      <div>
        <button
          className="btn-cancel"
          onClick={() => {
            resetState();
            setIsOpenModal(false);
            setIsPressSaveBtn(false);
          }}
        >
          <CancelIcon />
        </button>
      </div>
    </div>
  ), [action, resetState, setIsOpenModal]);

  return (
    <>
      <CustomModal
        title={modalTitle}
        open={isOpenModal}
        closable={false}
        width={"700px"}
        footer={[
          <div className="modal-create-footer" key={"modal-create-footer"}>
            {isLoading ? (
              <Loading />
            ) : (
              <button form="createPlant" type="submit" className="btn-save" onClick={()=>{ if(!isPressSaveBtn) setIsPressSaveBtn(true)}}>
                Lưu
              </button>
            )}
          </div>,
        ]}
      >
        <Form
          size="middle"
          name="createPlant"
          className="modal-create-content"
          form={form}
          onFinish={handleSave}
          colon={false}
          layout="vertical"
        >
          <Row gutter={[20, 0]} style={{ margin: "0 15px" }}>
            <Col xs={13} sm={15} md={15} lg={15} xl={15} style={{ paddingLeft: "0" }}>
              <CustomStyleFormItem
                name="name"
                label={<SpanLabel>Tên giống cây:</SpanLabel>}
                initialValue={plantData?.name ?? ""}
                rules={[
                  {
                    max: 50,
                    message: 'The maximum length for a string is 50 characters'
                  },
                  {
                    required: true,
                    pattern: /\S/,
                    message: "Bạn cần nhập thông tin này",
                  },
                ]}
              >
                <Input size="middle" placeholder="" name="" onBlur={()=>{trimFieldValue('name')}}/>
              </CustomStyleFormItem>
              <CustomStyleFormItem
                name="category"
                label={<SpanLabel>Thuộc loại:</SpanLabel>}
                initialValue={plantData?.category ? [plantData?.category] : []}
                rules={[
                  {
                    required: true,
                    pattern: /\S/,
                    message: "Bạn cần nhập thông tin này",
                  }
                ]}
              >
                <Select
                  mode="tags"
                  maxCount={1}
                  style={{ width: "100%" }}
                  onChange={handleChangeCategory}
                  options={categories.map((ele) => ({
                    value: ele,
                    label: ele,
                  }))}
                />
              </CustomStyleFormItem>
            </Col>
            <Col xs={11} sm={9} md={9} lg={9} xl={9} style={{ paddingRight: "0" }}>
              <CustomStyleFormItem
                name="avatar"
                label={
                  <SpanLabel>
                    <CameraIcon /> Hình ảnh
                  </SpanLabel>
                }
                initialValue={null}
                rules={[
                  {
                    required: !currAvatarImg,
                    message: "Bạn cần nhập thông tin này",
                  },
                ]}
              >
                  <Input
                    ref={inputFileRef}
                    type="file"
                    // value=""
                    hidden
                    accept="image/*"
                    // name="avatarLink"
                    onChange={onChange}
                  />
              </CustomStyleFormItem>
              {fileList.length === 0 && !currAvatarImg ? (
                <div
                  // className="field-upload"
                  className={"field-upload" + ((isPressSaveBtn) ? " error" : "")}
                  // className={"field-upload error"}
                  onClick={() => inputFileRef?.current?.input.click()}
                >
                  <UploadPlantImgIcon />
                </div>
              ) : (
                <div className="container-current-img field-upload">
                  <div className="delete-btn" onClick={handleDeleteCurrImg}><FontAwesomeIcon icon={faXmark} /></div>
                  <Image preview={false} style={{objectFit:'contain'}} width={"158px"} height={"120px"} src={currAvatarImg} />
                </div>
              )}
            </Col>
          </Row>
          <CustomDivider />
          <CustomStyleFormItem
            style={{ margin: "0 15px" }}
            name="description"
            label={
              <SpanLabel>
                <FileIcon /> Giới thiệu
              </SpanLabel>
            }
            initialValue={plantData?.description ?? ""}
            rules={[
              {
                max: 2000,
                message: 'The maximum length for a string is 2000 characters'
              },
              {
                required: true,
                pattern: /\S/,
                message: "Bạn cần nhập thông tin này",
              },
            ]}
          >
            <CustomTextArea autoSize={{ minRows: 5, maxRows: 6 }} onBlur={()=>{trimFieldValue('description')}}/>
          </CustomStyleFormItem>
        </Form>
      </CustomModal>
    </>
  );
}
