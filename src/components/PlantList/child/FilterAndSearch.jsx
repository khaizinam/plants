/* eslint-disable no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
import './FilterAndSearch.scss'

import { useEffect, useRef, useMemo } from "react";
import { NavLink, useLocation } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMagnifyingGlass } from "@fortawesome/free-solid-svg-icons";
import AllPlantsIcon from "../../../icons/AllPlantsIcon";

const FilterAndSearch = ({
  categories,
  category,
  searchTerm,
  setSearchTerm,
  searchTermQuery,
  isOpenFilterSreach,
  setIsOpenFilterSreach
}) => {
  const plantReferenceRef = useRef(null);
  const searchButtonRef = useRef(null);

  useEffect(() => {
    const handleResize = () => {
      setIsOpenFilterSreach(window.innerWidth > 1024);
    };

    window.addEventListener('resize', handleResize);

    // Initial call to handleResize to set the correct state
    handleResize();

    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, [setIsOpenFilterSreach]);

  const toggleSidebar = () => {
    setIsOpenFilterSreach(prevState => !prevState);
  };

  const searchLink = useMemo(() => ({
    pathname: './',
    search: `?type=${category}&searchTerm=${searchTerm}`
  }), [category, searchTerm]);

  const allPlantsLink = useMemo(() => ({
    pathname: './',
    search: searchTermQuery ? `?searchTerm=${searchTermQuery}` : '',
  }), [searchTermQuery]);

  const handleKeyDown = (e) => {
    if (e.key === 'Enter') {
      e.preventDefault();
      e.target.blur(); // Thoát focus thẻ input và kích hoạt sự kiện onBlur
    }
  };

  const handleBlur = () => {
    const result = searchTerm.trim().replace(/\s{2,}/g, ' ');
    setSearchTerm(result);
    // Kích hoạt thẻ NavLink sau khi setSearchTerm
    if (searchButtonRef.current) {
      searchButtonRef.current.click();
    }
  };

  return (
    <div className="filter-search-container">
      <div className="toggle-btn" onClick={toggleSidebar}>
        <FontAwesomeIcon icon={faMagnifyingGlass} />
      </div>
      <div className={`plant-reference ${isOpenFilterSreach ? 'show' : 'hidden'}`} ref={plantReferenceRef}>
        <div className="search-bar">
          <div className="icon-and-input">
            <span><FontAwesomeIcon icon={faMagnifyingGlass} /></span>
            <input 
              type="text" 
              onChange={(e) => setSearchTerm(e?.target?.value)}
              onBlur={handleBlur}
              value={searchTerm} 
              onFocus={(event) => { event.target.select() }} 
              onKeyDown={handleKeyDown} // Thêm sự kiện onKeyDown
              placeholder={"Tìm kiếm"} 
            />
          </div>
          <div className="search-button">
            <NavLink to={searchLink} ref={searchButtonRef}>
              Tìm
            </NavLink>
          </div>
        </div>
        <div className="search-result-title">
          Kết quả tìm kiếm
        </div>
        <div className="filter-title">
          Bộ lọc
        </div>
        <div className="filter-container">
          <NavLink to={allPlantsLink}>
            <div className={category === "" ? "filter-all active-all" : "filter-all"}>
              <div className="icon-container">
                <AllPlantsIcon />
              </div>
              <span className="title">Tất cả</span>
            </div>
          </NavLink>
          <div className="filter-type">
            <div className="reference-list">
              {categories?.map((currCategory, index) => {
                const categoryLink = {
                  pathname: './',
                  search: searchTermQuery ? `?type=${currCategory}&searchTerm=${searchTermQuery}` : `?type=${currCategory}`
                };
                return (
                  <NavLink key={index} to={categoryLink}>
                    <div
                      key={index}
                      className={"reference-item " + (currCategory === category ? "active" : "")}
                    >
                      <span className="dot"></span>
                      {currCategory}
                    </div>
                  </NavLink>
                )
              })}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FilterAndSearch;

// /* eslint-disable no-unused-vars */
// /* eslint-disable react-hooks/exhaustive-deps */
// import './FilterAndSearch.scss'

// import { useEffect, useState, useRef, useCallback, useMemo } from "react";

// import { NavLink, useLocation } from "react-router-dom";
// import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
// import { faMagnifyingGlass } from "@fortawesome/free-solid-svg-icons";
// import { debounce } from 'lodash';

// import AllPlantsIcon from "../../../icons/AllPlantsIcon";

// const FilterAndSearch = ({
//   categories,
//   category,
//   searchTerm,
//   setSearchTerm,
//   searchTermQuery,
//   isOpenFilterSreach,
//   setIsOpenFilterSreach
// }) => {
//   const plantReferenceRef = useRef(null);

//   useEffect(() => {
//     const handleResize = () => {
//       setIsOpenFilterSreach(window.innerWidth > 1024);
//     };

//     window.addEventListener('resize', handleResize);

//     // Initial call to handleResize to set the correct state
//     handleResize();

//     return () => {
//       window.removeEventListener('resize', handleResize);
//     };
//   }, [setIsOpenFilterSreach]);

//   const toggleSidebar = useCallback(() => {
//     setIsOpenFilterSreach(prevState => !prevState);
//   }, [setIsOpenFilterSreach]);

//   // const debouncedSetSearchTerm = useMemo(() => debounce((value) => {
//   //   setSearchTerm(value);
//   // }, 300), [setSearchTerm]);

//   // const handleInputChange = (e) => {
//   //   debouncedSetSearchTerm(e.target.value);
//   // };

//   const searchLink = useMemo(() => ({
//     pathname: './',
//     search: `?type=${category}&searchTerm=${searchTerm}`
//   }), [category, searchTerm]);

//   const allPlantsLink = useMemo(() => ({
//     pathname: './',
//     search: searchTermQuery ? `?searchTerm=${searchTermQuery}` : '',
//   }), [searchTermQuery]);

//   return (
//     <div className="filter-search-container">
//       <div className="toggle-btn" onClick={toggleSidebar}>
//         <FontAwesomeIcon icon={faMagnifyingGlass} />
//       </div>
//       <div className={`plant-reference ${isOpenFilterSreach ? 'show' : 'hidden'}`} ref={plantReferenceRef}>
//         <div className="search-bar">
//           <div className="icon-and-input">
//             <span><FontAwesomeIcon icon={faMagnifyingGlass} /></span>
//             <input
//               type="text"
//               // onChange={handleInputChange}
//               onChange={(e) => setSearchTerm(e?.target?.value)}
//               onBlur={() => {
//                 const result = searchTerm.trim().replace(/\s{2,}/g, ' ')
//                 setSearchTerm(result)
//               }}
//               value={searchTerm}
//               onFocus={(event) => { event.target.select() }}
//               placeholder={"Tìm kiếm"}
//             />
//           </div>
//           <div className="search-button">
//             <NavLink to={searchLink}>
//               Tìm
//             </NavLink>
//           </div>
//         </div>
//         <div className="search-result-title">
//           Kết quả tìm kiếm
//         </div>
//         <div className="filter-title">
//           Bộ lọc
//         </div>
//         <div className="filter-container">
//           <NavLink to={allPlantsLink}>
//             <div className={category === "" ? "filter-all active-all" : "filter-all"}>
//               <div className="icon-container">
//                 <AllPlantsIcon />
//               </div>
//               <span className="title">Tất cả</span>
//             </div>
//           </NavLink>
//           <div className="filter-type">
//             <div className="reference-list">
//               {categories?.map((currCategory, index) => {
//                 const categoryLink = {
//                   pathname: './',
//                   search: searchTermQuery ? `?type=${currCategory}&searchTerm=${searchTermQuery}` : `?type=${currCategory}`
//                 };
//                 return (
//                   <NavLink key={index} to={categoryLink}>
//                     <div
//                       key={index}
//                       className={"reference-item " + (currCategory === category ? "active" : "")}
//                     >
//                       <span className="dot"></span>
//                       {currCategory}
//                     </div>
//                   </NavLink>
//                 )
//               })}
//             </div>
//           </div>
//         </div>
//       </div>
//     </div>
//   );
// };

// export default FilterAndSearch;

