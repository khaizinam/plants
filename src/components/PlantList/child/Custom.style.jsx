import styled from 'styled-components'
import { Modal, Input, Form, Upload, Divider} from "antd";


export const CustomModal = styled(Modal)`
    & .ant-modal-content {
        // width: 480px;
        width: 100%;
        max-width: 880px;
        padding: 0;
        border-radius: 15px;
        overflow: hidden;
    }
    & .ant-modal-body {
        //padding: 0 15px;
    }
`
export const CustomInputPopup = styled(Input)`
  height: 43.6px;
  padding: 12px 20px;
`

export const SpanLabel = styled.span`
    
    text-align: center;
    font-size: 14px;
    font-style: normal;
    font-weight: 600;
    line-height: normal;
    display: flex;
    align-items: center;
    gap: 10px;
`
export const CustomStyleFormItem = styled(Form.Item)`
  //width: 100%;
  margin-bottom: 10px;
  & .ant-form-item-label {
    text-align: left;
  }
  & > .ant-form-item {
    marginbottom: 0px;
    justify-content: flex-start;
  }
  & .ant-form-item-label > label.ant-form-item-required:not(.ant-form-item-required-mark-optional)::before {
    display: none;
  }
  & .ant-form-item-control-input {
    min-height: 0;
  }
`
export const CustomTextArea = styled(Input.TextArea)`
  & .ant-input.css-dev-only-do-not-override-1xg9z9n {
    height: 85.6px !important;
  }
`

export const CustomUpload = styled(Upload)`
  .ant-upload.ant-upload-select {
    border-color: #0C9869 !important;
    background-color: white !important;
  }
  .ant-upload-list.ant-upload-list-picture-card {
    
  }
`

export const CustomDivider = styled(Divider)`
  margin-top: 0;
  margin-bottom: 12px;
`