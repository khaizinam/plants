import { Button } from "rsuite";
import WarningIcon from "../../icons/WarningIcon";
import "./ModalDelete.scss";

import { Modal } from "rsuite";

const ModalDelete = ({ open, onClose, onDelete = () => {}, content }) => {
  return (
    <Modal open={open} onClose={onClose} className="modal-delete">
      <div className="header">
        <div className="icon">
          <WarningIcon />
        </div>
        <div className="content">{content ?? "Bạn chắc chắn chưa?"}</div>
      </div>
      <div className="footer">
        <Button onClick={onClose}>Hủy</Button>
        <Button appearance="primary" color="cyan" onClick={onDelete}>
          Xác nhận
        </Button>
      </div>
    </Modal>
  );
};

export default ModalDelete;
