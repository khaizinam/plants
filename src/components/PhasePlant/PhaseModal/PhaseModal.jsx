import "./PhaseModal.scss";

import { useRef, useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faXmark } from "@fortawesome/free-solid-svg-icons";
import Loading from "../../Loading/Loading";
import plantService from "../../../services/plantService";
import { useDispatch } from "react-redux";
import { UpdateStages } from "../../../redux/plant";
import { toast } from "react-toastify";
import { useParams } from "react-router-dom";

const PhaseModal = ({ modal, onClose }) => {
  const dispatch = useDispatch();
  const thisModal = useRef();
  const outerClose = (e) => {
    e.target === thisModal.current && onClose && onClose();
  };
  const { id: plantId } = useParams();

  const [stage, setStage] = useState(
    modal.data || {
      name: "",
      range: "0",
      humidity_above: 0,
      humidity_below: 0,
      description: "",
    }
  );

  const [loading, setLoading] = useState(false);

  const handleChange = (e) => {
    setStage((prev) => ({
      ...prev,
      [e.target.name]: e.target.value,
    }));
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    setLoading(true);
    if (loading) return;
    try {
      if (modal.type === "EDIT") {
        const { id, ...rest } = stage;
        const { data = [] } = await plantService.updateStage(id, rest);
        await dispatch(UpdateStages(data));
        toast.info("Đã sửa");
      } else {
        const { id, ...rest } = stage;
        const { data = [] } = await plantService.createStage(plantId, rest);
        await dispatch(UpdateStages(data));
        toast.info("Đã thêm");
      }
      onClose && onClose();
    } catch (error) {
      console.log(error);
    }
    setLoading(false);
  };

  return (
    <div className={`phase-modal`} ref={thisModal} onClick={outerClose}>
      <div className="phase-modal-box">
        <div className="phase-modal-header">
          {modal.type === "EDIT" ? "Sửa thông tin" : "Thêm giai đoạn"}
          <div className="close-btn" onClick={onClose}>
            <FontAwesomeIcon icon={faXmark} />
          </div>
        </div>
        <div className="phase-modal-body">
          <div className="phase-info-list">
            <div className="phase-info">
              <div className="title">Tên: </div>
              <div className="value">
                <input
                  name="name"
                  type="text"
                  value={stage?.name}
                  onChange={handleChange}
                  placeholder="Nhập tên giai đoạn"
                />
              </div>
            </div>
            <div className="phase-info">
              <div className="title">Thời gian (ngày): </div>
              <div className="value">
                <input
                  type="number"
                  name="range"
                  min={0}
                  value={stage.range}
                  onChange={handleChange}
                  placeholder="Khoảng thời gian"
                />
              </div>
            </div>
            <div className="phase-info">
              <div className="title">Độ ẩm (%):</div>
              <div className="value">
                <input
                  type="number"
                  min={0}
                  max={100}
                  value={stage.humidity_below}
                  onChange={handleChange}
                  name="humidity_below"
                />
                đến
                <input
                  type="number"
                  min={0}
                  max={100}
                  value={stage.humidity_above}
                  onChange={handleChange}
                  name="humidity_above"
                />
              </div>
            </div>
            <div className="phase-info">
              <div className="title">Mô tả:</div>
            </div>
          </div>
          <textarea
            spellCheck="false"
            className="phase-info-description"
            rows={5}
            name="description"
            onChange={handleChange}
            placeholder="Nhập mô tả của giai đoạn ..."
            value={stage?.description}
          ></textarea>
          {/* <div className="phase-image">
              <img
                src={
                  stage?.img_url
                    ? stage?.img_url
                    : "https://images.unsplash.com/reserve/bOvf94dPRxWu0u3QsPjF_tree.jpg?ixid=M3wxMjA3fDB8MXxzZWFyY2h8NHx8bmF0dXJhbHxlbnwwfHx8fDE3MDgzODI4NDZ8MA&ixlib=rb-4.0.3"
                }
                alt=""
              />
            </div> */}
        </div>
        <div className="phase-modal-footer">
          <div
            className="submit-btn btn"
            onClick={(e) => {
              handleSubmit(e);
            }}
          >
            {loading ? (
              <Loading />
            ) : modal.type === "EDIT" ? (
              "Cập nhật"
            ) : (
              "Thêm"
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default PhaseModal;
