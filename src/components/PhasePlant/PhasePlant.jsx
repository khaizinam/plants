import "./PhasePlant.scss";

const PhasePlant = ({ stages = [] }) => {
  return (
    <div className="phase-plant">
      <div className="phase-list">
        {stages.map((stage) => {
          const {
            id,
            name,
            no,
            humidity_below,
            humidity_above,
            range,
            description,
            img_url,
          } = stage;
          return (
            <div id={`title-${id}`} className={`phase-card ${id ?? "hidden"}`}>
              <div className="img-container">
                <img
                  src={
                    img_url ??
                    "https://aehinnovativehydrogel.com/wp-content/uploads/2022/07/04June22_what-are-the-requirements-for-plant-growth_iStock-956366756-1024x730.jpg"
                  }
                  alt=""
                />
                <div className="phase-no">{no}</div>
              </div>
              <div className="info-container">
                <div className="phase-header">
                  <div className="phase-name">{name.toUpperCase()}</div>
                </div>
                <div className="phase-range">
                  <div className="title">Thời gian:</div>
                  <div className="value">{range} ngày</div>
                </div>
                <div className="phase-humid">
                  <div className="title">Độ ẩm đất:</div>
                  <div className="value">
                    {humidity_below}% - {humidity_above}%
                  </div>
                </div>
                <hr />
                <div className="phase-description">{description}</div>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default PhasePlant;
