// import React from 'react'
// import Loading from '../Loading/Loading'

// export default function ButtonLoading(props) {
//     return (
//         <> {
//             props.isLoading ?
//                 <div className="button-loading-custom">
//                     <Loading />
//                 </div>
//                 :
//                 <button disabled={props?.disabled ?? false} className="button-loading-custom c-text-base bold" onClick={props?.click ? (() => props.click()) : props?.onClick ? props?.onClick : ''}>
//                     {props.title}
//                 </button>
//         }</>
//     )
// }


import React from 'react'
import Loading from '../Loading/Loading'

export default function ButtonLoading(
{
    isLoading,
    disabled,
    title,
    click,
    ...remainingProps
}
) {
    return (
        <> {
            isLoading ?
                <div className="button-loading-custom">
                    <Loading />
                </div>
                :
                 <button disabled={disabled ?? false} className="button-loading-custom c-text-base bold"
                    onClick={click ? (() => click()) : ''}
                    {...remainingProps}
                >
                    {title}
                </button>
        }</>
    )
}
