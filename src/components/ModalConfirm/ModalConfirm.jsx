import React from "react";
import WarningIcon from "../../icons/WarningIcon";
import Loading from "../Loading/Loading";
import "./ModalConfirm.scss";
export default function ModalConfirm(props) {
  return (
    <>
      <div
        className={`bg-prevent-click ${!props.hidden ? "" : "d-hidden"}`}
        onClick={() => props.onCancel()}
      ></div>
      <div
        className={`modal-confirm-post-plant ${
          props.hidden ? "d-hidden" : ""
        } modal-index`}
      >
        <div className="body">
          <div className="icon">
            <WarningIcon />
          </div>
          <div>
            <div className="title">Xác nhận</div>
            <div className="msg">{props.message ? props.message : ""}</div>
          </div>
        </div>
        <div className="footer">
          <button
            onClick={() => {
              if (!props.isLoading) props.onConfirm();
            }}
            className="btn btn-confirm"
          >
            {props.isLoading ? <Loading /> : "OK"}
          </button>
          <button
            onClick={() => props.onCancel()}
            className="btn btn-cancel-confirm"
          >
            Huỷ
          </button>
        </div>
      </div>
    </>
  );
}
