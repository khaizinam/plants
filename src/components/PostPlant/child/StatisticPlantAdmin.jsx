import { useEffect, useState } from 'react';

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUsers,faHeart, faVirus, faWheatAwn, faX } from "@fortawesome/free-solid-svg-icons";
import ChartService from '../../../services/chartService';
const fontSize = 40

function hexToRgb(hex) {
    // Loại bỏ ký tự # nếu có
    hex = hex.replace(/^#/, '');

    // Chia mã hex thành các thành phần đỏ, xanh lục và xanh dương
    let r = parseInt(hex.substring(0, 2), 16);
    let g = parseInt(hex.substring(2, 4), 16);
    let b = parseInt(hex.substring(4, 6), 16);

    // Trả về màu rgb dưới dạng chuỗi
    return `rgb(${r}, ${g}, ${b})`;
}

function hexToRgba(hex, alpha) {
    // Loại bỏ ký tự "#" từ mã hex và chuyển thành giá trị RGB
    let r = parseInt(hex.slice(1, 3), 16);
    let g = parseInt(hex.slice(3, 5), 16);
    let b = parseInt(hex.slice(5, 7), 16);

    // Chuyển alpha thành phần từ dạng số thập phân sang dạng hex
    let alphaHex = Math.round(alpha * 255).toString(16);
    if (alphaHex.length === 1) {
        alphaHex = '0' + alphaHex; // Đảm bảo rằng alpha có 2 chữ số
    }

    // Kết hợp giá trị RGB và alpha để tạo mã hex RGBA đầy đủ
    let rgba = `rgba(${r}, ${g}, ${b}, ${alpha})`;

    return rgba;
}

function styleBackGround(colorHex) {
    return {
        background: `linear-gradient(to right, ${hexToRgb(colorHex)} 0, ${hexToRgba(colorHex, 0.5)} 100%)`
    }
}

const iconArr = [ faUsers, faHeart, faVirus, faWheatAwn, faX]

export default function StatisticPlantAdmin({ plant_id }) {
    const [dataStatus, setDataStatus] = useState([]);
    useEffect(() => {
        if(plant_id !== undefined)
            getNumberStatuses();
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [plant_id])

    const getNumberStatuses = async () => {
        try {
            const res = await ChartService.getChartPlantAdmin(plant_id);
            setDataStatus(res?.data);
        } catch (error) {
            console.log(error)
        }
    }

    return (<div className="general-statistic-wr">
        {dataStatus.length > 0 ?
            dataStatus.map((ele, index) => {
                return (
                    <div key={index} className="statistic-tag" style={styleBackGround(ele?.bgColor)}>
                        <div className="left-content">
                            <FontAwesomeIcon icon={iconArr[index]} color={ele?.iconColor} fontSize={fontSize} />
                        </div>
                        <div className="right-content">
                            <div className="title">
                                {ele?.name_status}
                            </div>
                            <div className="value">
                                {ele?.count}
                            </div>
                        </div>
                    </div>
                );
            })
            : <></>}
    </div>)
}