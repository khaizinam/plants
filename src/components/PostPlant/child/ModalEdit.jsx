import React, { useState, useEffect, useCallback } from "react";
import { Input } from "antd";
import CancelIcon from "../../../icons/CancelIcon";
import CustomEditor from "../../../components/CustomEditor/CustomEditor";
import ButtonLoading from "../../ButtonLoadind/ButtonLoading";

export default function ModalEdit(props) {
  const [isLoadingImg, setIsLoadingImg] = useState(false);
  const [isFirstActionTitle, setIsFirstActionTitle] = useState(true);
  const [isFirstActionContent, setIsFirstActionContent] = useState(true);
  const [isError, setIsError] = useState({ title: false, sameTitle: false, content: false });

  const onEditTitle = useCallback(
    (event) => {
      if (!props.isLoading) {
        setIsFirstActionTitle(false);
        setIsError((prev) => ({ ...prev, sameTitle: false }));
        props.handleEditingTitle(event);
      }
    },
    [props.isLoading, props.handleEditingTitle]
  );

  const onEditContent = useCallback(
    (value) => {
      if (!props.isLoading) {
        setIsFirstActionContent(false);
        props.handleEditingContent(value);
      }
    },
    [props.isLoading, props.handleEditingContent]
  );

  useEffect(() => {
    const style = document.createElement("style");
    if (!isFirstActionContent && isError.content) {
      style.innerHTML = `
        .ck-editor__editable {
          border-color: red !important;
        }
      `;
      document.head.appendChild(style);
    }

    return () => {
      if (style.parentNode) {
        document.head.removeChild(style);
      }
    };
  }, [isFirstActionContent, isError.content]);

  useEffect(() => {
    if (!props?.addAction) {
      setIsFirstActionTitle(true);
      setIsFirstActionContent(true);
      setIsError({ title: false, sameTitle: false, content: false });
    }
  }, [props?.addAction]);

  useEffect(() => {
    setIsError((prev) => ({ ...prev, title: !props?.modalEdit?.title }));
  }, [props?.modalEdit?.title]);

  useEffect(() => {
    setIsError((prev) => ({ ...prev, content: !props?.modalEdit?.content }));
  }, [props?.modalEdit?.content]);

  const handleOnClickBtnSave = useCallback(async () => {
    setIsFirstActionTitle(false);
    setIsFirstActionContent(false);
    if ((props?.modalEdit?.title)?.toLowerCase().trim() === ('giới thiệu')?.toLowerCase().trim()) 
      setIsError((prev) => ({ ...prev, sameTitle: true }));
    else if (!isLoadingImg && !isError.title && !isError.content) {
      const result = await props.handleSave();
      if (result?.status_code === 409) {
        setIsError((prev) => ({ ...prev, sameTitle: true }));
      }
    }
  }, [isLoadingImg, isError.title, isError.content, props.handleSave]);

  return (
    <>
      <div
        className={`bg-prevent-click ${props.addAction ? "" : "d-hidden"}`}
        onClick={props.closeModal}
      ></div>
      <div
        className={`modal-index modal-edit-post ${
          props.addAction ? "" : "d-hidden"
        }`}
      >
        <div className="modal-header">
          <div className="flex-10"></div>
          <div>
            {props.modalEdit.action === "UPDATE"
              ? "Chỉnh sửa thông tin"
              : props.modalEdit.action === "ADD"
              ? "Thêm thông tin"
              : "Chỉnh sửa Giới thiệu"}
          </div>
          <div>
            <button className="btn-cancel" onClick={props.closeModal}>
              <CancelIcon />
            </button>
          </div>
        </div>
        <div className="modal-custom-body">
          <div className="modal-title">
            <div className="title-input">
              <p>Tiêu đề:</p>
              <div className="right-container">
                <Input
                  className={!isFirstActionTitle && (isError.title || isError.sameTitle) ? "error" : ""}
                  type="text"
                  name="title"
                  value={props.modalEdit.title}
                  onChange={onEditTitle}
                />
              </div>
            </div>
            {!isFirstActionTitle && isError.title && (
              <div className="title-input">
                <p style={{ opacity: 0 }}>Tiêu đề:</p>
                <div className="right-container">
                  <div className="error-message-container">
                    <div className="text-container">
                      <div className="explain-error">Vui lòng nhập tiêu đề</div>
                    </div>
                  </div>
                </div>
              </div>
            )}
            {!isFirstActionTitle && !isError.title && isError.sameTitle && (
              <div className="title-input">
                <p style={{ opacity: 0 }}>Tiêu đề:</p>
                <div className="right-container">
                  <div className="error-message-container">
                    <div className="text-container">
                      <div className="explain-error">Tiêu đề đã tồn tại</div>
                    </div>
                  </div>
                </div>
              </div>
            )}
          </div>
          <div className="content-input">
            <p>Nội dung:</p>
            <CustomEditor
              setIsLoadingImg={setIsLoadingImg}
              data={props?.modalEdit?.content}
              onBlur={(event, editor) => onEditContent(editor.getData())}
            />
            {!isFirstActionContent && isError.content && (
              <div className="error-message-container">
                <div className="text-container">
                  <div className="explain-error">Vui lòng nhập nội dung</div>
                </div>
              </div>
            )}
          </div>
        </div>
        <div className="modal-custom-footer">
          <ButtonLoading
            isLoading={props.isLoading}
            onClick={handleOnClickBtnSave}
            title={"Lưu"}
            disabled={isLoadingImg}
          />
        </div>
      </div>
    </>
  );
}


// import React, { useState, useEffect, useCallback } from "react";
// import { Input } from "antd";
// import CancelIcon from "../../../icons/CancelIcon";
// import CustomEditor from "../../../components/CustomEditor/CustomEditor";
// import ButtonLoading from "../../ButtonLoadind/ButtonLoading";

// export default function ModalEdit(props) {
//   const [isLoadingImg, setIsLoadingImg] = useState(false);
//   const [isFirstActionTitle, setIsFirstActionTitle] = useState(true);
//   const [isFirstActionContent, setIsFirstActionContent] = useState(true);
//   const [isErrorTitle, setIsErrorTitle] = useState(false);
//   const [isErrorSameTitle, setIsErrorSameTitle] = useState(false);
//   const [isErrorContent, setIsErrorContent] = useState(false);

//   const onEditTitle = useCallback(
//     (event) => {
//       if (!props.isLoading) {
//         setIsFirstActionTitle(false);
//         setIsErrorSameTitle(false);
//         props.handleEditingTitle(event);
//       }
//     },
//     [props.isLoading, props.handleEditingTitle]
//   );

//   const onEditContent = useCallback(
//     (value) => {
//       if (!props.isLoading) {
//         setIsFirstActionContent(false);
//         props.handleEditingContent(value);
//       }
//     },
//     [props.isLoading, props.handleEditingContent]
//   );

//   useEffect(() => {
//     const style = document.createElement("style");
//     if (!isFirstActionContent && isErrorContent) {
//       style.innerHTML = `
//         .ck-editor__editable {
//           border-color: red !important;
//         }
//       `;
//       document.head.appendChild(style);
//     }

//     return () => {
//       if (style.parentNode) {
//         document.head.removeChild(style);
//       }
//     };
//   }, [isFirstActionContent, isErrorContent]);

//   useEffect(() => {
//     if (!props?.addAction) {
//       setIsFirstActionTitle(true);
//       setIsFirstActionContent(true);
//       setIsErrorSameTitle(false);
//     }
//   }, [props?.addAction]);

//   useEffect(() => {
//     setIsErrorTitle(!props?.modalEdit?.title);
//   }, [props?.modalEdit?.title]);

//   useEffect(() => {
//     setIsErrorContent(!props?.modalEdit?.content);
//   }, [props?.modalEdit?.content]);

//   const handleOnClickBtnSave = useCallback(async () => {
//     setIsFirstActionTitle(false);
//     setIsFirstActionContent(false);
//     if (!isLoadingImg && !isErrorTitle && !isErrorContent) {
//       const result = await props.handleSave();
//       if (result?.status_code === 409) setIsErrorSameTitle(true);
//     }
//   }, [isLoadingImg, isErrorTitle, isErrorContent, props.handleSave]);

//   return (
//     <>
//       <div
//         className={`bg-prevent-click ${props.addAction ? "" : "d-hidden"}`}
//         onClick={props.closeModal}
//       ></div>
//       <div
//         className={`modal-index modal-edit-post ${
//           props.addAction ? "" : "d-hidden"
//         }`}
//       >
//         <div className="modal-header">
//           <div className="flex-10"></div>
//           <div>
//             {props.modalEdit.action === "UPDATE"
//               ? "Chỉnh sửa thông tin"
//               : props.modalEdit.action === "ADD"
//               ? "Thêm thông tin"
//               : "Chỉnh sửa Giới thiệu"}
//           </div>
//           <div>
//             <button className="btn-cancel" onClick={props.closeModal}>
//               <CancelIcon />
//             </button>
//           </div>
//         </div>
//         <div className="modal-custom-body">
//           <div className="modal-title">
//             <div className="title-input">
//               <p>Tiêu đề:</p>
//               <div className="right-container">
//                 <Input
//                   className={!isFirstActionTitle && (isErrorTitle || isErrorSameTitle) ? "error" : ""}
//                   type="text"
//                   name="title"
//                   value={props.modalEdit.title}
//                   onChange={onEditTitle}
//                 />
//               </div>
//             </div>
//             {!isFirstActionTitle && isErrorTitle && (
//               <div className="title-input">
//                 <p style={{ opacity: 0 }}>Tiêu đề:</p>
//                 <div className="right-container">
//                   <div className="error-message-container">
//                     <div className="text-container">
//                       <div className="explain-error">Vui lòng nhập tiêu đề</div>
//                     </div>
//                   </div>
//                 </div>
//               </div>
//             )}
//             {!isFirstActionTitle && !isErrorTitle && isErrorSameTitle &&(
//               <div className="title-input">
//                 <p style={{ opacity: 0 }}>Tiêu đề:</p>
//                 <div className="right-container">
//                   <div className="error-message-container">
//                     <div className="text-container">
//                       <div className="explain-error">Tiêu đề đã tồn tại</div>
//                     </div>
//                   </div>
//                 </div>
//               </div>
//             )}
//           </div>
//           <div className="content-input">
//             <p>Nội dung:</p>
//             <CustomEditor
//               setIsLoadingImg={setIsLoadingImg}
//               data={props?.modalEdit?.content}
//               onBlur={(event, editor) => onEditContent(editor.getData())}
//             />
//             {!isFirstActionContent && isErrorContent && (
//               <div className="error-message-container">
//                 <div className="text-container">
//                   <div className="explain-error">Vui lòng nhập nội dung</div>
//                 </div>
//               </div>
//             )}
//           </div>
//         </div>
//         <div className="modal-custom-footer">
//           <ButtonLoading
//             isLoading={props.isLoading}
//             // onClick={async () => {
//             //   setIsFirstActionTitle(false);
//             //   setIsFirstActionContent(false);
//             //   if (!isLoadingImg && !isErrorTitle && !isErrorContent) {
//             //     const result = await props.handleSave();
//             //     if (result?.status_code === 409) setIsErrorSameTitle(true);
//             //   }
//             // }}
//             onClick={handleOnClickBtnSave}
//             title={"Lưu"}
//             disabled={isLoadingImg}
//           />
//         </div>
//       </div>
//     </>
//   );
// }
