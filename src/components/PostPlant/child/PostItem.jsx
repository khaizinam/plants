import React from "react";
import parse from "html-react-parser";
import EditIcon from "../../../icons/EditIcon";
import DeletePostIcon from "../../../icons/DeletePostIcon";

export default function PostItem(props) {
  return (
    <div className="row-box" id={props.id}>
      <div className="post-title">
        <h1>{props.post.title}</h1>
        <div
          className="group-button"
          hidden={!props.isEditable || props.addAction}
        >
          <button
            className="btn-library"
            onClick={() => props.editPost(props.post)}
          >
            <EditIcon />
          </button>

          <button
            className="btn-library"
            onClick={() => props.handleDelete(props.post)}
          >
            <DeletePostIcon />
          </button>
        </div>
      </div>
      <div className="post-content">{parse(props.post.content)}</div>
    </div>
  );
}
