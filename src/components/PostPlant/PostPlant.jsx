import { useSelector } from "react-redux";
import "./postPLant.scss";
import parse from "html-react-parser";
import { useEffect, useState, useCallback } from "react";
import EditIcon from "../../icons/EditIcon";
import plantService from "../../services/plantService";
import PlusIcon from "../../icons/PlusIcon";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import ModalEdit from "./child/ModalEdit";
import PostItem from "./child/PostItem";
import ModalConfirm from "../ModalConfirm/ModalConfirm";
import ModalEditPlant from "../../components/PlantList/child/ModalCreateEditPlant";
import StatisticPlantAdmin from "./child/StatisticPlantAdmin";

export default function PostPlant(props) {
  const [data, setData] = useState(props.data);
  const { user } = useSelector((state) => state.Auth);
  const isEditable = user?.role === "ADMIN";
  const [addAction, setAddAction] = useState(false);
  const [modalConfirm, setModalConfirm] = useState({ hidden: true, id: 0 });
  const [modalEdit, setModalEdit] = useState({
    id: 0,
    title: "",
    content: "",
    action: "ADD",
  });
  const [isOpenEditModal, setIsOpenEditModal] = useState(false);
  const [loading, setIsLoading] = useState(false);

  useEffect(() => {
    setData(props.data);
  }, [props.data]);

  const editPost = useCallback((post) => {
    setModalEdit({
      id: post.id,
      title: post.title,
      content: post.content,
      action: "UPDATE",
    });
    setAddAction(true);
  }, []);

  const handleEditPlant = async (plantData) => {
    try {
      setIsLoading(true);
      await plantService.updatePlant(data?.id, plantData);
      setIsLoading(false);
      toast.success("Update successful!", { theme: "colored" });
      setIsOpenEditModal(false);
      props.fetchData();
      return { result: true, status_code: 200 };
    } catch (error) {
      setIsLoading(false);
      if (error?.response?.status !== 409) {
        toast.error("System error", { theme: "colored" });
      }
      return { result: false, status_code: error?.response?.status };
    }
  };

  const handleSave = useCallback(async () => {
    setIsLoading(true);
    try {
      if (modalEdit.action === "UPDATE_DES") {
        await plantService.updateDescription(data.id, modalEdit.content);
        data.description = modalEdit.content;
      } else if (modalEdit.action === "UPDATE") {
        await plantService.updatePost({
          id: modalEdit.id,
          title: modalEdit.title,
          content: modalEdit.content,
        });
        const post = props.data.posts.find((p) => p.id === modalEdit.id);
        if (post) {
          post.title = modalEdit.title;
          post.content = modalEdit.content;
        }
      } else {
        const res = await plantService.createPost(data.id, {
          title: modalEdit.title,
          content: modalEdit.content,
        });
        props.data.posts = [...props.data.posts, res.data];
      }
      resetValue();
      props.fetchData();
      toast.success("Đã thêm bài viết", { theme: "colored", autoClose: 2000 });
      return { result: true, status_code: 200 };
    } catch (error) {
      setIsLoading(false);
      if (error?.response?.status !== 409) {
        toast.error("Update failed!", { theme: "colored" });
      }
      return { result: false, status_code: error?.response?.status };
    }
  }, [modalEdit, data.id, props]);

  const resetValue = useCallback(() => {
    setModalEdit({ id: 0, title: "", content: "", action: "" });
    setAddAction(false);
    setIsLoading(false);
  }, []);

  const closeConfirm = useCallback(() => {
    setModalConfirm({ hidden: true, id: 0 });
  }, []);

  const closeModal = useCallback(() => {
    setModalEdit({ id: 0, title: "", content: "", action: "" });
    setAddAction(false);
  }, []);

  const handleDelete = useCallback((post) => {
    setModalConfirm({ hidden: false, id: post.id });
  }, []);

  const deleteConfirm = useCallback(async () => {
    try {
      setIsLoading(true);
      await plantService.deletePost(modalConfirm.id);
      setData((prev) => ({
        ...prev,
        posts: prev.posts.filter((e) => e.id !== modalConfirm.id),
      }));
      setModalConfirm({ hidden: true, id: 0 });
      setIsLoading(false);
      props.fetchData();
      toast.success("Đã xóa!", { autoClose: 1500 });
    } catch (error) {
      toast.error("Delete failed!", { theme: "colored" });
    }
  }, [modalConfirm.id, props]);

  const openModal = useCallback(() => {
    setModalEdit({ id: 0, title: "", content: "", action: "ADD" });
    setAddAction(!addAction);
  }, [addAction]);

  const handleEditingTitle = useCallback(
    (event) => {
      if (modalEdit.action !== "UPDATE_DES") {
        setModalEdit((prev) => ({ ...prev, title: event.target.value }));
      }
    },
    [modalEdit.action]
  );

  const handleEditingContent = useCallback((content) => {
    setModalEdit((prev) => ({ ...prev, content }));
  }, []);

  return (
    <div className="wr-post-plant">
      <StatisticPlantAdmin plant_id={data.id} />
      <div className="row-box" id="gioi_thieu">
        <div className="post-title">
          <h1>Giới thiệu</h1>
          <div hidden={!isEditable || addAction} className="group-button">
            <button
              hidden={addAction}
              className="btn-library"
              onClick={() => setIsOpenEditModal(true)}
            >
              <EditIcon />
            </button>
          </div>
        </div>
        <div className="post-content-head">
          <div className="post-content-text">
            {parse(data?.description ? data.description : "")}
          </div>
          <div className="post-content-image">
            <img src={data?.avatar} alt="plant image" />
          </div>
        </div>
      </div>
      {data?.posts &&
        data.posts.map((post) => (
          <PostItem
            key={`title-${post.id}`}
            id={`title-${post.id}`}
            post={post}
            isEditable={isEditable}
            addAction={addAction}
            editPost={editPost}
            handleDelete={handleDelete}
          />
        ))}
      <button
        className="btn-add-post"
        hidden={!isEditable || addAction}
        onClick={openModal}
      >
        <PlusIcon />
      </button>
      <ModalEdit
        isLoading={loading}
        addAction={addAction}
        handleSave={handleSave}
        modalEdit={modalEdit}
        closeModal={closeModal}
        handleEditingTitle={handleEditingTitle}
        handleEditingContent={handleEditingContent}
      />
      <ModalEditPlant
        action="UPDATE"
        isOpenModal={isOpenEditModal}
        setIsOpenModal={setIsOpenEditModal}
        isLoading={loading}
        handlePlant={handleEditPlant}
        plantData={{
          name: data?.name,
          avatar: data?.avatar,
          category: data?.category,
          description: data?.description,
        }}
        toast={toast}
      />
      <ModalConfirm
        isLoading={loading}
        message={"Bạn chắc chắn muốn xoá!"}
        hidden={modalConfirm.hidden}
        onConfirm={deleteConfirm}
        onCancel={closeConfirm}
      />
    </div>
  );
}

// import { useSelector } from "react-redux";
// import "./postPLant.scss";
// import parse from "html-react-parser";
// import { useEffect, useState } from "react";
// import EditIcon from "../../icons/EditIcon";

// import plantService from "../../services/plantService";
// import PlusIcon from "../../icons/PlusIcon";

// import { toast } from "react-toastify";
// import "react-toastify/dist/ReactToastify.css";
// import ModalEdit from "./child/ModalEdit";
// import PostItem from "./child/PostItem";
// import ModalConfirm from "../ModalConfirm/ModalConfirm";
// import ModalEditPlant from "../../components/PlantList/child/ModalCreateEditPlant";
// import StatisticPlantAdmin from "./child/StatisticPlantAdmin";

// export default function PostPlant(props) {
//   const [data, setData] = useState(props.data);

//   const { user } = useSelector((state) => state.Auth);
//   const isEditable = user?.role === "ADMIN";
//   const [addAction, setAddAction] = useState(false);
//   const [modalConfirm, setModalConfirm] = useState({ hidden: true, id: 0 });
//   const [modalEdit, setModalEdit] = useState({
//     id: 0,
//     title: "",
//     content: "",
//     action: "ADD",
//   });
//   const [isOpenEditModal, setIsOpenEditModal] = useState("");

//   const [loading, setIsLoading] = useState(false);

//   useEffect(() => {
//     setData(props.data);
//   }, [props.data]);

//   /* OPEN UPDATE */
//   const editPost = (post) => {
//     setModalEdit({
//       id: post.id,
//       title: post.title,
//       content: post.content,
//       action: "UPDATE",
//     });
//     setAddAction(true);
//   };

//   const handleEditPlant = async (plantData) => {
//     try {
//       setIsLoading(true);
//       await plantService.updatePlant(data?.id, plantData);
//       setIsLoading(false);
//       toast.success("update success!", { theme: "colored" });
//       setIsOpenEditModal(false);
//       props.fetchData();
//       return {
//         result: true,
//         status_code: 200
//       };
//     } catch (error) {
//       setIsLoading(false);
//       if (error?.response?.status != 409) {
//         toast.error("Hệ thống bị lỗi", { theme: "colored" });
//       }
//       return {
//         result: false,
//         status_code: error?.response?.status
//       };
//     }
//   }

//   /* UPDATE */
//   const handleSave = () => {
//     setIsLoading(true);
//     if (modalEdit.action === "UPDATE_DES") {
//       saveUpdateDescription();
//     } else if (modalEdit.action === "UPDATE") {
//       saveUpdatePost();
//     } else {
//       saveNewPost();
//     }
//   };

//   const resetValue = () => {
//     setModalEdit({
//       id: 0,
//       title: "",
//       content: "",
//       action: "",
//     });
//     setAddAction(false);
//     setIsLoading(false);
//     toast.success("update success!", { theme: "colored" });
//   };

//   const closeConfirm = () => {
//     setModalConfirm({ hidden: true, id: 0 });
//   };

//   const closeModal = () => {
//     setModalEdit({
//       id: 0,
//       title: "",
//       content: "",
//       action: "",
//     });
//     setAddAction(false);
//   };
//   const saveUpdateDescription = async () => {
//     try {

//       // eslint-disable-next-line no-unused-vars
//       const res = await plantService.updateDescription(
//         data.id,
//         modalEdit.content
//       );
//       data.description = modalEdit.content;
//       resetValue();
//     } catch (error) {
//       toast.error("update fail!", { theme: "colored" });
//       setIsLoading(false);
//     }
//   };

//   const saveUpdatePost = async () => {
//     try {
//       // eslint-disable-next-line no-unused-vars
//       const res = await plantService.updatePost({
//         id: modalEdit.id,
//         title: modalEdit.title,
//         content: modalEdit.content,
//       });

//       const found = props.data.posts.find(
//         (element) => element.id === modalEdit.id
//       );

//       if (found) {
//         found.title = modalEdit.title;
//         found.content = modalEdit.content;
//         resetValue();
//         props?.fetchData();
//       }
//     } catch (error) {
//       toast.error("update fail!", { theme: "colored" });
//       setIsLoading(false);
//     }
//   };

//   const saveNewPost = async () => {
//     try {
//       const res = await plantService.createPost(data.id, {
//         title: modalEdit.title,
//         content: modalEdit.content,
//       });
//       props.data.posts = [...props.data.posts, res.data];
//       resetValue();
//       props.fetchData();
//     } catch (error) {
//       toast.error("update fail!", { theme: "colored" });
//       setIsLoading(false);
//     }
//   };

//   /* DELETE */

//   const handleDelete = async (post) => {
//     setModalConfirm({
//       hidden: false,
//       id: post.id,
//     });
//   };

//   const deleteConfirm = async () => {
//     try {
//       setIsLoading(true);

//       // eslint-disable-next-line no-unused-vars
//       const res = await plantService.deletePost(modalConfirm.id);
//       setData((prev) => {
//         return {
//           ...prev,
//           posts: prev.posts.filter((e) => e.id !== modalConfirm.id),
//         };
//       });

//       setModalConfirm({
//         hidden: true,
//         id: 0,
//       });
//       setIsLoading(false);
//       props.fetchData();
//       toast.success("Delete success!", { theme: "colored" });
//     } catch (error) {
//       toast.error("Delete fail!", { theme: "colored" });
//     }
//   };

//   /* open add modal */
//   const openModal = () => {
//     setModalEdit({
//       id: 0,
//       title: "",
//       content: "",
//       action: "ADD",
//     });
//     setAddAction(!addAction);
//   };

//   /* EDITING */
//   const handleEditingTitle = (event) => {
//     if (modalEdit.action !== "UPDATE_DES") {
//       setModalEdit((prev) => {
//         return {
//           ...prev,
//           title: event.target.value,
//         };
//       });
//     }
//   };

//   const handleEditingContent = (content) => {
//     setModalEdit((prev) => {
//       return {
//         ...prev,
//         content: content,
//       };
//     });
//   };

//   return (
//     <div className="wr-post-plant">
//       <StatisticPlantAdmin plant_id={data.id} />
//       <div className="row-box" id="gioi_thieu">
//         <div className="post-title">
//           <h1>Giới thiệu</h1>
//           <div hidden={!isEditable || addAction} className="group-button">
//             <button
//               hidden={addAction}
//               className="btn-library"
//               onClick={() => setIsOpenEditModal(true)}
//             >
//               <EditIcon />
//             </button>
//           </div>
//         </div>
//         <div className="post-content-head">
//           <div className="post-content-text">
//             {parse(data?.description ? data.description : "")}
//           </div>
//           <div className="post-content-image">
//             <img src={data?.avatar} alt="plant image" />
//           </div>
//         </div>
//       </div>
//       {data?.posts ? (
//         data.posts.map((post, index) => {
//           return (
//             <PostItem
//               key={`title-${post.id}`}
//               id={`title-${post.id}`}
//               post={post}
//               isEditable={isEditable}
//               addAction={addAction}
//               editPost={editPost}
//               handleDelete={handleDelete}
//             />
//           );
//         })
//       ) : (
//         <></>
//       )}

//       <button
//         className="btn-add-post"
//         hidden={!isEditable || addAction}
//         onClick={openModal}
//       >
//         <PlusIcon />
//       </button>
//       <ModalEdit
//         isLoading={loading}
//         addAction={addAction}
//         handleSave={handleSave}
//         modalEdit={modalEdit}
//         closeModal={closeModal}
//         handleEditingTitle={handleEditingTitle}
//         handleEditingContent={handleEditingContent}
//       />
//       <ModalEditPlant
//         action="UPDATE"
//         isOpenModal={isOpenEditModal}
//         setIsOpenModal={setIsOpenEditModal}
//         isLoading={loading}
//         handlePlant={handleEditPlant}
//         plantData={{
//           name: data?.name,
//           avatar: data?.avatar,
//           category: data?.category,
//           description: data?.description
//         }}
//         toast={toast}
//       />
//       <ModalConfirm
//         isLoading={loading}
//         message={"Bạn chắc chắn muốn xoá!"}
//         hidden={modalConfirm.hidden}
//         onConfirm={deleteConfirm}
//         onCancel={closeConfirm}
//       />
//     </div>
//   );
// }
