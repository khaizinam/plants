import React from 'react'

export default function Pagination(props) {
    function changePage(index) {
        props.onChangePage(index);
    }

    function renderPagination(pagination) {
        if (pagination.total_page === 1) {
            return "";
        }
        let el = []
        if (pagination.total_page >= 5) {
            let num_prev_page = 2;
            let num_next_page = 2;
            if (pagination.page <= 3) {
                num_prev_page = pagination.page - 1;
                num_next_page = 5 - num_prev_page - 1;
            } else if (pagination.total_page - pagination.page <= 3) {
                num_next_page = pagination.total_page - pagination.page;
                num_prev_page = 5 - num_next_page - 1;
            }
            for (let index = pagination.page - num_prev_page; index <= pagination.page + num_next_page; index++) {
                el.push(<li key={index} className={`page-item${pagination.page === index ? ' pagination-active' : ""}`}><button onClick={() => changePage(index)} className="page-link" href="#1">{index}</button></li>)
            }
        } else {
            for (let index = 1; index <= pagination.total_page; index++) {
                el.push(
                    <li key={index} className={`page-item${pagination.page === index ? ' pagination-active' : ""}`}>
                        <button className="page-link" href="#1" onClick={() => changePage(index)}>{index}</button>
                    </li>)
            }
        }
        return <nav aria-label="row-jc-center">
            <ul className="pagination">
                <li className="page-item">
                    <button className="page-link" href="#1" onClick={() => changePage(1)}>
                        <span aria-hidden="true">&laquo;</span>
                    </button>
                </li>
                {el}
                <li className="page-item">
                    <button className="page-link" href="#1" onClick={() => changePage(pagination.total_page)}>
                        <span aria-hidden="true">&raquo;</span>
                    </button>
                </li>
            </ul>
        </nav>

    }
    return <>{renderPagination(props.pagination)}</>;
}
