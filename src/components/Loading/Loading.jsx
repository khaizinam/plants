import React from "react";
import "./Loading.scss";

const Loading = ({ size = 24 }) => {
  return (
    <div className="loading-spinner">
      <div className="spinner" style={{ width: `${size}px` }}></div>
    </div>
  );
};

export default Loading;
