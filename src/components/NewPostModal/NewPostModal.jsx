/* eslint-disable no-unused-vars */
import "./NewPostModal.scss";

import { useRef, useState } from "react";
import {
  faArrowUpFromBracket,
  faImage,
  faXmark,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import UploadImage from "../../icons/UploadImage.png";

import SwitchButton from "../SwitchButton/SwitchButton";
import DropDown from "../DropDown/DropDown";
import Textarea from "../Textarea/Textarea";
import axios from "axios";
import LibraryImages from "../LibraryImages/LibraryImages";

const NewPostModal = () => {
  const fileRef = useRef();
  const [showLibrary, setShowLibrary] = useState(false);
  const [images, setImages] = useState([]);

  const [plants, setPlants] = useState([
    {
      img: "https://www.applesfromny.com/wp-content/uploads/2020/05/20Ounce_NYAS-Apples2.png",
      title: "Táo",
      value: "Táo",
    },
    {
      img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRfTqZ3L_CyUUfJMomcPsTuYbcAl89g1D5p8g&usqp=CAU",
      title: "Mận",
      value: "Mận",
    },
    {
      img: "https://file.hstatic.net/200000455983/article/y-nghia-hoa-huong-duong_f7dd8dd0932042539493d34c7764c2ab.png",
      title: "Hướng dương",
      value: "Hướng dương",
    },
  ]);

  const submitNewPost = async () => {
    try {
      const fileData = new FormData();
      for (let f of images) {
        if (f.type === "file") fileData.append("images", f.data);
        else fileData.append("links", f.data);
      }
      const { data } = await axios.post(
        "http://localhost:8800/uploads",
        fileData
      );
      // console.log(data);
    } catch (error) {
      // console.log(error);
    }
  };

  const removeImage = (i) => {
    const newImages = images.filter((img, index) => index !== i);
    setImages(newImages);
  };

  const selectFile = (e) => {
    const selectedFiles = [];
    for (let f of e.target.files) {
      selectedFiles.push({ type: "file", data: f });
    }
    setImages((prev) => [...prev, ...selectedFiles]);
  };

  return (
    <div className="new-post-body">
      <div className="new-post-left">
        <input
          type="file"
          multiple
          hidden
          ref={fileRef}
          onChange={selectFile}
        />
        <div className="images-container">
          {images.length > 0 ? (
            <div className="image-list">
              {images.map((image, index) => (
                <div className="image-item" key={index}>
                  {image.type === "file" ? (
                    <img src={URL.createObjectURL(image.data)} alt="" />
                  ) : (
                    <img src={image.data} alt="" />
                  )}
                  <div
                    className="remove-btn"
                    onClick={() => removeImage(index)}
                  >
                    <FontAwesomeIcon icon={faXmark} />
                  </div>
                </div>
              ))}
            </div>
          ) : (
            <div className="upload-container">
              <img src={UploadImage} alt="" />
              <span>Chưa có ảnh</span>
            </div>
          )}
        </div>

        <div className="btn-group">
          <div
            className="img-btn upload-btn"
            onClick={() => fileRef.current.click()}
          >
            <FontAwesomeIcon icon={faArrowUpFromBracket} />
            <div className="upload-tooltip">Tải ảnh lên</div>
          </div>
          <div
            className="img-btn choose-btn"
            onClick={() => setShowLibrary(true)}
          >
            <FontAwesomeIcon icon={faImage} />
            <div className="upload-tooltip">Thư viện</div>
          </div>
        </div>

        {showLibrary && (
          <LibraryImages
            onClose={() => setShowLibrary(false)}
            onSelect={(data) => setImages((prev) => [...prev, ...data])}
          />
        )}
      </div>

      <div className="new-post-right">
        <div className="right-user">
          <div className="avatar">
            <img
              src="https://media.istockphoto.com/id/1402801804/photo/closeup-nature-view-of-palms-and-monstera-and-fern-leaf-background.webp?b=1&s=612x612&w=0&k=20&c=cvXa4CkqbIdOXxYkgAMxz-iFN3aBRibY7HMskePBRaE="
              alt=""
            />
          </div>

          <div className="name">Hồng Khánh</div>
        </div>

        <div className="right-description">
          <Textarea placeholder="Thêm mô tả ..." rows={5} maxRows={10} />
        </div>

        <div className="right-setting">
          <div className="setting-title">Tùy chọn</div>

          <div className="setting-option">
            <div className="option-name">Chủ đề</div>
            <div className="option-value">
              <DropDown options={plants} />
            </div>
          </div>
          <div className="setting-option">
            <div className="option-name">Công khai</div>
            <div className="option-value">
              <SwitchButton />
            </div>
          </div>
        </div>

        <div className="right-submit">
          <div className="send-post-btn" onClick={submitNewPost}>
            Đăng
          </div>
        </div>
      </div>
    </div>
  );
};

export default NewPostModal;
