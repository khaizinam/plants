/* eslint-disable no-unused-vars */
import "./LibraryImages.scss";

import { useState } from "react";

const LibraryImages = ({ onClose, onSelect }) => {
  const [selected, setSelected] = useState([]);
  const [images, setImages] = useState([
    "https://res.cloudinary.com/dnvpdjbx0/image/upload/v1705313249/file-manager/bexsa23ugtudoifayrk5.jpg",
    "https://res.cloudinary.com/dnvpdjbx0/image/upload/v1705313250/file-manager/kc1hpaczbba3hd5dr8sg.jpg",
    "https://res.cloudinary.com/dnvpdjbx0/image/upload/v1705313252/file-manager/e0iy1dkgeq8jtuhpmvlw.jpg",
    "https://res.cloudinary.com/dnvpdjbx0/image/upload/v1705313253/file-manager/jowmi1n2cyjzaipdfq3v.jpg",
    "https://res.cloudinary.com/dnvpdjbx0/image/upload/v1705313254/file-manager/hmmkuergvhhvgk4c2ux2.jpg",
    "https://res.cloudinary.com/dnvpdjbx0/image/upload/v1705313255/file-manager/pov0ms071k5fwuvqrsih.jpg",
    "https://res.cloudinary.com/dnvpdjbx0/image/upload/v1705313257/file-manager/nytk7gnaziboclssqhgv.jpg",
    "https://res.cloudinary.com/dnvpdjbx0/image/upload/v1705313258/file-manager/uroxpixahvfirajrvrwx.jpg",
    "https://res.cloudinary.com/dnvpdjbx0/image/upload/v1705313259/file-manager/hr5vokzpbg0dl8dzzuir.jpg",
    "https://res.cloudinary.com/dnvpdjbx0/image/upload/v1705313261/file-manager/l5vmyhop7aq8itvi7pzg.jpg",
    "https://res.cloudinary.com/dnvpdjbx0/image/upload/v1705313263/file-manager/gqqivmwz1go4auofccfq.jpg",
    "https://res.cloudinary.com/dnvpdjbx0/image/upload/v1705313266/file-manager/zawix3huuqucndw2yjpe.jpg",
    "https://res.cloudinary.com/dnvpdjbx0/image/upload/v1705313269/file-manager/vgrdjgladz9uukx77hhi.jpg",
    "https://res.cloudinary.com/dnvpdjbx0/image/upload/v1705313270/file-manager/nmrzro26hrmhqgfhhlwp.jpg",
    "https://res.cloudinary.com/dnvpdjbx0/image/upload/v1705313274/file-manager/kmm5rzxhkposdwfbqh3b.jpg",
    "https://res.cloudinary.com/dnvpdjbx0/image/upload/v1705313275/file-manager/ertja1ciy787fqv21pcn.jpg",
    "https://res.cloudinary.com/dnvpdjbx0/image/upload/v1705313276/file-manager/enlsxvbmxeofy8eiubtu.jpg",
    "https://res.cloudinary.com/dnvpdjbx0/image/upload/v1705313277/file-manager/q7mcfi51ytwj3eujxxrw.jpg",
  ]);

  const imageClick = (i) => {
    let newSelected;
    if (selected.includes(i)) {
      newSelected = selected.filter((s) => s !== i);
    } else {
      newSelected = [...selected, i];
    }
    setSelected(newSelected);
  };

  const submitSelection = () => {
    const selectedImages = selected.map((i) => ({
      type: "link",
      data: images[i],
    }));
    setSelected([]) || onClose() || onSelect(selectedImages);
  };

  return (
    <div className="library-select-images">
      <div className="lib-container">
        <div className="lib-images">
          <div className="images-list">
            {images.map((image, index) => {
              const number = selected.indexOf(index) + 1;
              return (
                <div
                  className={`image-item ${number > 0 ? "selected" : ""}`}
                  key={index}
                  onClick={() => imageClick(index)}
                >
                  <img src={image} alt="" />
                  {number > 0 && <div className="number">{number}</div>}
                </div>
              );
            })}
          </div>
        </div>
        <div className="actions-container">
          <div className="action-btn cancel" onClick={onClose}>
            Hủy
          </div>
          <div className="action-btn select" onClick={submitSelection}>
            Chọn
          </div>
        </div>
      </div>
    </div>
  );
};

export default LibraryImages;
