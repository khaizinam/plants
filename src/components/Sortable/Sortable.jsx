import { useRef } from "react";
import "./Sortable.scss";

const Sortable = ({ className, list, onChange, ...props }) => {
  const sortableRef = useRef();

  const initSortableList = (e) => {
    // e.preventDefault();
    // const sortableList = sortableRef.current;
    // const draggingItem = sortableList.querySelector(".dragging");
    // // Getting all items except currently dragging and making array of them
    // let siblings = [
    //   ...sortableList.querySelectorAll(".sortable-item:not(.dragging)"),
    // ];
    // // // console.log(siblings);
    // // // Finding the sibling after which the dragging item should be placed
    // let nextSibling = siblings.find((sibling) => {
    //   return e.clientY <= sibling.offsetTop + sibling.offsetHeight / 2;
    // });
    // // // Inserting the dragging item before the found sibling
    // sortableList.insertBefore(draggingItem, nextSibling);
  };

  const handleDragStart = (e) => {
    // let component = e.target;
    // while (!component.classList.contains("sortable-item")) {
    //   component = component.parentNode;
    // }
    // component.classList.add("dragging");
  };
  const handleDragEnd = (e) => {
    // let component = e.target;
    // while (!component.classList.contains("sortable-item")) {
    //   component = component.parentNode;
    // }
    // component.classList.remove("dragging");
    // const sortableList = sortableRef.current;
    // const listItem = sortableList.querySelectorAll(".sortable-item");
    // const newList = [];
    // for (let i = 0; i < listItem.length; i++) {
    //   newList.push(parseInt(listItem[i].dataset.value));
    // }
    // const oldList = list.map((item) => item.data);
    // if (JSON.stringify(oldList) !== JSON.stringify(newList)) {
    //   onChange && onChange(newList);
    // }
  };

  return (
    <div
      className={`sortable ${className}`}
      {...props}
      ref={sortableRef}
      onDragOver={initSortableList}
      onDragEnter={(e) => e.preventDefault()}
    >
      {list.map((p, index) => (
        <div
          key={index + Date.now()}
          className="sortable-item"
          draggable="true"
          onDragStart={handleDragStart}
          onDragEnd={handleDragEnd}
          data-value={p.data}
        >
          {p.title}
        </div>
      ))}
    </div>
  );
};

export default Sortable;
