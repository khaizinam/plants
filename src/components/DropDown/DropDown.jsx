import "./DropDown.scss";

import { useEffect, useRef, useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronDown, faSearch } from "@fortawesome/free-solid-svg-icons";

const DropDown = ({
  options = [],
  value,
  onChange,
  canNull = false,
  canFilter = false,
  disabled,
}) => {
  const dropdownRef = useRef();
  const [show, setShow] = useState(false);
  const [filter, setFilter] = useState("");

  const resizetString = (str, number) => {
    if (str.length <= number) return str;
    else {
      let r = str.slice(0, number - 3);
      return <div title={str}>{r + "..."}</div>;
    }
  };

  const filterChange = (e) => setFilter(e.target.value);

  const select = (option) => {
    onChange && onChange(option.id);
    setDefault();
  };

  const myFunction = () => {
    setShow((p) => !p);
    show && setDefault();
  };

  const setDefault = () => {
    setShow(false);
    setFilter("");
  };

  useEffect(() => {
    const handleClickOut = (e) => {
      if (dropdownRef.current && !dropdownRef.current.contains(e.target)) {
        setDefault();
      }
    };
    document.addEventListener("click", handleClickOut);
    return () => {
      document.removeEventListener("click", handleClickOut);
    };
  }, []);

  const newOptions = options.filter((option) =>
    option?.name.toLowerCase().includes(filter.toLowerCase())
  );

  const currentValue = options.find((option) => option.id === value);

  return (
    <div className="dropdown" ref={dropdownRef}>
      <div className="current-box" onClick={() => !disabled && myFunction()}>
        <div className="current-value">
          {currentValue?.img && (
            <div className="option-image">
              <img src={currentValue.img} alt="" />
            </div>
          )}
          <div className="option-value">
            {currentValue
              ? resizetString(currentValue.name, 15)
              : canNull
              ? "Khác"
              : "Chưa chọn"}
          </div>
        </div>
        {!disabled && (
          <div className="dropdown-icon">
            <FontAwesomeIcon icon={faChevronDown} />
          </div>
        )}
      </div>
      <div className={`dropdown-content ${show ? "show" : ""}`}>
        {canFilter && (
          <div className="filter-box">
            <FontAwesomeIcon icon={faSearch} />
            <input
              type="text"
              placeholder="Tìm ..."
              onChange={filterChange}
              value={filter}
            />
          </div>
        )}
        <div className="dropdown-options">
          {newOptions.length > 0 ? (
            <>
              {newOptions.map((option, index) => (
                <div
                  key={index}
                  className="option"
                  onClick={() => select(option)}
                >
                  {option.avatar && (
                    <div className="option-image">
                      <img src={option.avatar} alt="" />
                    </div>
                  )}
                  <div className="option-value">
                    {resizetString(option.name, 15)}
                  </div>
                </div>
              ))}
              {canNull && (
                <div
                  className="option"
                  onClick={() => select({ id: null, name: "Khác" })}
                >
                  <div className="option-value">Khác</div>
                </div>
              )}
            </>
          ) : (
            <div className="option disable">Không có</div>
          )}
        </div>
      </div>
    </div>
  );
};

export default DropDown;
