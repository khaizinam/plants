/* eslint-disable no-unused-vars */
import { useState } from "react";
import "./SlideImage.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faChevronLeft,
  faChevronRight,
} from "@fortawesome/free-solid-svg-icons";

const SlideImage = () => {
  const [images, setImages] = useState([
    "https://d2r55xnwy6nx47.cloudfront.net/uploads/2020/07/Photosynthesis_2880_Lede.jpg",
    "https://th.bing.com/th/id/R.dd8c0abf7e993d0fd4d4e982123e4258?rik=tXeTGngcxEx%2bHw&pid=ImgRaw&r=0",
    "https://th.bing.com/th/id/R.ff58e31d7bea58d8fc7366bb32989b58?rik=nK8TyFlL5h84qg&pid=ImgRaw&r=0",
    "https://www.pixelstalk.net/wp-content/uploads/images1/Backgrounds-Plant.jpg",
  ]);

  const [index, setIndex] = useState(0);
  return (
    <div className="slide-image">
      {index > 0 && (
        <div
          className="btn-ward prev"
          onClick={() => setIndex((prev) => prev - 1)}
        >
          <FontAwesomeIcon icon={faChevronLeft} />
        </div>
      )}
      {index < images.length - 1 && (
        <div
          className="btn-ward next"
          onClick={() => setIndex((prev) => prev + 1)}
        >
          <FontAwesomeIcon icon={faChevronRight} />
        </div>
      )}
      <div className="cumb">
        {images.map((image, i) => (
          <div
            key={i}
            className={`cumb-dot ${index === i ? "active" : ""}`}
            onClick={() => setIndex(i)}
          ></div>
        ))}
      </div>
      <img src={images[index]} alt="" />
    </div>
  );
};

export default SlideImage;
