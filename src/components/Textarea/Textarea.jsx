import { useRef } from "react";
import "./Textarea.scss";

const Textarea = ({ maxRows, onChange, ...props }) => {
  const textRef = useRef();

  const maxheight = maxRows ? 20 * maxRows : Infinity;

  const textChange = (e) => {
    const textarea = textRef.current;
    textarea.style.height = "auto";
    let scHeight = e.target.scrollHeight;
    if (scHeight > maxheight) {
      textarea.style.height = `${maxheight}px`;
      textarea.classList.remove("scrollY");
    } else {
      textarea.style.height = `${scHeight}px`;
      textarea.classList.add("scrollY");
    }
    onChange && onChange(textarea.value);
  };
  return (
    <div className="textarea">
      <textarea {...props} ref={textRef} onChange={textChange} />
    </div>
  );
};

export default Textarea;
