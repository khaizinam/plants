/* eslint-disable no-unused-vars */
import "./Sidebar.scss";

import { NavLink } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowRightFromBracket } from "@fortawesome/free-solid-svg-icons";

import FeedbackIcon from "../../icons/FeedbackIcon";
import BookIcon from "../../icons/BookIcon";
import StatisticIcon from "../../icons/StatisticIcon";

import { useDispatch } from "react-redux";
import { LogoutAction } from "../../redux/auth";
import Logo from "../Logo/Logo";

const AdminSidebar = () => {
  // const { user } = useSelector((state) => state.Auth);
  const dispatch = useDispatch();
  const handleLogout = async () => {
    try {
      dispatch(LogoutAction());
    } catch (error) {}
  };
  return (
    <div className="sidebar">
      <div className="sidebar-top">
        <div className="logo-sidebar">
          <Logo />
        </div>
      </div>

      <div className="sidebar-nav-list">
        <NavLink to={"/admin/statistic"} className="nav-link">
          {({ isActive }) => {
            return (
              <div className={`nav-item ${isActive ? `nav-active` : ``}`}>
                <span className="nav-icon">
                  <StatisticIcon />
                </span>
                <span className="nav-title">Thống kê</span>
              </div>
            );
          }}
        </NavLink>
        <NavLink to={"/admin/library"} className="nav-link">
          {({ isActive }) => {
            return (
              <div className={`nav-item ${isActive ? `nav-active` : ``}`}>
                <span className="nav-icon">
                  <BookIcon />
                </span>
                <span className="nav-title">Thư viện cây</span>
              </div>
            );
          }}
        </NavLink>
        <NavLink to={"/admin/feedbacks"} className="nav-link">
          {({ isActive }) => {
            return (
              <div className={`nav-item ${isActive ? `nav-active` : ``}`}>
                <span className="nav-icon">
                  <FeedbackIcon />
                </span>
                <span className="nav-title">Phản hồi</span>
              </div>
            );
          }}
        </NavLink>
      </div>

      <div className="sidebar-bottom">
        <NavLink className="nav-link" onClick={handleLogout}>
          <div className={`nav-item`}>
            <div className="nav-icon">
              <FontAwesomeIcon icon={faArrowRightFromBracket} />
            </div>
            <span className="nav-title">Đăng xuất</span>
          </div>
        </NavLink>
      </div>
    </div>
  );
  // const adminCMS = [
  //   {
  //     id: 1,
  //     title: "Plants CMS",
  //     url: "plants",
  //     child: [
  //       { title: "Plants", url: "plant" },
  //       { title: "Posts", url: "plant/posts" },
  //       { title: "Categories", url: "plant/categories" },
  //       { title: "Stages", url: "plant/stages" },
  //     ],
  //   },
  //   { id: 2, title: "Feed Backs", url: "feed-backs", child: [] },
  // ];
  // const [isSelectingMenu, setIsSelectingMenu] = useState(-1);
  // const handleLogout = async () => {
  //   try {
  //     dispatch(LogoutAction());
  //   } catch (error) {}
  // };
  // return (
  //   <div className="admin-sidebar">
  //     <div className="sidebar-top">
  //       <div className="logo-sidebar">
  //         <Logo />
  //       </div>
  //     </div>

  //     <div className="sidebar-nav-list">
  //       {adminCMS.map((menu, index) => {
  //         if (menu.child.length === 0) {
  //           return (
  //             <NavLink
  //               end
  //               to={menu.url}
  //               className="btn-menu-cms"
  //               key={`menu-${index}`}
  //               onClick={() => {
  //                 setIsSelectingMenu(-1);
  //               }}
  //             >
  //               {({ isActive }) => {
  //                 return (
  //                   <div className={`nav-item ${isActive ? `nav-active` : ``}`}>
  //                     <span className="nav-title">{menu.title}</span>
  //                   </div>
  //                 );
  //               }}
  //             </NavLink>
  //           );
  //         } else {
  //           return (
  //             <div className="wr-dropdown-btn" key={`menu-${index}`}>
  //               <button
  //                 className="btn-menu-cms"
  //                 id={`menu-${index}`}
  //                 onClick={() => setIsSelectingMenu(index)}
  //               >
  //                 <div
  //                   className={`nav-item ${
  //                     isSelectingMenu === index ? `title-active` : ``
  //                   }`}
  //                 >
  //                   <span className="nav-title">{menu.title}</span>
  //                 </div>
  //                 <div className="icon-dropdown">
  //                   <DownArrow down={isSelectingMenu === index} />
  //                 </div>
  //               </button>
  //               <div
  //                 className={`drop-down-list ${
  //                   isSelectingMenu === index ? "dropdown-active" : ""
  //                 }`}
  //               >
  //                 {menu.child.map((menuChild, childIndex) => {
  //                   return (
  //                     <NavLink
  //                       end
  //                       to={menuChild.url}
  //                       className="btn-menu-cms"
  //                       key={`menu-child-${childIndex}`}
  //                     >
  //                       {({ isActive }) => {
  //                         return (
  //                           <div
  //                             className={`nav-item ${
  //                               isActive ? `nav-active` : ``
  //                             }`}
  //                           >
  //                             <span className="nav-child-title">
  //                               {menuChild.title}
  //                             </span>
  //                           </div>
  //                         );
  //                       }}
  //                     </NavLink>
  //                   );
  //                 })}
  //               </div>
  //             </div>
  //           );
  //         }
  //       })}
  //       <NavLink className="btn-menu-cms" onClick={handleLogout}>
  //         <div className="nav-item">
  //           <span className="nav-title">Đăng xuất</span>
  //         </div>
  //       </NavLink>
  //     </div>
  //   </div>
  // );
};

export default AdminSidebar;
