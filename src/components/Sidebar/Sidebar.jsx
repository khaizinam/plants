import "./Sidebar.scss";

import { NavLink } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowRightFromBracket } from "@fortawesome/free-solid-svg-icons";

import GardenIcon from "../../icons/GardenIcon";
import BookIcon from "../../icons/BookIcon";
import RemindIcon from "../../icons/RemindIcon";

import { useDispatch, useSelector } from "react-redux";
import { LogoutAction } from "../../redux/auth";
import Logo from "../Logo/Logo";
import StatisticIcon from "../../icons/StatisticIcon";

const Sidebar = () => {
  const { user } = useSelector((state) => state.Auth);
  const dispatch = useDispatch();
  const handleLogout = async () => {
    try {
      dispatch(LogoutAction());
    } catch (error) {}
  };
  return (
    <div className="sidebar">
      <div className="sidebar-top">
        <div className="logo-sidebar">
          <Logo />
        </div>

        <NavLink to={"/profile"} className="sidebar-user">
          <div className="avatar">
            <img
              src="https://www.shutterstock.com/image-vector/adorable-vector-illustration-cute-turtle-600nw-2351051927.jpg"
              alt=""
            />
          </div>
          <div className="username">{user?.full_name}</div>
        </NavLink>
      </div>

      <div className="sidebar-nav-list">
        <NavLink to={"/garden"} className="nav-link">
          {({ isActive }) => {
            return (
              <div className={`nav-item ${isActive ? `nav-active` : ``}`}>
                <span className="nav-icon">
                  <GardenIcon />
                </span>
                <span className="nav-title">
                  <span>Vườn của tôi</span>
                </span>
              </div>
            );
          }}
        </NavLink>
        <NavLink to={"/reminder"} className="nav-link">
          {({ isActive }) => {
            return (
              <div className={`nav-item ${isActive ? `nav-active` : ``}`}>
                <span className="nav-icon">
                  <RemindIcon />
                </span>
                <span className="nav-title">Công việc</span>
              </div>
            );
          }}
        </NavLink>
        <NavLink to={"/statistic"} className="nav-link">
          {({ isActive }) => {
            return (
              <div className={`nav-item ${isActive ? `nav-active` : ``}`}>
                <span className="nav-icon">
                  <StatisticIcon />
                </span>
                <span className="nav-title">Thống kê</span>
              </div>
            );
          }}
        </NavLink>
        <NavLink to={"/library"} className="nav-link">
          {({ isActive }) => {
            return (
              <div className={`nav-item ${isActive ? `nav-active` : ``}`}>
                <span className="nav-icon">
                  <BookIcon />
                </span>
                <span className="nav-title">Thư viện cây</span>
              </div>
            );
          }}
        </NavLink>
      </div>

      <div className="sidebar-bottom">
        <NavLink className="nav-link" onClick={handleLogout}>
          <div className={`nav-item`}>
            <div className="nav-icon">
              <FontAwesomeIcon icon={faArrowRightFromBracket} />
            </div>
            <span className="nav-title">Đăng xuất</span>
          </div>
        </NavLink>
      </div>
    </div>
  );
};

export default Sidebar;
