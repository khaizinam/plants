import "./Logo.scss";

const Logo = () => {
  return (
    <div className="app-logo">
      <span className="logo-text large">Plants.com</span>
      <span className="logo-text small">P</span>
    </div>
  );
};

export default Logo;
