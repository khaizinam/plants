import "./CommentInput.scss";

import { faPaperPlane } from "@fortawesome/free-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const CommentInput = () => {
  return (
    <div className="comment-input">
      <div className="user-avatar">
        <img
          src="https://www.shutterstock.com/image-vector/adorable-vector-illustration-cute-turtle-600nw-2351051927.jpg"
          alt=""
        />
      </div>
      <input
        type="text"
        className="input-box"
        placeholder="Nhập bình luận ..."
      />
      <FontAwesomeIcon icon={faPaperPlane} className="comment-submit" />
    </div>
  );
};

export default CommentInput;
