import { NavLink, useLocation } from "react-router-dom";
import "./Breadcrumb.scss";
import { URL_BREADCRUMB } from "../../utils/utils";

const Breadcrumb = () => {
  const location = useLocation();
  let pathArr = location.pathname.split("/");
  pathArr = pathArr.filter((item) => item !== "");
  let pathConsum = "";
  function getSlug(val) {
    return URL_BREADCRUMB[val] ? URL_BREADCRUMB[val] : val;
  }
  function createBread(arr_list) {
    if (arr_list.length === 0) return;
    let el = [];
    arr_list.forEach((path, index) => {
      if (index > 0) el.push(<span key={`c-${index}`}>/</span>);
      pathConsum = `${pathConsum}/${path}`;
      if (index === arr_list.length - 1) {
        el.push(
          <span id={`bread-${index}`} key={index}>
            {getSlug(path)}
          </span>
        );
      } else {
        el.push(
          <NavLink id={`bread-${index}`} key={index} to={pathConsum}>
            {getSlug(path)}
          </NavLink>
        );
      }
    });
    return el;
  }
  return <div className="breadcrumb-custom">{createBread(pathArr)}</div>;
};

export default Breadcrumb;
