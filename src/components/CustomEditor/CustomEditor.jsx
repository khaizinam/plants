import './CustomEditor.scss';

import { toast } from "react-toastify";
import { CKEditor } from '@ckeditor/ckeditor5-react';
import Editor from 'ckeditor5-custom-build/build/ckeditor.js';

import plantService from "../../services/plantService";


const CustomEditor = ({
  setIsLoadingImg,
  ...colProps
}) => {

  const uploadAdapter = (loader) => {
    return {
      upload: () => {
        return new Promise(async (resolve, reject) => {
          try {
            setIsLoadingImg(true);
            const file = await loader.file;
            const formData = new FormData();
            formData.append("image", file);
            const response = await plantService.uploadImage(formData);
            setIsLoadingImg(false);
            resolve({
              default: response.data
            });
          } catch (error) {
            setIsLoadingImg(false);
            toast.error("Upload imgae failed!", { theme: "colored" });
          }
        });
      },
      abort: () => {}
    };
  }
  function uploadPlugin(editor) {
    editor.plugins.get("FileRepository").createUploadAdapter = (loader) => {
      return uploadAdapter(loader);
    };
  }

  return (
    <CKEditor
      editor={Editor}
      config={{
        // @ts-ignore
        extraPlugins: [uploadPlugin],
        allowedContent: true,
        fontSize: {
            // options: [12, 14, 16], // Các kích thước font cho phép
            defaultValue: '16px' // Giá trị mặc định cho font size
        }
      }}
      // data=""
      // onReady={ editor => {
      //   // You can store the "editor" and use when it is needed.
      //   console.log( 'Editor is ready to use!', editor );
      // } }

      onChange={ ( event, editor ) => {
        // console.log("check input data: ", editor.getData());
      } }
      // onBlur={ ( event, editor ) => {
      //   console.log( 'Blur.', editor );
      // } }
      // onFocus={ ( event, editor ) => {
      //   console.log( 'Focus.', editor );
      // }}
      {...colProps}
          />
  );
};

export default CustomEditor;
